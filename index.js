/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
 import {name as appName} from './app.json';
//  import {name as Hamanahel1} from './app.json';

AppRegistry.registerComponent(appName, () => App);
