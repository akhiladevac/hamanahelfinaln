
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Platform, ToastAndroid, BackHandler ,AsyncStorage} from 'react-native';

import { Actions, Router, Stack, Scene } from 'react-native-router-flux';
import Home from '../Scenes/Home/Home';
import TabIcon from './TabIcon';
import MemberDetail from '../Scenes/Members/MemberDetail';
import Profile from '../Scenes/Profile/Main';
import UserList from '../Scenes/UsersList/UserList';
import MemberSearch from '../Scenes/UsersList/MemberSearch';

import EventAll from '../Scenes/EventUsers/EventAll';
import Amount from '../Scenes/finance/Amount';
import DetailedNotification from '../Scenes/NotificationsDetail/Head';
import Login from '../Scenes/Login/Login';
import ChurchChoose from '../Scenes/Login/ChooseChurch';
import ChurchList from '../Scenes/Login/ChurchList';
import EditProfile from '../Scenes/Profile/EditProfile';
import Notification_Visits from '../priest/Notification_Visits/Notifications_Visits';
import FamilyPhoto from '../Scenes/Profile/familyPhoto';
import PriestProfile from '../priest/Profile/FatherProfile';

// -------------------ameen-------------------------------

import ViewNotification from '../priest/Notification/ViewNotification';
import AddNotification from '../priest/Notification/AddNotification';
import ViewVisit from '../priest/Visit/ViewVisit';
import FatherUserList from '../priest/Visit/FatherUserList';
import Visitlist from '../priest/Visit/Visitlist';

import AddVisit from '../priest/Visit/AddVisit';
import FatherUserList1 from '../priest/Visit/FatherUserLis1';
import EditNotification from '../priest/Notification/EditNotification';
import EditVisit from '../priest/Visit/EditVisit';
import Events from '../priest/Events/Events';
import FatherProfileEdit from '../priest/Profile/FatherProfileEdit';
import EditFamily from '../priest/Profile/EditFamily';
import FamilyList from '../priest/Profile/familyList';

// -------------------Akhila-------------------------------

import ResetPswd from '../Scenes/Login/ResetPswd'
import PhoneCheck from '../Scenes/Login/PhoneCheck'
import GenerateOTP from '../Scenes/Login/GenerateOTP'



const LOGIN_CREDENTIALS = '@login_credentials';

export default class Routers extends Component {
    
    constructor() {
        super();

        this.state = {
            userRole: '',
            abc:'',
            abc1:'',
            status_church:'',
            status_home:'',
    
           
        

        }
    }
    
      componentDidMount() {

        this.checkUserIsLoggedIn();

    }
    
    
    async checkUserIsLoggedIn() {
        try {
            var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
            if(loginCredentials) {
                // console.log(loginCredentials);
                var login_credentials = JSON.parse(loginCredentials);
                // console.log(login_credentials.role);
                this.setState({ userRole: login_credentials.role });
                this.setState({ status_church: false, status_home: true });


                 Actions.Home();
               
            }  else {
               
                this.setState({ status_church: true, status_home: false });

            }
        } 
        catch (error) {
            //console.error('Something went Wrong in Get Section' + error);
        }
    }

     renderTabBar() {


         if (Platform.OS === 'ios') {
            var navStatus = false;
         } else {
            var navStatus = true;
         }
        if (this.state.userRole === 'priest') {
            return (
               
                <Stack key="root">
                    <Scene key='tabbar' tabs={true} hideNavBar tabBarStyle={{ backgroundColor: '#000'}} showLabel={false} >
                    
                            <Scene
                                key="Home"
                                component={Home}
                              
                                initial={this.state.status_home}
                                title="Home" 
                                hideNavBar    
                                iconName="home"
                                showLabel={false}
                                icon={TabIcon}
                            />

                            <Scene
                                key="UserList"
                                component={UserList}
                                title="UserList" 
                                hideNavBar  
                                iconName="address-book"
                                showLabel={false}
                                icon={TabIcon}  
                                onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />
                               <Scene
                                key="MemberSearch"
                                component={MemberSearch}
                                title="MemberSearch" 
                                hideNavBar  
                                iconName1="blood-bag"
                                showLabel={false}
                                icon={TabIcon}  
                                onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />

                            <Scene
                                key="Events"
                                component={Events}
                                title="Events" 
                                hideNavBar  
                                iconName="calendar"
                                showLabel={false}
                                icon={TabIcon}  
                                // onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />

                            <Scene
                                key="Profile"
                                component={PriestProfile}
                                title="Profile" 
                                hideNavBar   
                                iconName="sliders"
                                showLabel={false}
                                icon={TabIcon} 
                                onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />
                    </Scene>

                    <Scene key='DetailedUser' tabs={false} hideNavBar={navStatus} component={MemberDetail} />
                    <Scene key='Login' tabs={false} hideNavBar={navStatus} component={Login} />
                    <Scene key='ChooseChurch' tabs={false} hideNavBar={navStatus} component={ChurchChoose} hideNavBar initial={this.state.status_church} />
                    <Scene key="detailedNotification" tabs={false} hideNavBar={navStatus} component={DetailedNotification} />
                    <Scene key="ChurchList" tabs={false} hideNavBar={navStatus} component={ChurchList} />
                    <Scene key="EditProfile" tabs={false} hideNavBar={navStatus} component={EditProfile} />

                     {/* ===================================  Akhila  ===================================== */}

                    <Scene key="ResetPswd" tabs={false} component={ResetPswd} />
                    <Scene key="PhoneCheck" tabs={false} component={PhoneCheck} />
                    <Scene key="GenerateOTP" tabs={false} component={GenerateOTP} />

                    {/* ===================================  Ameen  ===================================== */}

                    <Scene key="VisitNotification" tabs={false} hideNavBar component={Notification_Visits}  />
                    <Scene key="ViewNotification" tabs={false} hideNavBar={navStatus} component={ViewNotification} />
                    <Scene key="AddNotification" tabs={false} hideNavBar={navStatus} component={AddNotification} />
                    <Scene key="ViewVisit" tabs={false} hideNavBar={navStatus} component={ViewVisit} />
                    <Scene key="FatherUserList" tabs={false} hideNavBar={navStatus} component={FatherUserList} />
                    <Scene key="FatherUserList1" tabs={false} hideNavBar={navStatus} component={FatherUserList1} />
                    <Scene key="AddVisit" tabs={false} hideNavBar={navStatus} component={AddVisit} />
                    <Scene key="EditNotification" tabs={false} hideNavBar={navStatus} component={EditNotification} />
                    <Scene key="EditVisit" tabs={false} hideNavBar={navStatus} component={EditVisit} />
                    <Scene key="FatherProfileEdit" tabs={false} hideNavBar={navStatus} component={FatherProfileEdit} />
                    <Scene key="EditFamily" tabs={false} hideNavBar={navStatus} component={EditFamily} />
                    <Scene key="FamilyList" tabs={false} hideNavBar={navStatus} component={FamilyList} />

                    <Scene key="Visitlist" tabs={false} hideNavBar={navStatus} component={Visitlist} />

                </Stack>
                
            );
        } else {
            return (
                <Stack key="root">
                    <Scene key='tabbar' tabs={true} hideNavBar tabBarStyle={{ backgroundColor: '#000'}} showLabel={false} >
                    
                            <Scene
                                key="Home"
                                component={Home}
                                // initial
                                 initial={this.state.status_home}
                                title="Home" 
                                hideNavBar    
                                iconName="home"
                                showLabel={false}
                                icon={TabIcon}
                            />

                            <Scene
                                key="UserList"
                                component={UserList}
                                title="UserList" 
                                hideNavBar  
                                iconName="address-book"
                                showLabel={false}
                                icon={TabIcon}  
                                onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />
                              <Scene
                                key="MemberSearch"
                                component={MemberSearch}
                                title="MemberSearch" 
                                hideNavBar  
                                iconName1="blood-bag"
                                showLabel={false}
                                icon={TabIcon}  
                                onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />

                            <Scene
                                key="Payment"
                                component={Amount}
                                title="Payment" 
                                hideNavBar   
                                iconName="credit-card"
                                showLabel={false}
                                icon={TabIcon} 
                                onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />

                              <Scene
                                key="EventAll"
                                component={EventAll}
                                title="EventAll" 
                                hideNavBar   
                                iconName="calendar"
                                showLabel={false}
                                icon={TabIcon} 
                                onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />
                            <Scene
                                key="Profile"
                                component={Profile}
                                title="Profile" 
                                hideNavBar   
                                iconName="sliders"
                                showLabel={false}
                                icon={TabIcon} 
                                onEnter={() => { Actions.refresh({key: Math.random()})}}
                            />
                        
                        </Scene>

                    <Scene key='DetailedUser' tabs={false} component={MemberDetail} />
                    <Scene key='Login' tabs={false} component={Login} />
                    <Scene key='ChooseChurch' tabs={false} component={ChurchChoose} hideNavBar initial={this.state.status_church}/>
                    <Scene key="detailedNotification" tabs={false} component={DetailedNotification} hideNavBar />
                    <Scene key="ChurchList" tabs={false} component={ChurchList} />
                    <Scene key="EditProfile" tabs={false} component={EditProfile} />
                    <Scene key="familyPhoto" tabs={false} component={FamilyPhoto} />

                    {/* ===================================  Akhila  ===================================== */}

                    <Scene key="ResetPswd" tabs={false} component={ResetPswd} />
                    <Scene key="PhoneCheck" tabs={false} component={PhoneCheck} />
                    <Scene key="GenerateOTP" tabs={false} component={GenerateOTP} />

                    </Stack>
                   
            );
        }
    }
   
    render() {

        return (
            <Router>
                {this.renderTabBar()}
            </Router>
        );
    }
}
// /* eslint-disable prettier/prettier */
// import React, { Component } from 'react';
// import { Platform, ToastAndroid, BackHandler ,AsyncStorage,View,Image,Text} from 'react-native';

// import { Actions, Router, Stack, Scene } from 'react-native-router-flux';
// import Home from '../Scenes/Home/Home';
// import TabIcon from './TabIcon';
// import MemberDetail from '../Scenes/Members/MemberDetail';
// import Profile from '../Scenes/Profile/Main';
// import UserList from '../Scenes/UsersList/UserList';
// import MemberSearch from '../Scenes/UsersList/MemberSearch';

// import EventAll from '../Scenes/EventUsers/EventAll';
// import Amount from '../Scenes/finance/Amount';
// import DetailedNotification from '../Scenes/NotificationsDetail/Head';
// import Login from '../Scenes/Login/Login';
// import ChurchChoose from '../Scenes/Login/ChooseChurch';
// import ChurchList from '../Scenes/Login/ChurchList';
// import EditProfile from '../Scenes/Profile/EditProfile';
// import Notification_Visits from '../priest/Notification_Visits/Notifications_Visits';
// import FamilyPhoto from '../Scenes/Profile/familyPhoto';
// import PriestProfile from '../priest/Profile/FatherProfile';

// // -------------------ameen-------------------------------

// import ViewNotification from '../priest/Notification/ViewNotification';
// import AddNotification from '../priest/Notification/AddNotification';
// import ViewVisit from '../priest/Visit/ViewVisit';
// import FatherUserList from '../priest/Visit/FatherUserList';
// import AddVisit from '../priest/Visit/AddVisit';
// import FatherUserList1 from '../priest/Visit/FatherUserLis1';
// import EditNotification from '../priest/Notification/EditNotification';
// import EditVisit from '../priest/Visit/EditVisit';
// import Visitlist from '../priest/Visit/Visitlist';

// import Events from '../priest/Events/Events';
// import FatherProfileEdit from '../priest/Profile/FatherProfileEdit';
// import EditFamily from '../priest/Profile/EditFamily';
// import FamilyList from '../priest/Profile/familyList';

// // -------------------Akhila-------------------------------

// import ResetPswd from '../Scenes/Login/ResetPswd'
// import PhoneCheck from '../Scenes/Login/PhoneCheck'
// import GenerateOTP from '../Scenes/Login/GenerateOTP'



// const LOGIN_CREDENTIALS = '@login_credentials';

// export default class Routers extends Component {
    
//     constructor() {
//         super();

//         this.state = {
//             userRole: '',
//             abc:'',
//             abc1:'',
//             status_church:'',
//             status_home:'',
    
           
        

//         }
//     }
    
//       componentDidMount() {

//         this.checkUserIsLoggedIn();
        

//     }
   
    
//     async checkUserIsLoggedIn() {
//         try {
//             var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
//             if(loginCredentials) {
//                 // console.log(loginCredentials);
//                 var login_credentials = JSON.parse(loginCredentials);
//                 // console.log(login_credentials.role);
//                 this.setState({ userRole: login_credentials.role });
//                 this.setState({ status_church: false, status_home: true });


//                  Actions.Home();
               
//             }  else {
               
//                 this.setState({ status_church: true, status_home: false });

//             }
//         } 
//         catch (error) {
//             //console.error('Something went Wrong in Get Section' + error);
//         }
//     }
     
//      renderTabBar() {


//          if (Platform.OS === 'ios') {
//             var navStatus = false;
//          } else {
//             var navStatus = true;
//          }
         
//         if (this.state.userRole === 'priest') {
//             return (
               
//                 <Stack key="root">
//                     <Scene key='tabbar' tabs={true} hideNavBar tabBarStyle={{ backgroundColor: '#000'}} showLabel={false} >
                    
//                             <Scene
//                                 key="Home"
                    
//                                 initial={this.state.status_home}
//                                 component={Home}
//                                 title="Home" 
//                                 hideNavBar  
//                                 iconName="home"
//                                 showLabel={false}
//                                 icon={TabIcon}  
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />

//                             <Scene
//                                 key="UserList"
//                                 component={UserList}
//                                 title="UserList" 
//                                 hideNavBar  
//                                 iconName="address-book"
//                                 showLabel={false}
//                                 icon={TabIcon}  
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />
//                                <Scene
//                                 key="MemberSearch"
//                                 component={MemberSearch}
//                                 title="MemberSearch" 
//                                 hideNavBar  
//                                 iconName1="blood-bag"
//                                 showLabel={false}
//                                 icon={TabIcon}  
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />

//                             <Scene
//                                 key="Events"
//                                 component={Events}
//                                 title="Events" 
//                                 hideNavBar  
//                                 iconName="calendar"
//                                 showLabel={false}
//                                 icon={TabIcon}  
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />
                           

//                             <Scene
//                                 key="Profile"
//                                 component={PriestProfile}
//                                 title="Profile" 
//                                 hideNavBar   
//                                 iconName="sliders"
//                                 showLabel={false}
//                                 icon={TabIcon} 
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />
//                     </Scene>

//                     <Scene key='DetailedUser' tabs={false} hideNavBar={navStatus} component={MemberDetail} />
//                     <Scene key='Login' tabs={false} hideNavBar={navStatus} component={Login} />
//                     <Scene key='ChooseChurch' tabs={false} hideNavBar={navStatus} component={ChurchChoose} hideNavBar initial={this.state.status_church} />
//                     <Scene key="detailedNotification" tabs={false} hideNavBar={navStatus} component={DetailedNotification} />
//                     <Scene key="ChurchList" tabs={false} hideNavBar={navStatus} component={ChurchList} />
//                     <Scene key="EditProfile" tabs={false} hideNavBar={navStatus} component={EditProfile} />

//                      {/* ===================================  Akhila  ===================================== */}

//                     <Scene key="ResetPswd" tabs={false} component={ResetPswd} />
//                     <Scene key="PhoneCheck" tabs={false} component={PhoneCheck} />
//                     <Scene key="GenerateOTP" tabs={false} component={GenerateOTP} />

//                     {/* ===================================  Ameen  ===================================== */}

//                     <Scene key="VisitNotification" tabs={false} hideNavBar component={Notification_Visits}  />
//                     <Scene key="ViewNotification" tabs={false} hideNavBar={navStatus} component={ViewNotification} />
//                     <Scene key="AddNotification" tabs={false} hideNavBar={navStatus} component={AddNotification} />
//                     <Scene key="ViewVisit" tabs={false} hideNavBar={navStatus} component={ViewVisit} />
//                     <Scene key="FatherUserList" tabs={false} hideNavBar={navStatus} component={FatherUserList} />
//                     <Scene key="FatherUserList1" tabs={false} hideNavBar={navStatus} component={FatherUserList1} />
//                     <Scene key="AddVisit" tabs={false} hideNavBar={navStatus} component={AddVisit} />
//                     <Scene key="Visitlist" tabs={false} hideNavBar={navStatus} component={Visitlist} />

//                     <Scene key="EditNotification" tabs={false} hideNavBar={navStatus} component={EditNotification} />
//                     <Scene key="EditVisit" tabs={false} hideNavBar={navStatus} component={EditVisit} />
//                     <Scene key="FatherProfileEdit" tabs={false} hideNavBar={navStatus} component={FatherProfileEdit} />
//                     <Scene key="EditFamily" tabs={false} hideNavBar={navStatus} component={EditFamily} />
//                     <Scene key="FamilyList" tabs={false} hideNavBar={navStatus} component={FamilyList} />


//                 </Stack>
                
//             );
//         } else {
//             return (
//                 <Stack key="root">
//                     <Scene key='tabbar' tabs={true} hideNavBar  tabBarStyle={{ backgroundColor: '#000'}} showLabel={false} >
                    
//                             <Scene
//                                 key="Home"
//                                 component={Home}
//                                 // initial
//                                 initial={this.state.status_home}

//                                 title="Home" 
//                                 hideNavBar    
//                                 iconName="home"
//                                 showLabel={false}
//                                 icon={TabIcon}
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />

//                             <Scene
//                                 key="UserList"

//                                 component={UserList}
//                                 title="UserList" 
//                                 hideNavBar  
//                                 iconName="address-book"
//                                 showLabel={false}
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                                 icon={TabIcon}  
//                             />
//                               <Scene
//                                 key="MemberSearch"
//                                 component={MemberSearch}
//                                 title="MemberSearch" 
//                                 hideNavBar  
//                                 iconName1="blood-bag"
//                                 showLabel={false}
//                                 icon={TabIcon}
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  
//                                 // OnPress  ={() => { Actions.refresh({ key: 'MemberSearch' }) }}

//                             />

//                             <Scene
//                                 key="Payment"
//                                 component={Amount}
//                                 title="Payment" 
//                                 hideNavBar   
//                                 iconName="credit-card"
//                                 showLabel={false}
//                                 icon={TabIcon} 
//                                 type='reset'
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />


//                               <Scene

//                                 key="EventAll"
//                                 component={EventAll}
//                                 title="EventAll" 
//                                 hideNavBar   
//                                 iconName="calendar"
//                                 showLabel={false}
//                                 icon={TabIcon} 
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />
//                             <Scene
//                                 key="Profile"
//                                 component={Profile}
//                                 title="Profile" 
//                                 hideNavBar   
//                                 iconName="sliders"
//                                 showLabel={false}
//                                 icon={TabIcon} 
//                                 onEnter={() => { Actions.refresh({key: Math.random()})}}  

//                             />
                        
//                         </Scene>

//                     <Scene key='DetailedUser' tabs={false} component={MemberDetail} hideNavBar />
//                     <Scene key='Login' tabs={false} component={Login} hideNavBar />
//                     <Scene key='ChooseChurch' tabs={false} component={ChurchChoose} hideNavBar initial={this.state.status_church}/>
//                     <Scene key="detailedNotification" tabs={false} component={DetailedNotification} hideNavBar />
//                     <Scene key="ChurchList" tabs={false} component={ChurchList} hideNavBar/>
//                     <Scene key="EditProfile" tabs={false} component={EditProfile} hideNavBar/>
//                     <Scene key="familyPhoto" tabs={false} component={FamilyPhoto} hideNavBar />

//                     {/* ===================================  Akhila  ===================================== */}

//                     <Scene key="ResetPswd" tabs={false} component={ResetPswd} />
//                     <Scene key="PhoneCheck" tabs={false} component={PhoneCheck} />
//                     <Scene key="GenerateOTP" tabs={false} component={GenerateOTP} />

//                     </Stack>
                   
//             );
//         }
//     }
   
//     render() {

//         return (
//             <Router>
//                 {this.renderTabBar()}
//             </Router>
//         );
//     }
// }