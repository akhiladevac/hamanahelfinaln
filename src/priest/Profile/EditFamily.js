/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { TextInput, Platform, View, Text, Alert, Linking, Image,BackHandler,NetInfo, ScrollView, ActivityIndicator, TouchableOpacity, FlatList } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5';

const DATA_BASE_URL = '@base_url';

export default class EditFamily extends Component {

    constructor() {
        super();

        this.state = {
            loadingStatus: false, 
            loadingButton: false,
            phone: '',
            phoneTemp: ''  ,
            baseUrl: '',   
            connection_Status : '',
            settimeout:false,
            loadingStatus: false,
          };

        this.arrayholder = [];
    }

    componentDidMount() {
        this.checkNetwork();

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

        console.log(this.props.fam_details);
        this.setState({
            phone: this.props.fam_details.members[0].phone,
            phoneTemp: this.props.fam_details.members[0].phone
        });
        this.getBaseUrl();
    }  
    checkNetwork=() => {
        NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
      
      );
      
      NetInfo.isConnected.fetch().done((isConnected) => {
      
        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
      
      
      
        }
      
      });
      
      }
      updateData = () => {
        this.props.parentCallback("updated");
   }
      componentWillUnmount() {
        this.backHandler.remove();
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
      
        );
      
      }
      
      _handleConnectivityChange = (isConnected) => {
      
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
      
          }
          else
          {
            this.setState({connection_Status : "Offline"})
      
            
      
      
          }
      };
       
       
       handleBackPress = () => {
        
         if (Actions.currentScene !== 'EditFamily') {
         } 
         else 
         {
             Actions.pop();
 
       
           return true;
         }
       }

    async getBaseUrl() {
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            this.setState({ baseUrl: baseUrl });
            
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
      }
    
    updateButton(){
        if ((this.state.phone)==(this.state.phoneTemp))
        {
            Alert.alert('Number already exist');
        }
        else
        {
            this.updateNumber();
        }  
    }

    updateNumber() {
        
            this.updateNumberApiCall(this.state.baseUrl);
    }

    updateNumberApiCall(baseUrl) {
        this.setState({ loadingStatus: true })
        const { phone } = this.state;
        const { Fam_Id, members } = this.props.fam_details;
        
        var endpoint = 'edit_family_phone.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', Fam_Id);
        data.append('Person_Id', members[0].Person_Id);
        data.append('phone', phone);
        console.log(data);
        fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data,
            })
            .then((response) => response.json())
            .then((responseJson) => {
    
                if (responseJson.status === 200) {
                  //this.setState({ response: responseJson.response[0], loadingStatus: false });
                  this.setState({ loadingStatus: false })
                  Alert.alert(responseJson.Message);
                   this.updateData();
                  Actions.pop();
                }
                else {
                    this.setState({ loadingStatus: false })
                const error = 'Newtork error';
                }
            })
            .catch((error) => {
                console.error(error);
            });

            setTimeout(() => {
                this.setState({
                    loadingStatus:false,
                    settimeout:true
                })
                },45000)
    }
    renderUpdateButton() {
        if (this.state.loadingStatus) {
            return (
                
                
                    <View style={{ width: '100%'}}> 
                        <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size='small' />
                        </View>
                    </View>
               
            )
        } else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){
    
    return (
        <TouchableOpacity onPress={ () => this.updateButton()}>
            <View style={{ width: '100%'}}> 
                <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 15, fontWeight: '500' }}> You are offline </Text>
                    <Icon style={{ marginRight:0}} name="undo" size={17} color="#fff" />
    
                </View>
            </View>
        </TouchableOpacity>
    )  
    }else{
        
            return (
              <View style={{ width: '100%'}}> 
              <TouchableOpacity onPress={ () => this.updateButton()} style={{backgroundColor: '#7BCFE8',height: 60, alignItems: 'center', justifyContent: 'center',   borderRightWidth: 1}}>
                  <Text style={{ fontSize: 17, fontWeight: 'bold'}}>
                      UPDATE
                  </Text>
              </TouchableOpacity>
              </View>
              
            )
        
        }
    }

        render() {
            const { loadingStatus } = this.state;
            if(loadingStatus) {
              return (
                <View style={{ flex: 1, justifyContent: 'center', marginTop: 300}}>
                   <ActivityIndicator size='large'/>
                </View>
               
              );
            }
            return(
            <View style={{ flex: 1 }}>
             <ScrollView showsVerticalScrollIndicator={false}>
               <View style= {{ alignItems: 'center', backgroundColor:'#7BCFE8',height: 320, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
                    <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8',height: 280, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                        <Image
                            style={{ height: 101, width: 101, }}
                            source={require('../../img/avatar.png')}
                        /> 
                        <Text style= {{ fontSize: 22, fontWeight: 'bold', marginTop: 10 }}>{this.props.fam_details.Fam_Owner}</Text>     
                        <Text style= {{ fontSize: 13, marginTop: 8 }}> {this.props.fam_details.Fam_Name} House</Text>
                        <Text style= {{ fontSize: 10, marginTop: 5 }}> {this.props.fam_details.members[0].Person_Occ}</Text> 
                         
                    </View>    
                </View>
                <View style={{ alignItems: 'center', width: '100%' }}>

                    <View style={{ backgroundColor: '#fff', height: 100, borderRadius: 10, marginTop: 20, width: '95%', }}>
                        <Text style={{ paddingTop: 10, paddingLeft: 20, fontWeight: '600' }}>Family Mobile Number</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <TextInput
                                style={{ paddingTop: 20, paddingLeft: 20, width: '80%', paddingRight: 20, fontSize: 17, fontWeight: '300' }} 
                                value={this.state.phone}
                                keyboardType='number-pad'
                                onChangeText={(text) => this.setState({ phone: text })}
                            />
                        </View>
                    </View>
                </View>
                
                    {/* <List abc={this.props.fam_details.members} /> */}
              </ScrollView>
              {this.renderUpdateButton()}


              {/* <TouchableOpacity onPress={() => this.updateButton()} style={{backgroundColor: '#7BCFE8',height: 60, alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{ fontSize: 17, fontWeight: 'bold'}}>
                        UPDATE
                    </Text>
                </TouchableOpacity> */}
            </View>
            );

        
        }
    }