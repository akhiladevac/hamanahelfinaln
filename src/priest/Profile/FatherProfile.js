/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, Alert, Linking, Image, ScrollView, NetInfo,ActivityIndicator, TouchableOpacity,TextInput,BackHandler } from 'react-native';
import  {Actions} from 'react-native-router-flux'
import AsyncStorage from '@react-native-community/async-storage';
const DATA_BASE_URL = '@base_url';
const DATA_PROFILE = '@profile_list';

const LOGIN_CREDENTIALS = '@login_credentials';
const USERNAME = '@username_credential';

import Icon from 'react-native-vector-icons/FontAwesome';
import FatherProfileEdit from '../Profile/FatherProfileEdit';



import Dialog, { DialogTitle, DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';


export default class FatherProfile extends Component {


    constructor() {
        super();
        this.callApiProfileUpdate = this.callApiProfileUpdate.bind(this)
       
                this.state = {
                  username:'',
            response: [],
            person_id:'',
            loadingStatus: false,
            baseUrl: '',
            fam_id: '',
            username: '',
            password: '',
            connection_Status : '',
            settimeout:false,
            visibleOtp: false,
            visibleNewPassword: false,

            passwordRe: '',
            passwordReCon: '',

            forgotEmail: '',
            forgetusername: '',
            forgetoldpassword:'',
            forgetnewpassword:'',
            forgetconfirmpassword:'',
            responseprofile:[],
            data:[],
            temp:[]
        }
        this.arrayholder = []; 

    }

    componentWillMount() {

        //this.profileDetails();
        // this.getBaseUrl();
    }
    
    
  
     
    componentWillUnmount() {
        this.backHandler.remove();
       }
       handleBackPress = () => {
       
        if (Actions.currentScene !== '_Profile') {
            
        } else {
            this.setState({ visibleEmail: false })

            this.setState({ visibleOtp: false })
            this.setState({ visibleNewPassword: false })

          Actions.pop();
      
          return true;
        }
      }
    renderResetPassword() {
      return (
          <View>
          <Dialog
              visible={this.state.visibleOtp}
              dialogTitle={<DialogTitle title="Reset Password" />}
              footer={
                  <DialogFooter>
                      <DialogButton
                      text="CANCEL"
                      onPress={() => { this.setState({ visibleOtp: false })}}
                      />
                      <DialogButton
                      text="SUBMIT"
                      onPress={() => { this.resetPasswordSubmitButton()}}
                      //onPress={() => { this.sendContactForm() }}
                      />
                  </DialogFooter>
                  }
              >
              <DialogContent>
  
                  <View style={{ width: 300, alignItems: 'center' }}>
  
                      {/* <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                      
                         
                           <TextInput 
                              placeholder='Enter Username'
                              value={this.state.forgetusername}
                              style={{ color: '#000', paddingLeft:20 }}
                              onChangeText={(text) => this.setState({ forgetusername: text })}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />
                         
                      </View> */}
                      <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                          <TextInput 
                              placeholder='Enter Old Password'
                              value={this.state.forgetoldpassword}
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={(text) => this.setState({ forgetoldpassword: text })}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />
                          
                      </View>
                      <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                          <TextInput 
                              placeholder='Enter new Password'
                              value={this.state.forgetnewpassword}
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={(text) => this.setState({ forgetnewpassword: text })}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />
                          
                      </View>
                      <View style={{ marginTop: 30, width: 300, height: 40, backgroundColor: '#cfcdcc', justifyContent: 'center', borderRadius: 5 }}>
                          <TextInput 
                              placeholder='Enter confirm Password'
                              value={this.state.forgetconfirmpassword}
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={(text) => this.setState({ forgetconfirmpassword: text })}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />
                          
                      </View>
                      
      
                  </View>
                  {this.renderActivityIndicator()}
              </DialogContent>
          </Dialog>
      </View>
      )
  }

apiCall() {
   Actions.FatherProfileEdit({data: this.state.data, parentCallback: this.callApiProfileUpdate})
 
}
  NewPassword = async () => {
    this.setState({ loadingIndicator: true });
    // alert('priest_'+this.state.response.Phone)
    const {  forgetnewpassword,forgetoldpassword,baseUrl,fam_id ,person_id} = this.state;
    var endpoint = 'priest_reset_password.php';
    var url = baseUrl + endpoint;
    var data = new FormData()
    data.append('hashcode', '##church00@');
    data.append('Person_Id', person_id);
     data.append('username','priest_'+this.state.response.Phone);
    data.append('old_password', forgetoldpassword);
    data.append('new_password', forgetnewpassword);
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
      Alert.alert(responseJson.Message);

        if (responseJson.status === 200) {
            this.setState({visibleOtp:false, visibleNewPassword: false, loadingIndicator: false ,});
        } else {
            this.setState({ loadingIndicator: false });
        }
    })
    .catch((error) => {
      console.error(error);
    });
    setTimeout(() => {
        this.setState({
            loadingStatus:false,
            settimeout:true
        })
        },45000)
}

  resetPasswordSubmitButton() {
    if (this.state.username && this.state.forgetoldpassword && this.state.forgetnewpassword && this.state.forgetconfirmpassword) {
        if(this.state.forgetnewpassword == this.state.forgetconfirmpassword){
          this.NewPassword();

        }
        else{
          Alert.alert('Password do not match');
        }
    } else {
        Alert.alert('Enter all fields');
    }
}


checkNetwork=() => {
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange

  );
 
  NetInfo.isConnected.fetch().done((isConnected) => {

    if(isConnected == true)
    {
      this.setState({connection_Status : "Online"})
    }
    else
    {
      this.setState({connection_Status : "Offline"})



    }

  });

  }

componentWillUnmount() {
    this.backHandler.remove();
    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }
 
  _handleConnectivityChange = (isConnected) => {
 
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})

      }
      else
      {
        this.setState({connection_Status : "Offline"})

        


      }
  };
    componentDidMount() {
    
        this.checkNetwork();
         this.getProfile();
         this.getUpdatedProfile();
        this.getBaseUrl();
          // this.getUpdatedProfile();


        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     
  }
  getUpdatedProfile(){
    this.getBaseUrl();
  }
  async getProfile() {
    this.setState({ loadingStatus: true });
    try {
        const ProfileList = await AsyncStorage.getItem(DATA_PROFILE);
        if (ProfileList !== null) {
          const abc = JSON.parse(ProfileList);

        this.setState({          
            data: abc,  
            temp: abc,        
                  
            loadingStatus: false,
               
          });  
          this.arrayholder = abc; 
          
        } 
    } catch (error) {
        console.log('Something went Wrong Saving list');
    }
  }
renderActivityIndicator() {
    if (this.state.loadingStatus) {
        return (
                <ActivityIndicator size='small' />
        );
    }
    else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline'  )
    {
        return (
            <View style={{ marginLeft:100}}>
            <Text >You are offline</Text>
            <TouchableOpacity onPress={() => this.NewPassword (this.state.baseUrl)}>
            <Icon style={{ marginLeft:40}} name="undo" size={17} color="#7BCFE8" />
            </TouchableOpacity>
            </View>
        )
    }else{

    }
  }
  
  callApiProfileUpdate(){
    if(this.state.connection_Status=='Online'){
      this.getBaseUrl();

    }else{
      
    }

  }
    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          //let userDetaisl = await AsyncStorage.getItem(DATA_BASE_URL);
          var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
          var login_credentials = JSON.parse(loginCredentials);
          this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id,person_id: login_credentials.person_id,username:login_credentials.username});
      //  var username = await AsyncStorage.getItem(USERNAME);
      //  var username_credential = JSON.parse(username);

          // alert(username_credential)
          if(this.state.connection_Status=='Online'){
            this.callApiProfilePriest(baseUrl);

          }else{

          }
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
    }

      callApiProfilePriest(baseUrl, fam_id) {
       

        //const { baseUrl, fam_id } = this.state;
        // this.setState({ loadingStatus: true });
        var endpoint = 'priest_profile.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Person_Id', this.state.person_id);
        
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {

            if (responseJson.status === 200) {
              this.setState({ response: responseJson.response[0], loadingStatus: false });
              // this.chechVariations();
              this.chechVariations();


            }


            else {
            const error = 'Newtork error';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    async chechVariations() {
      try {
          const ProfileList = await AsyncStorage.getItem(DATA_PROFILE);
          if (ProfileList !== null) {
            const abc = JSON.parse(ProfileList);
          
            this.setState({          
                data: abc,  
                temp: abc,        
                error: null,          
                loading: false,
                query: '',        
              });  
              this.arrayholder = abc; 
            } 

            if(JSON.stringify(this.state.response) == JSON.stringify(this.state.data))
            { }else{
              this.updateAsyncValues(this.state.response,DATA_PROFILE);
            }
        
      } catch (error) {
          console.log('Something went Wrong Saving list');
      }
  }

  async updateAsyncValues(response, KEY) {
    await AsyncStorage.setItem(KEY, JSON.stringify(response));
    this.getProfile();
  }
    
      logout() {
            Alert.alert(  
              'Logout',  
              'Do you want to logout',  
              [  
                  {  
                      text: 'Cancel',  
                      onPress: () => console.log('Cancel Pressed'),  
                      style: 'cancel',  
                  },  
                  {text: 'OK', onPress: () => this.clearAsyncStorage()},  
              ]  
          );  
      }

      clearAsyncStorage() {
        AsyncStorage.clear();
        Actions.ChooseChurch();
      }

      

        render() {

            console.log('test 009');
            console.log(this.state.response);
            const { loadingStatus, response,data } = this.state;
            // if(loadingStatus) {
            //   return (
            //     <View style={{ flex: 1, justifyContent: 'center', marginTop: 300}}>
            //        <ActivityIndicator size='large'/>
            //     </View>
               
            //   );
            // }
            if (data.photo) {
                var profPic =  { uri: this.state.baseUrl + data.photo };
            } else {
                    var profPic = require('../../img/man.png');
                }

            

            return(

             <ScrollView showsVerticalScrollIndicator={false}>

               <View style={{ backgroundColor:'#7BCFE8',height: 360, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
                    {this.renderResetPassword()} 
                  <TouchableOpacity onPress={() => this.logout()}>
                    <View style={{ marginLeft: '75%', opacity: 0.6, position: 'relative', marginTop: 50, height: 30, width: 80, backgroundColor: '#000', borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}>
                          <Text style={{ color: '#fff' }}>Logout</Text>
                    </View>
                  </TouchableOpacity>
                  <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8', borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                        <Image
                            style={{ height: 101, width: 101, borderRadius: 50 }}
                            source={profPic}
                        /> 
                        <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: 10}}>{data.Priest_Name}</Text>     
                        {/* <Text style={{ fontSize: 10, marginTop: 5 }}> {}</Text>  */}
                        <Text style={{ fontSize: 13, marginTop: 10 }}> {data.Phone} </Text> 
                    </View>
                    <View style={{ alignItems: 'center', width: '100%'}}>
                      <View style= {{ marginTop: 40, flexDirection: 'row'}}>
                              
                              
                        </View>
                        
                      </View>
                      
                       
                </View>
                <View style= {{ backgroundColor: '#DBDBDB', marginTop: 10, borderRadius:20, height: 330}}>
                    <TouchableOpacity 
// onPress={()=>Actions.FatherProfileEdit({data:response},
  onPress={() => this.apiCall()}
  //  {functionPropNameHere:this.callApiProfile})} 
style= {{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', marginTop: 15, marginLeft: 10, marginRight: 10, borderRadius:15, height: 45}}>
                        <Text style={{fontSize:15, fontWeight:'bold'}}>Edit Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>Actions.FamilyList()} style= {{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius:15, height: 45}}>
                        <Text style={{fontSize:15, fontWeight:'bold'}}>Edit Family</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({visibleOtp:true})} style= {{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius:15, height: 45}}>
                        <Text style={{fontSize:15, fontWeight:'bold'}}>Reset Password</Text>
                    </TouchableOpacity>
                </View>
              </ScrollView>
              

            );

        
        }
    }