import React, { Component } from 'react';
import { View, Text , Image, ScrollView, TouchableOpacity, TextInput,NetInfo, BackHandler,Helper, Alert, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
const LOGIN_CREDENTIALS = '@login_credentials';

const DATA_BASE_URL = '@base_url';


export default class FatherProfileEdit extends Component {
    constructor() {
        super();

        this.state = {
           
            image: '',
            imageVisible: false,
            mobile: '',
            imageValue: '',
            loadingStatus: false,
            name: '',
            baseUrl: '',
            abc:'',
           Fam_Id:0,
            connection_Status : '',
            settimeout:false,
            email:'',
            baseUrl:'',
            person_id:'',
            validate:false




        }
    }
    sendData = () => {
        this.props.parentCallback("juhiu");
   }
//    async getBaseUrl() {
//     try {
//         let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
//         //let userDetaisl = await AsyncStorage.getItem(DATA_BASE_URL);
//         var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
//         var login_credentials = JSON.parse(loginCredentials);
//         this.setState({ baseUrl: baseUrl, person_id: login_credentials.person_id});
        
//     } catch (error) {
//         console.error('Something went Wrong in Get Section' + error);
//     }
//   }
    componentDidMount() {
        this.checkNetwork();
        this.getBaseUrl();
         this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     
        console.log(Actions.currentScene)
        if (this.props.data.Phone == '0') {
            var mobileNumber = '';
        } else {
            var mobileNumber = this.props.data.Phone;
        }
        
        this.setState({
           
            mobile: mobileNumber,
            name: this.props.data.Priest_Name,
            email:this.props.data.members[0].Email
        });
        console.log('profiles')
        console.log(this.props.data);
        this.getBaseUrl();

    }
    checkNetwork=() => {
        NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
    
      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {
    
        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
    
    
    
        }
    
      });
    
      }
      static isEmailValid(email) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return reg.test(email) == 0;
   }
     
    componentWillUnmount() {
        this.backHandler.remove();
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
    
          }
          else
          {
            this.setState({connection_Status : "Offline"})
    
            
    
    
          }
      };
     
      handleBackPress = () => {

        if (Actions.currentScene !== 'FatherProfileEdit') {
        } 
        else 
        {
            
             
            Actions.pop()
      
          return true;
        }
      }
    async getBaseUrl() {
        try {
            // let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            // this.setState({ baseUrl: baseUrl });
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            //let userDetaisl = await AsyncStorage.getItem(DATA_BASE_URL);
            var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
            var login_credentials = JSON.parse(loginCredentials);
            this.setState({ baseUrl: baseUrl, person_id: login_credentials.person_id});
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
      }

    ProfilePictureAdd = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
          };
          ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else {
              const source = { uri: response.uri };
          
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
              console.log(response);
          
              this.setState({
                image: response.uri,
              });
            }
          })
    };
    
    
       
        validate = (text) => {
            console.log(text);
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
            if(reg.test(text) === false)
            {
                Alert.alert(  
                    ' ',  
                    'Enter a valid Email id',  
                    [  
                        {  
                           
                        },  
                        {text: 'ok', },  
                    ]  
                ); 
        
          
            this.setState({email:text})
            return false;
              }
            else {
              this.setState({email:text,validate:true})
              this.updateProfileApiCall(this.state.baseUrl);
            }
        }
            
    updateProfile() {
        

        const { name, mobile ,email} = this.state;
        if(email){
            this.validate(email)

        }else{
            this.updateProfileApiCall(this.state.baseUrl);

        }
        // console.log(email + ' ' + imageVisible + ' ' + mobileVisible + ' ' + mobile);
        // if (imageVisible && mobileVisible) {


        // } else {
        //     Alert.alert('Fields are empty');
        // }
    }
    UpdateData = () => {
        this.props.parentCallback("updated");
      
      }
  
    updateProfileApiCall(baseUrl) {

        this.setState({ loadingStatus: true })
        const { name,  mobile, image,email ,person_id} = this.state;
        var endpoint = 'edit_priest_profile.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Person_Id', person_id);
        data.append('name', name);
        data.append('phone', mobile);
        data.append('email', email);

        if (image) {
            data.append('file', {
                uri: image,
                type: 'image/jpg',
                name: 'person_photo.jpg'
                
              });
        } else {
            data.append('file', '');
        }
        console.log(data);
        fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data,
            })
            .then((response) => response.json())
            .then((responseJson) => {
    
                if (responseJson.status === 200) {
                  //this.setState({ response: responseJson.response[0], loadingStatus: false });
                  this.setState({ loadingStatus: false })
                  Alert.alert(responseJson.Message);
                 this.sendData(this.state.baseUrl);
                 this.UpdateData();

                   Actions.pop();
                          
                }
                else {
                    this.setState({ loadingStatus: false })
                const error = 'Newtork error';
                }
            })
            .catch((error) => {
                console.error(error);
            });
    
            setTimeout(() => {
                this.setState({
                    loadingStatus:false,
                    settimeout:true
                })
                },45000)
        
    }

    renderUpdateButton() {
        if (this.state.loadingStatus) {
            return (
                
                
                    <View style={{ width: '100%'}}> 
                        <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                            <ActivityIndicator size='small' />
                        </View>
                    </View>
               
            )
        }else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline'  ){

            return (
                <TouchableOpacity onPress={() => this.updateProfile()}>
                    <View style={{ width: '100%'}}> 
                        <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, fontWeight: '500' }}> You are offline </Text>
                            <Icon style={{ marginRight:0}} name="undo" size={17} color="#fff" />
        
                        </View>
                    </View>
                </TouchableOpacity>
            )  
        } 
        else {
            return (
                <TouchableOpacity onPress={() => this.updateProfile()}>
                    <View style={{ width: '100%'}}> 
                        <View style={{ backgroundColor: '#7BCFE8', width: '100%', height: 60, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 15, fontWeight: '500' }}> UPDATE </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        
        }
    }

    render() {
        

        

        if (this.state.image) {
            var profPic =  { uri: this.state.image };
        } else {
            if (this.props.data.photo) {
                var profPic =  { uri: this.state.baseUrl + this.props.data.photo };
            } else {
                var profPic = require('../../img/man.png');
            }
        }
        
        return(

            <View style={{ flex: 1 }}>
            <ScrollView style={{ flex: 2 }}>
                <View style={{ backgroundColor:'#7BCFE8',height: 320, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
                    <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8', borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                        
                        <TouchableOpacity >
                            <Image
                                style={{ marginTop: 70, height: 101, width: 101, borderRadius: 50 }}
                                source={profPic}
                                //source={{ uri: this.state.image }}
                            />
                        </TouchableOpacity>
                            {/* <TouchableOpacity onPress={() => this.ProfilePictureAdd()}>
                                <View style={{ marginTop: -25, marginLeft: 60, height: 30, width: 30, borderRadius: 50, backgroundColor: '#808080', justifyContent: 'center', alignItems: 'center', }}>
                                    <Icon style={{ color: '#000'}} name='plus' size={20} />
                                </View> 
                            </TouchableOpacity> */}
            
                        <Text style={{ fontSize: 22, fontWeight: 'bold', marginTop: 10}}> {this.props.data.Priest_Name} </Text>     
                       
                    </View> 
                </View>

            <View style={{ alignItems: 'center', width: '100%' }}>

                <View style={{ backgroundColor: '#fff', height: 100, borderRadius: 10, marginTop: 20, width: '95%', }}>
                    <Text style={{ paddingTop: 10, paddingLeft: 20 }}>Name</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={{ paddingTop: 20, paddingLeft: 20, width: '80%', paddingRight: 20, fontSize: 15, fontWeight: '300' }} 
                            value={this.state.name}
                            onChangeText={(text) => this.setState({ name: text })}
                        />
                       
                    </View>
                </View>

                <View style={{ backgroundColor: '#fff', height: 100, borderRadius: 10, marginTop: 10, width: '95%', }}>
                    <Text style={{ paddingTop: 20, paddingLeft: 20 }}>Mobile Number</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={{ paddingTop: 10, paddingLeft: 20, width: '80%', paddingRight: 20, fontSize: 15, fontWeight: '300' }} 
                            placeholder="Mobile Number"
                            keyboardType='number-pad'
                            value={this.state.mobile}
                            onChangeText={(text) => this.setState({ mobile: text })}
                        />
                       
                    </View>
                    

                </View>
                <View style={{ backgroundColor: '#fff', height: 100, borderRadius: 10, marginTop: 10, width: '95%', }}>
                    <Text style={{ paddingTop: 10, paddingLeft: 20 }}>Email id</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            style={{ paddingTop: 20, paddingLeft: 20, width: '80%', paddingRight: 20, fontSize: 15, fontWeight: '300' }} 
                            placeholder="Email id"
                            value={this.state.email}
                            onChangeText={(text) => this.setState({ email: text })}
                        />
                       
                    </View>
                    

                </View>

                

            </View>
          </ScrollView>
                {this.renderUpdateButton()}
          </View>
        );
    }
}