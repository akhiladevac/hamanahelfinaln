
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, Dimensions,BackHandler, Switch,NetInfo, ActivityIndicator, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
const DATA_LIST_MEMBERS = '@members_list';
const DATA_BASE_URL = '@base_url';
const DATA_EVENT_LIST = '@eventlist_list';
const MAX_HEIGHT = Dimensions.get('window').height;
const MAX_WIDTH = Dimensions.get('window').width;
import Modal, { ModalContent } from 'react-native-modals';
const DATA_WISH_LIST = '@wish_list';

export default class Events extends Component {

    constructor() {
        super();

        this.state = {
          dataWish:[],
          dataEvent:[],
          propicture:'',
          slideAnimationDialog: false,
          loadingStatus: false,  
          baseUrl: '',    
            data: [],
            temp: [],      
            error: null,  
            switch: '',
            pageMode:true,
            modee:'',
            eventsData2: [],
            connection_Status : '',
            settimeout:false,
            loadingStatus1:true

             //true for events and false for wishlists  
          };

        this.arrayholder = [];
    }
    componentDidMount() {
      this.checkNetwork();
      
      this.getEventPriestList();
      this.getUpdatedEventList();
      this.getWishList();
      this.getUpdatedWishList();

      this.getBaseUrl();
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     


        //this.getMembersList();
    }
    getUpdatedEventList(){
        this.getBaseUrlEvent();

    
    }
    getUpdatedWishList(){
      if(this.state.connection_Status=='Online'){
        this.getBaseUrlWish();

      }
    }
    async getEventPriestList() {
      // this.setState({ loadingStatus: true });
      try {
          const eventPriestList = await AsyncStorage.getItem(DATA_EVENT_LIST);
          if (eventPriestList !== null) {
            const abc = JSON.parse(eventPriestList);
            
          this.setState({          
              dataEvent: abc,  
              temp: abc,        
              error: null,          
              loadingStatus: false,
                 
            });  
            this.arrayholder = abc; 
            
          } 
      } catch (error) {
          console.log('Something went Wrong Saving list');
      }
    }
    async getWishList() {
      this.setState({ loading: true });
      try {
          const wishList = await AsyncStorage.getItem(DATA_WISH_LIST);
          if (wishList !== null) {
            const abc = JSON.parse(wishList);
          this.setState({          
              dataWish: abc,  
              temp: abc,        
              error: null,          
              loading: false,
                 
            });  
            this.arrayholder = abc; 
            
          } 
      } catch (error) {
          console.log('Something went Wrong Saving list');
      }
    }
    componentWillUnmount() {
      this.backHandler.remove();
     }
     handleBackPress = () => {
     
      if (Actions.currentScene !== '_Events') {
          
      } else {
        if(this.state.slideAnimationDialog==true){
          this.setState({ slideAnimationDialog: false });

        }else{
          Actions.pop();
        }


        
    
        return true;
      }
    }
    async componentWillMount() {
     

      try {
        let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
        this.checkModeWish(baseUrl);
    } catch (error) {
        console.error('Something went Wrong in Get Section' + error);
    }
    }

   
    
    checkNetwork=() => {
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
  
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
  
      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
  
  
  
      }
  
    });
  
    }
    componentWillUnmount() {
  
      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
   
      );
   
    }
   
    _handleConnectivityChange = (isConnected) => {
   
      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
  
        }
        else
        {
          this.setState({connection_Status : "Offline"})
  
          
  
  
        }
    };
   
  
  async getBaseUrlEvent(){
    try {
      let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
      this.setState({ baseUrl: baseUrl });
   
      this.callApiEventsList(baseUrl);
      // this.callApiWishList(baseUrl);
  } catch (error) {
      console.error('Something went Wrong in Get Section' + error);
  }
}
async getBaseUrlWish(){
  try {
    let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
    this.setState({ baseUrl: baseUrl });
 
    // this.callApiEventsList(baseUrl);
     this.callApiWishList(baseUrl);
} catch (error) {
    console.error('Something went Wrong in Get Section' + error);
}
}
    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
        
          // this.callApiEventsList(baseUrl);
          // this.callApiWishList(baseUrl);
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
    }

    renderButton() {
      if (this.state.loadingStatus2) {
          return (
              
              
                  <View style={{ width: '100%'}}> 
                      <View style={{  width: '100%', height: 40, justifyContent: 'center', alignItems: 'center' }}>
                          <ActivityIndicator size='small' />
                      </View>
                  </View>
             
          )
      } else {
          return (

                <Switch
                                ios_backgroundColor={'rgba(0, 0, 0, 0.6)'}
                                thumbTintColor={'#fff'}
                                onTintColor={'#000'}
                                onValueChange = {() => this.changeEventMode()}
                                value = {this.state.switch}
                              /> 
                              
          )
      }
  }
callApiEventsList(baseUrl){
      this.setState({ });
      var endpoint = 'birthday_wish.php';
      var url = baseUrl + endpoint;
      // console.log(url);
      var data = new FormData();
      data.append('hashcode', '##church00@');
      fetch(url, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
      },
      body: data,
      })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log('bithday');
          // console.log(responseJson);
          if (responseJson.status === 200) {

              this.setState({ 
                eventsData2: responseJson.response,
                  settimeout:'false',
                  loadingStatus:false
              });

              this.chechVariations();

          } else {
            this.setState({ 
              
                loadingStatus:false
            });
              
          }
          // console.log(responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
      setTimeout(() => {
          this.setState({
              loadingStatus:false,
              settimeout:true
          })
          },45000)

  }
  async chechVariations() {
    try {
        const eventPriestList = await AsyncStorage.getItem(DATA_EVENT_LIST);
        if (eventPriestList !== null) {
          const abc = JSON.parse(eventPriestList);
        
          this.setState({          
              dataEvent: abc,  
              temp: abc,        
              error: null,          
              loading: false,
              query: '',        
            });  
            this.arrayholder = abc; 
          } 

          if(JSON.stringify(this.state.eventsData2) == JSON.stringify(this.state.dataEvent))
          { }else{
            this.updateAsyncValues(this.state.eventsData2,DATA_EVENT_LIST);
          }
      
    } catch (error) {
        console.log('Something went Wrong Saving list');
    }
}
async chechVariationsWish() {
  try {
      const wishList = await AsyncStorage.getItem(DATA_WISH_LIST);
      if (wishList !== null) {
        const abc = JSON.parse(wishList);
      
        this.setState({          
            dataWish: abc,  
            temp: abc,        
            error: null,          
            loading: false,
            query: '',        
          });  
          this.arrayholder = abc; 
        } 

        if(JSON.stringify(this.state.data) == JSON.stringify(this.state.dataWish))
        { }else{
          this.updateAsyncValuesWish(this.state.data,DATA_WISH_LIST);
        }
    
  } catch (error) {
      console.log('Something went Wrong Saving list');
  }
}
async updateAsyncValues(response, KEY) {
  await AsyncStorage.setItem(KEY, JSON.stringify(response));
  this.getEventPriestList();
}
async updateAsyncValuesWish(response, KEY) {
  await AsyncStorage.setItem(KEY, JSON.stringify(response));
  this.getWishList();
}
  checkModeWish(baseUrl) {
    //this.setState({ loadingStatus: true });
    var endpoint = 'view_mode.php';
    var url = baseUrl + endpoint;
    var data = new FormData();
    //console.log(url);
    data.append('hashcode', '##church00@');
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
      // console.log(responseJson);
        // console.log(responseJson);
        if (responseJson.status === 200) {

        

          this.setState({
            switch: responseJson.response[0].mode,
          });
          // if(this.state.modee == 'auto'){
          //   this.setState({
          //     switch:true,
          //   });

          // }

        } else {
          Alert.alert(responseJson.msg);
        }
    })
    .catch((error) => {
        console.error(error);
    });
  }

  callApiWishList(baseUrl) {
    
    // this.setState({ loadingStatus: true });
    var endpoint = 'wish_list.php';
    var url = baseUrl + endpoint;
    var data = new FormData();
    //console.log(url);
    data.append('hashcode', '##church00@');
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
        console.log('seeet2 : ' + responseJson);
        console.log(responseJson);
        if (responseJson.status === 200) {
          this.setState({
            loadingStatus: false,
            data: responseJson.response,
          })
          this.chechVariationsWish();

        } else {
          this.setState({
            loadingStatus: false,
          })
        }
    })
    .catch((error) => {
        console.error(error);
    });
    // setTimeout(() => {
    //   this.setState({
    //       loadingStatus:false,
    //       settimeout:true
    //   })
    //   },45000)
}

  individualWishApi(event_id) {
    const { baseUrl } = this.state;
    // this.setState({ loadingStatus: true });
    var endpoint = 'wish.php';
    var url = baseUrl + endpoint;
    var data = new FormData();
    //console.log(url);
    data.append('hashcode', '##church00@');
    data.append('event_id', event_id);
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
        //console.log(responseJson);
        if (responseJson.status === 200) {
          this.callApiEventsList(this.state.baseUrl);
        } else {
          this.setState({
            // loadingStatus: false,
          })
          const error = 'Error in response';
        }
    })
    .catch((error) => {
        console.error(error);
    });
}

changeWishModeApi() {
  const { baseUrl } = this.state;
  // this.setState({ loadingStatus: true });
  var endpoint = 'mode.php';
  var url = baseUrl + endpoint;
  var data = new FormData();
  //console.log(url);
  data.append('hashcode', '##church00@');
  fetch(url, {
  method: 'POST',
  headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
  },
  body: data,
  })
  .then((response) => response.json())
  .then((responseJson) => {
    // console.log(responseJson);
      // console.log(responseJson);
      
      if (responseJson.status === 200) {
        this.setState({
          switch: responseJson.response,
          // loadingStatus:false

        })
        this.checkModeWish(this.state.baseUrl);
      } else {
        this.setState({
          
          // loadingStatus:false

        })
      }
  })
  .catch((error) => {
      console.error(error);
  });
}

wishAllApi() {
  const { baseUrl } = this.state;
  this.setState({ loadingStatus: true });
  var endpoint = 'wish_all.php';
  var url = baseUrl + endpoint;
  var data = new FormData();
  console.log(url);
  data.append('hashcode', '##church00@');
  fetch(url, {
  method: 'POST',
  headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
  },
  body: data,
  })
  .then((response) => response.json())
  .then((responseJson) => {
      // console.log('seeet2 : ' + responseJson);
      console.log(responseJson);
        if (responseJson.status === 200) {
          this.callApiEventsList(this.state.baseUrl);
          this.setState({
             loadingStatus: false,
          })
          Alert.alert(responseJson.msg);
        } else {
          this.setState({
            // loadingStatus: false,
          })
          const error = 'Error in response';
        }
    })
    .catch((error) => {
        console.error(error);
    });
}

    renderWishButtons(event_id) {
      if (this.state.switch  ) {

      } else {
          return (
              <TouchableOpacity onPress={() => this.individualWishApi(event_id)} style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)', marginLeft: 20, borderRadius: 5, height: 20, width: 50, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{fontSize: 11 }}>Wish</Text>
              </TouchableOpacity>
          )
      }
    }

    renderWishAllButton() {
      var width = MAX_WIDTH - (MAX_WIDTH/4.57);
      var height = MAX_HEIGHT - (MAX_HEIGHT/1.35);
      if (!this.state.switch   && this.state.eventsData2.length > 1 ) {
        return (
            <TouchableOpacity onPress={() => this.wishAllApi()} style={{ marginLeft: width, marginTop: height, position: 'absolute', width: 75, height: 35, backgroundColor: 'rgba(0, 0, 0, 0.7)', borderRadius: 7, alignItems: 'center', marginRight: 2, justifyContent: 'center'}}>
              <Text style={{ color: '#fff' }}>Wish all</Text>
          </TouchableOpacity>
        )
      }
    }

      renderPropertiesEvents = ({ item }) => {
        const { baseUrl } = this.state;
        if (item.Photo === '') {
          var avatar = require('../../img/avatar.png');
        } else {
          var avatar = {
            uri: baseUrl + item.Photo,
          }
        }
        // console.log(item);
        // console.log(item.Person_Name);
        return (
          <TouchableOpacity  style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
                <View style={{ flexDirection: 'row', borderRadius: 10, alignItems: 'center' }}>
                <TouchableOpacity  onPress={()=> this.showPicture(avatar)}>

                    <Image 
                        style={{ width: 60, height: 60, borderRadius: 30, marginLeft: 10, marginTop: -10 }}
                        source={avatar}
                    />
</TouchableOpacity>
                    <View style={{ width: 200, height: 70, justifyContent: 'center',flexDirection:'column' }}>
                        <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' ,marginTop:8}}>{ item.Person_Name }</Text>
                        <Text numberOfLines={1} style={{ paddingLeft: 15,fontSize:10, paddingTop: 4, marginBottom: 0,width:130 }}>{ item.Event}</Text> 
                       <Text numberOfLines={1} style={{ paddingLeft: 15,fontSize:10, paddingTop: 4, marginBottom: 20,width:130 }}>{item.Fam_Name} House</Text> 
                    
                       
                       {/* <Text numberOfLines={1} style={{ paddingLeft: 15, paddingTop: 8, marginBottom: 20 }}>{ item.Event }</Text>
                       <Text numberOfLines={1} style={{ paddingLeft: 35, paddingTop: 8, marginBottom: 20 }}>{ item.Family_Name }</Text> */}

                    </View>

                    <View style={{ width: 200, height: 70, justifyContent: 'center',  }}>
                      {this.renderWishButtons(item.Event_Id)}
                    </View>

                </View>
                <View style={{ height: 0.2, backgroundColor: 'grey', width: '100%'}}></View>
            </TouchableOpacity>
        );
      }

      showPicture(profPic){
        this.setState({ slideAnimationDialog: true,propicture:profPic})
    
      }

      renderPropertiesWishes = ({ item }) => {
        const { baseUrl } = this.state;
        if (item.Photo === '') {
          var avatar = require('../../img/avatar.png');
        } else {
          var avatar = {
            uri: baseUrl + item.Photo,
          }
        }

        return (
          <TouchableOpacity style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
                <View style={{ flexDirection: 'row', borderRadius: 10, alignItems: 'center' }}>
                <TouchableOpacity  onPress={()=> this.showPicture(avatar)}>
                    <Image 
                        style={{ width: 60, height: 60, borderRadius: 30, marginLeft: 10, marginTop: -10 }}
                        // source={require('../../img/avatar.png')}
                        source={avatar}
                    />
</TouchableOpacity>
                    <View style={{ width: 200, height: 70, justifyContent: 'center' }}>
                        <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.Person_Name }</Text>
                        <Text numberOfLines={1} style={{ paddingLeft: 15, paddingTop: 8, marginBottom: 20 }}>{ item.Fam_Name } House</Text>
                    </View>

                    {/* <View style={{ width: 200, height: 70, justifyContent: 'center',  }}>
                      <View style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)', marginLeft: 20, borderRadius: 5, height: 20, width: 50, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{fontSize: 11 }}>Wish</Text>
                      </View>
                    </View> */}

                </View>
                <View style={{ height: 0.2, backgroundColor: 'grey', width: '100%'}}></View>
            </TouchableOpacity>
        );
      }

      changeEventMode() {
        //this.apiCallModeChange();

        // this.setState({
        //   switch: !this.state.switch,
        // });
        this.changeWishModeApi();
      }

      renderFlatListWishes() {
        if (this.state.loadingStatus) {
          return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' />
            </View>
          );
        }else if(this.state.dataWish.length==0){
          return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
               <Text >No Data Found </Text>
            </View>
          );

        } 
        else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

          return (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{fontSize:18}}>You are offline</Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                  <TouchableOpacity onPress={() => this.callApiWishList(this.state.baseUrl)}>
                      <Icon style={{ marginTop:10}} name="undo" size={17} color="#7BCFE8" />
                      </TouchableOpacity>
                  <Text style={{fontSize:18,marginTop:10,marginLeft:10}}>Retry</Text>
                  </View>

              </View>
          );
      } else if(this.state.settimeout == true,this.state.connection_Status=='Online')  {
        
          return (
            <View>
            {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
            onDismiss={() => {
              this.setState({ slideAnimationDialog: false });
            }}
            onTouchOutside={() => {
              this.setState({ slideAnimationDialog: false });
            }}
            visible={this.state.slideAnimationDialog}
            // dialogTitle={<DialogTitle title="" />}
            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
            <DialogContent>
            <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                   
                   source={this.state.propicture}
                   />
            </DialogContent>
          </Dialog> */}
             <Modal
    visible={this.state.slideAnimationDialog}
    onTouchOutside={() => {
      this.setState({ slideAnimationDialog: false });
    }}
  >
    <ModalContent>
    <Image style={{ width: 200, height: 200, marginTop:3}}
                 
                 source={this.state.propicture}
                 />
    </ModalContent>
  </Modal>
            <FlatList   
                style={{ marginTop: 5,}}    
                contentContainerStyle={{ paddingBottom: 35}}     
   
                data={this.state.dataWish}          
                renderItem={this.renderPropertiesWishes}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                          
            /> 
            </View>
          )
        }else{
          return(
          <View>
          {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
          onDismiss={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          onTouchOutside={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          visible={this.state.slideAnimationDialog}
          // dialogTitle={<DialogTitle title="" />}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
          <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                 
                 source={this.state.propicture}
                 />
          </DialogContent>
        </Dialog> */}
           <Modal
  visible={this.state.slideAnimationDialog}
  onTouchOutside={() => {
    this.setState({ slideAnimationDialog: false });
  }}
>
  <ModalContent>
  <Image style={{ width: 200, height: 200, marginTop:3}}
               
               source={this.state.propicture}
               />
  </ModalContent>
</Modal>
          <FlatList   
              style={{ marginTop: 5,}}    
              contentContainerStyle={{ paddingBottom: 35}}     
 
              data={this.state.dataWish}          
              renderItem={this.renderPropertiesWishes}          
              keyExtractor={(item, index) => index.toString()}  
              ItemSeparatorComponent={this.renderSeparator}  
                                        
          /> 
          </View>
        )
        }
      }

      renderFlatListEvents() {
        if (this.state.loadingStatus) {
          return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator  style={{marginTop:420}} size='large' />
            </View>
          );
        }else if(this.state.dataEvent.length==0){
          return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
               <Text style={{marginTop:420}}>No Event Found </Text>
            </View>
          );

        }
        else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

          return (
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{fontSize:18,marginTop:300}}>You are offline</Text>
                  <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                  <TouchableOpacity onPress={() => this.callApiEventsList(this.state.baseUrl)}>
                      </TouchableOpacity>
                  <Text style={{fontSize:18,marginTop:30,marginLeft:10,marginRight:25}}>Retry</Text>
                  <Icon onPress={() => this.callApiEventsList(this.state.baseUrl)}style={{ marginTop:35}} name="undo" size={17} color="#fff" />

                  </View>

              </View>
          
                     );
        } 
       else if(this.state.settimeout == true && this.state.connection_Status=='Online')  {
        return (
          <View>
          {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
          onDismiss={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          onTouchOutside={() => {
            this.setState({ slideAnimationDialog: false });
          }}
         
          visible={this.state.slideAnimationDialog}
          // dialogTitle={<DialogTitle title="" />}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
          <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                 
                 source={this.state.propicture}
                 />
          </DialogContent>
        </Dialog>
             */}
               <Modal
    visible={this.state.slideAnimationDialog}
    onTouchOutside={() => {
      this.setState({ slideAnimationDialog: false });
    }}
  >
    <ModalContent>
    <Image style={{ width: 200, height: 200,marginTop:3}}
                 
                 source={this.state.propicture}
                 />
    </ModalContent>
  </Modal>
             <FlatList   
                style={{ marginTop: 5,}}    
                contentContainerStyle={{ paddingBottom: 35}}     
   
                data={this.state.dataEvent}          
                renderItem={this.renderPropertiesEvents}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                          
            /> 
            </View>  
          )
         
        }else{
          return (
            <View>
            {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
            onDismiss={() => {
              this.setState({ slideAnimationDialog: false });
            }}
            onTouchOutside={() => {
              this.setState({ slideAnimationDialog: false });
            }}
            visible={this.state.slideAnimationDialog}
            // dialogTitle={<DialogTitle title="" />}
            dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
            <DialogContent>
            <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                   
                   source={this.state.propicture}
                   />
            </DialogContent>
          </Dialog> */}
            <Modal
    visible={this.state.slideAnimationDialog}
    onTouchOutside={() => {
      this.setState({ slideAnimationDialog: false });
    }}
  >
    <ModalContent>
    <Image style={{ width: 200, height: 200,marginTop:3}}
                 
                 source={this.state.propicture}
                 />
    </ModalContent>
  </Modal>
          <FlatList   
                style={{ marginTop: 5,}}    
                contentContainerStyle={{ paddingBottom: 35}}     
   
                data={this.state.dataEvent}          
                renderItem={this.renderPropertiesEvents}          
                keyExtractor={(item, index) => index.toString()}  
                ItemSeparatorComponent={this.renderSeparator}  
                                          
            /> 
            </View> 
          )
         
        }
      }

      changePage(mode) {
        if (mode === 1) {
          this.setState({ pageMode: true });
          // this.callApiEventsList(this.state.baseUrl);
        } 

        if (mode === 2) {
          this.setState({ pageMode: false })
          //this.callApiWishList(this.state.baseUrl);
          this.callApiWishList(this.state.baseUrl);

        } 
       
      }
      renderItems() {
        
        if (this.state.pageMode) {
            return (
              <View style={{ flex: 1 }}>
                    <View style={{ marginLeft: 10, marginRight: 10, marginTop: 40, backgroundColor: '#7BCFE8'}}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Today's events</Text>
                          <View style={{ height: 50, width: '100%' }}>
                            <View style={{ marginLeft: '50%', backgroundColor: 'rgba(255, 255, 255, 0.6)', height: 50, width: 170, borderRadius: 30, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
                              <Text style={{ paddingRight: 10, fontSize: 17, fontWeight: 'bold' }}>Auto Wish</Text>
                              <View >
                              {/* <Switch
                                ios_backgroundColor={'rgba(0, 0, 0, 0.6)'}
                                thumbTintColor={'#fff'}
                                onTintColor={'#000'}
                                onValueChange = {() => this.changeEventMode()}
                                value = {this.state.switch}
                              /> */}
                             { this.renderButton()}
                              </View>
                            </View>
                        </View>

                        <View style={{ height: 50, backgroundColor: 'rgba(255, 255, 255, 0.6)', marginTop: 20, borderRadius: 7, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                          <TouchableOpacity onPress={() => this.changePage(1)} style={{ width: '49%', height: 45, backgroundColor: '#fff',  marginRight: 2, borderRadius: 7}}>
                            <View style={{ width: '100%', height: 45, borderRadius: 7, alignItems: 'center', justifyContent: 'center'}}>
                              <Text> Events </Text>
                            </View>
                          </TouchableOpacity>

                          
                          <TouchableOpacity onPress={() => this.changePage(2)} style={{ width: '49%', height: 45, borderRadius: 7, alignItems: 'center', marginRight: 2, justifyContent: 'center'}}>
                            <Text> Wishes </Text>
                          </TouchableOpacity>
                          
                                                
                        </View>
                    </View>

                    <View style={{ borderTopWidth: 0, borderBottomWidth: 0, marginTop: 10, backgroundColor: '#fff', borderRadius: 7, borderBottomEndRadius: 0, borderBottomStartRadius: 0 }}>
                      
                        {/* <FlatList   
                            style={{ marginTop: 20 }}       
                            data={this.state.data}          
                            renderItem={this.renderPropertiesEvents}          
                            keyExtractor={(item, index) => index.toString()}  
                            ItemSeparatorComponent={this.renderSeparator}  
                                                      
                        />             */}
                        {this.renderFlatListEvents()}
                    </View>
                    
                      {this.renderWishAllButton()}
                    
                  </View>
            )
        } else {
          return (
            <View style={{ flex: 1 }}>
                    <View style={{ marginLeft: 10, marginRight: 10, marginTop: 40, backgroundColor: '#7BCFE8'}}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold' }}>Today's events</Text>
                          <View style={{ height: 50, width: '100%' }}>
                            <View style={{ marginLeft: '50%', backgroundColor: 'rgba(255, 255, 255, 0.6)', height: 50, width: 170, borderRadius: 30, alignItems: 'center', justifyContent: 'center', flexDirection: 'row'}}>
                              <Text style={{ paddingRight: 10, fontSize: 17, fontWeight: 'bold' }}>Auto Wish</Text>
                              <View >
                              {/* <Switch
                                ios_backgroundColor={'rgba(0, 0, 0, 0.6)'}
                                thumbTintColor={'#fff'}
                                onTintColor={'#000'}
                                onValueChange = {() => this.changeEventMode()}
                                value = {this.state.switch}
                              /> */}
                             { this.renderButton()}
                              </View>
                            </View>
                        </View>

                        <View style={{ height: 50, backgroundColor: 'rgba(255, 255, 255, 0.6)', marginTop: 20, borderRadius: 7, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                          
                          
                        <TouchableOpacity onPress={() => this.changePage(1)} style={{ width: '49%', height: 45, borderRadius: 7, alignItems: 'center', marginRight: 2, justifyContent: 'center'}}>
                            <Text> Events </Text>
                          </TouchableOpacity>

                          <TouchableOpacity onPress={() => this.changePage(2)} style={{ width: '49%', height: 45, backgroundColor: '#fff',  marginRight: 2, borderRadius: 7}}>
                            <View style={{ width: '100%', height: 45, borderRadius: 7, alignItems: 'center', justifyContent: 'center'}}>
                              <Text> Wishes </Text>
                            </View>
                          </TouchableOpacity>
                                                
                        </View>
                    </View>

                    <View style={{ flex: 1, borderTopWidth: 0, borderBottomWidth: 0, marginTop: 10, backgroundColor: '#fff', borderRadius: 7, borderBottomEndRadius: 0, borderBottomStartRadius: 0 }}>
                      
                        {/* <FlatList   
                            style={{ marginTop: 5 }}       
                            data={this.state.data}          
                            renderItem={this.renderPropertiesWishes}          
                            keyExtractor={(item, index) => index.toString()}  
                            ItemSeparatorComponent={this.renderSeparator}  
                                                      
                        />             */}
                        {this.renderFlatListWishes()}
                    </View>
                  </View>
          );
        }
      }

    render() {
     
      console.log('switch button : ' + this.state.switch);
        return (
            <View style={{ flex: 1, backgroundColor: '#7BCFE8'}}>
              {this.renderItems()}
            </View>
        );
    }
}
