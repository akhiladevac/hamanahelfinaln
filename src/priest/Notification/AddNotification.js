
import React, { Component } from 'react';
import { View, Text , Image,ScrollView, ActivityIndicator,BackHandler,Picker, TouchableOpacity, TextInput,Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Actions } from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker';
import AsyncStorage from '@react-native-community/async-storage';


const DATA_BASE_URL = '@base_url';
export default class AddNotification extends Component{


  constructor() {
    super();

    this.state = {
        description: '',
        date: '',
        title: '',
        response: [],
        PickerValueHolder : '',
        response1: [],

      

    }
}
UpdateData = () => {
  this.props.parentCallback("juhiu");

}
componentDidMount() {
  
  this.getBaseUrl();
     this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

  
}
componentWillUnmount() {
    this.backHandler.remove();
  }
  
  handleBackPress = () => {
   
    if (Actions.currentScene !== 'AddNotification') {
        Actions.pop();
    } else {
      Actions.VisitNotification();
  
      return true;
    }
  }
  choosecategory(baseUrl){
    var endpoint = 'member_categories.php';
    var url = baseUrl + endpoint;
    // console.log(url);
    var data = new FormData();
    data.append('hashcode', '##church00@');
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
   

    .then((response) => response.json())
    
    .then((responseJson) => {
        // console.log(responseJson);
        this.setState({ 
            response1: responseJson.response, 
            loadingStatus: false,
        })
         console.log(responseJson);
    }) 
    
    .catch((error) => {
      console.error(error);
    });
    // setTimeout(() => ab
    
  }
async getBaseUrl() {
  try {
      let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
      this.setState({ baseUrl: baseUrl });
      this.choosecategory(this.state.baseUrl);
      
  } catch (error) {
      console.error('Something went Wrong in Get Section' + error);
  }
}

  clearText(){
    this.setState({title: '',
    description: '',
    date: '',
    })
  }


  addButton(){
    const {title, description, date}=this.state
    if( description && date && title){
      this.apiCall(this.state.baseUrl)
    }
    else{
      Alert.alert(' fields are empty');
    }
  }


   apiCall(baseUrl) {

    const {title, description, date,PickerValueHolder}=this.state
    var endpoint = 'add_notification.php';
    var url = baseUrl + endpoint;
    // console.log(url);
    var data = new FormData();
    data.append('hashcode', '##church00@');
    data.append('title', title);
    data.append('description', description);
    data.append('expire', date);
    data.append('category', PickerValueHolder);

    console.log(data);
    console.log(data);
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
        this.setState({ response: responseJson.response, loadingStatus: false});
         console.log(responseJson);
         if (responseJson.status === 200){
           Alert.alert('success')
           this.UpdateData();
      //      sendData = () => {
      //       this.props.parentCallback("juhiu");
      //  }
    
           Actions.pop();
         }
         else
         {
          Alert.alert('faild')
         }
    })
    .catch((error) => {
      console.error(error);
    });
 }


        render() {
          const { role, datas } = this.props;
          
            return(
          
              <View style={{ flex: 1}} >
               <View style={{ alignItems: 'center'}}>
               <View style={{ height: 70, marginTop: 100, width: '95%', }}>
               <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                       <Text style={{ paddingTop: 10, paddingLeft: 20, fontWeight: '600' ,marginTop:10}}> Choose Category</Text>
                      <View style={{width:200,height:50 ,marginTop:19,marginLeft:20,backgroundColor: '#fff',borderRadius: 10,paddingRight:20}}>
                       <Picker style={{}}
            selectedValue={this.state.PickerValueHolder}
 
            onValueChange={(itemValue, itemIndex) => this.setState({PickerValueHolder: itemValue})} >
 
            { this.state.response1.map((item, key)=>(
            <Picker.Item label={item.position} value={item.position} key={key} />)
            )}
    
          </Picker>
          </View>
                       
                      
                       </View>
                   </View>
                   <View style={{ backgroundColor: '#fff', height: 70, borderRadius: 10, marginTop: 15, width: '95%', }}>
                       <Text style={{ paddingTop: 10, paddingLeft: 20, fontWeight: '600' }}> Title</Text>
                       <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                           <TextInput
                               style={{ paddingTop: 10, paddingLeft: 20, width: '80%', paddingRight: 20 }} 
                               onChangeText={title => this.setState({title:title})}
                               value={this.state.title}
                           />
                       </View>
                   </View>
                   <View style={{ backgroundColor: '#fff', height: 200, borderRadius: 10, marginTop: 15, width: '95%', }}>
                       <Text style={{ paddingTop: 5, paddingLeft: 20, fontWeight: '600' }}> Description</Text>
                       <View style={{ flexDirection: 'row' }}>
                           <TextInput
                               style={{ textAlignVertical: 'top', paddingTop: 10, paddingLeft: 20, width: '100%', height: 170, paddingRight: 20}} 
                               onChangeText={description => this.setState({ description: description})}
                               value={this.state.description}
                               numberOfLines={1} 
                               multiline={true}
                           />
                       </View>
                   </View>
                   <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 0}}>
                      <View style={{flexDirection: 'column', justifyContent: 'center', height: 50, marginTop: 20}}> 
                          <Text>
                              Exp.Date
                          </Text>
                          <DatePicker
                              style={{  marginTop: 2, backgroundColor: '#fff'}}
                              date={this.state.date}
                              mode="date"
                              placeholder="select date"
                             format="YYYY-MM-DD"
                              //  format="DD-MM-YYYY"

                              minDate="2000-01-01"
                              maxDate="2030-12-31"
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              customStyles={{
                                dateIcon: {
                                  position: 'absolute',
                                  left: 0,
                                  top: 4,
                                  // marginLeft: 5
                                },
                                dateInput: {
                                  
                                }
                                // ... You can check the source to find the other keys.
                              }}
                              onDateChange={(date) => {this.setState({ date: date})}}
                          />   

                                           
                      </View>
                      
                      </View>
               </View>
               <View style={{flexDirection: 'row', position:'absolute', bottom: 0, width: '100%'}}>
               <TouchableOpacity onPress={ () => this.clearText()} style={{backgroundColor: '#7BCFE8',height: 60, alignItems: 'center', justifyContent: 'center', width: '50%', borderRightWidth: 1}}>
                   <Text style={{ fontSize: 17, fontWeight: 'bold'}}>
                       Clear
                   </Text>
               </TouchableOpacity>
               <TouchableOpacity onPress={()=> this.addButton()} style={{backgroundColor: '#7BCFE8',height: 60, alignItems: 'center', justifyContent: 'center', width: '50%'}}>
                   <Text style={{ fontSize: 17, fontWeight: 'bold'}}>
                       Add
                   </Text>
               </TouchableOpacity>
               </View>
             </View>
                   

            );

                    
        }
    }