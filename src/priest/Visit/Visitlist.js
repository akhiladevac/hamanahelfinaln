import React, { Component } from 'react';
import { View, Text , Image, ScrollView, ActivityIndicator,BackHandler,NetInfo, TouchableOpacity,FlatList, TextInput, Alert, Picker } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Actions } from 'react-native-router-flux';
import DatePicker from 'react-native-datepicker'
import AsyncStorage from '@react-native-community/async-storage';


const DATA_BASE_URL = '@base_url';
export default class Visitlist extends Component{


  constructor() {
    super();
    this.renderUpdateviewvist = this.renderUpdateviewvist.bind(this)

    this.state = {
        Fam_Id: '',
        description: '',
        date: '',
        hour: '',
        mint: '',
        mode: 'am',
        name:'',
        housename:'',
        Photo:'',
        loadingStatus: true,
        visit_id:'',
        fam_id:'',
        deletenotifresponse:[],

        
       
pageMode2:'',
data:[{}],
response:[],
response2:[]

      

    }
}

componentDidMount() {
    this.getBaseUrl();

  this.setState({
    pageMode2:this.props.pageMode1
  })
  this.checkNetwork();

  const { Fam_Id, Visit_Id, title, description, time, visit_date} = this.props.datas;
  console.log(this.props.datas);
  this.setState({ Fam_Id: Fam_Id, Visit_Id: Visit_Id , title: title, description: description, date: visit_date, time: time })
   this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

}
checkNetwork=() => {
  NetInfo.isConnected.addEventListener(
    'connectionChange',
    this._handleConnectivityChange

);

NetInfo.isConnected.fetch().done((isConnected) => {

  if(isConnected == true)
  {
    this.setState({connection_Status : "Online"})
  }
  else
  {
    this.setState({connection_Status : "Offline"})



  }

});

}

componentWillUnmount() {

  this.backHandler.remove();
  NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this._handleConnectivityChange

  );

}

_handleConnectivityChange = (isConnected) => {

  if(isConnected == true)
    {
      this.setState({connection_Status : "Online"})

    }
    else
    {
      this.setState({connection_Status : "Offline"})

      


    }
};
 
 
async getBaseUrl() {
  try {
      let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
      this.setState({ baseUrl: baseUrl });
      this.apiCall1(this.state.baseUrl)
      
  } catch (error) {
      console.error('Something went Wrong in Get Section' + error);
  }
}
renderUpdateviewvist(){
    this.getBaseUrl()
}

handleBackPress = () => {

    if (Actions.currentScene !== 'Visitlist') {
        Actions.pop();
    } else {
      Actions.VisitNotification({data:this.state.pageMode2});
  
      return true;
    }
  }




    apiCall1(baseUrl) {
              this.setState({ loadingStatus: true })
    var endpoint = 'family_visit_list.php';
    var url = baseUrl + endpoint;
    // console.log(url);
    var data = new FormData();
    data.append('hashcode', '##church00@');
    data.append('Fam_Id', this.props.datas.Fam_Id);
    // alert(this.props.datas.Fam_Id)
    
    // console.log(data);
    console.log(data);
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
    .then((response) => response.json())
    .then((responseJson) => {
        this.setState({ response: responseJson.response, loadingStatus: false});
        console.log(responseJson);
        if (responseJson.status === 200){
           
            this.setState({
                response2:responseJson,
                name:responseJson.Fam_Owner,
                housename:responseJson.Fam_Name,
                Photo:responseJson.Photo,
                loadingStatus:false,
                fam_id:responseJson.Fam_Id



            })
        //   Alert.alert('Visit updated')
        //   this.UpdateData();
        //   Actions.pop();
        }
        else
        {
          this.setState({
            loadingStatus:false
          })
          Alert.alert('faild')
        }
    })
    .catch((error) => {
      console.error(error);
    });
    setTimeout(() => {
      this.setState({
          loadingStatus:false,
          settimeout:true
      })
      },45000)
  }

  
renderProperties = ({ item }) => {
    return (
        <TouchableOpacity 
        onPress={() => Actions.EditVisit({ datas:this.state.response2, baseUrl: this.state.baseUrl ,parentCallback: this.renderUpdateviewvist,callbackstate1:this.props.callbackstate ,pageMode2: this.props.pageMode1})}
        >
            <View style={{ flexDirection: 'row', borderRadius: 10, alignItems: 'center',backgroundColor:'#fff' , marginTop: 10, marginLeft: 10, marginRight: 10}}>

            <View style={{ backgroundColor: '#fff', height: 100, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 ,width:200}}>
                    <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.title }</Text>
                    <Text  style={{ paddingLeft: 15, paddingTop: 8, marginBottom: 20,height:60 }}>{ item.description } </Text>
                </View>

                <View style={{ width: 200, height: 70, justifyContent: 'center' }}>
                        <Text style={{fontSize: 11, paddingLeft: 20 ,marginLeft:30}}>{item.visit_date}</Text>
                    </View>

            </View>
        </TouchableOpacity>
    );
  };
  deletealertvisit(response,r,f){
alert(f)
    Alert.alert(  
      'Delete ',  
      'Do you really want to delete this visit?',  
      [  
          {  
              text: 'No',  
              onPress: () => console.log('Cancel Pressed'),  
              style: 'cancel',  
          },  
          {text: 'Yes', onPress: () => this.deletevisit(response,r,f)},  
      ]  
  );  
    }
    deletevisit(response,r,f){
      this.setState({
          visit_id:r,
         
      })
      var endpoint = 'delete_visits.php';
      var url =this.state.baseUrl + endpoint;
      // console.log(url);
      var data = new FormData();
      data.append('hashcode', '##church00@');
       data.append('Fam_Id',this.state.fam_id);

       data.append('visit_id',this.state.visit_id);

      fetch(url, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
      },
      body: data,
      })
     

      .then((response) => response.json())
      
      .then((responseJson) => {
          // console.log(responseJson);
          this.setState({ 
              deletenotifresponse: this.state.response, 
              loadingStatus: false,
          })
          Alert.alert("Deleted Successfully")
          this.apiCall1(this.state.baseUrl);

          // console.log(responseJson);
      }) 
      
      .catch((error) => {
        console.error(error);
      });
      // setTimeout(() => a

  }

        render() {
           
            // var abc = 0;
              if (this.props.datas.Photo) {
                  var profPic =  { uri: this.state.baseUrl + this.props.datas.Photo };
              } else {
                  var profPic = require('../../img/avatar.png');
              }
            return(
              <View style={{ flex: 1}} >
                <ScrollView showsVerticalScrollIndicator={false}>

                <View style={{flexDirection:'row', justifyContent: "flex-end",backgroundColor:'#7BCFE8',}}>
              <Text style= {{ fontSize: 16, fontWeight: 'bold', marginTop: 10,}}>No of Visit : </Text>     
              <Text style= {{ fontSize: 16, fontWeight: 'bold', marginTop: 10,marginRight:20 }}>{this.state.response.length}</Text>     
              </View>
              <View style= {{ alignItems: 'center', backgroundColor:'#7BCFE8',height:320, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
             
                   <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8',height: 280, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                       <Image
                           style={{ height: 101, width: 101,borderRadius: 50, marginTop: '5%' }}
                           source={profPic}
                        /> 
                       <Text style= {{ fontSize: 22, fontWeight: 'bold', marginTop: 10 }}>{this.props.datas.Fam_Owner}</Text>     
                       <Text style= {{ fontSize: 13, marginTop: 8 }}> {this.props.datas.Fam_Name} House</Text>                        
                   </View>    
               </View> 
               <View style={{ alignItems: 'center', width: '100%', height: '61%'}}>

               <FlatList   
                    style={{ marginTop: 2,width:'100%' }}    
                    contentContainerStyle={{ paddingBottom: 25}}     
   
                    data={this.state.response}          
                    renderItem={this.renderProperties}          
                    keyExtractor={(item, index) => index.toString()}  
                    ItemSeparatorComponent={this.renderSeparator}                             
                />        
               </View>
               
               </ScrollView>

             </View>
                   

            );

                    
        }
    }