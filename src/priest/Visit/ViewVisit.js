
import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity, ActivityIndicator, FlatList, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

const MAX_HEIGHT = Dimensions.get('window').height;
const MAX_WIDTH = Dimensions.get('window').width;

const DATA_BASE_URL = '@base_url';

export default class ViewVisit extends Component {

    constructor() {
        super();

        this.state = {
            response: [],
            loadingStatus: true,
            baseUrl: ''
        };
    }

    async componentDidMount() {

        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            //this.setState({  baseUrl: baseUrl });
            this.apiCall(baseUrl);
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
    }

    //check user is already logged in
    async getBaseUrl() {
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            console.log("Saved BASEURL : " + baseUrl);
            if (baseUrl) {
                console.log('base url is okey');
            } else {
                Actions.ChooseChurch();
            }
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
    }

    async apiCall(baseUrl) {

        var endpoint = 'view_visits.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({ response: responseJson.response, loadingStatus: false});
             console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
    }

    renderNotifications() {
        if (this.state.loadingStatus) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size='large' />
                </View>
            );
        }
    }

                  renderProperties = ({ item }) => {
                    return (
                  
                      <TouchableOpacity onPress= {() => Actions.EditVisit({ datas: item })}>
                        <View style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
                          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                              
                              <View>
                                  <Text
                                  numberOfLines={1} 
                                  ellipsizeMode={'tail'}
                                   style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500', }}>{item.title}</Text>
                                  <Text style={{ paddingLeft: 15, paddingTop: 8 }}>{item.visit_date } </Text>
                              </View>
                            
                          </View>
                        </View>
                      </TouchableOpacity>
                      
                      
                      
                    );
                  };
            
                render() {
                    var width = MAX_WIDTH - (MAX_WIDTH/5);
                    var height = MAX_HEIGHT - (MAX_HEIGHT/4);
                    return (
                        
                        <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}> 
                            <FlatList   
                                style={{ marginTop: 2 }} 
                                contentContainerStyle={{ paddingBottom: 25}}     
      
                                data={this.state.response}          
                                renderItem={this.renderProperties}          
                                keyExtractor={(item, index) => index.toString()}  
                                ItemSeparatorComponent={this.renderSeparator}                                  
                            />
                            <TouchableOpacity  onPress={()=> Actions.FatherUserList1()} style={{ position: 'absolute', marginLeft: width, marginTop: height }}>
                
                                <View style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0ea0cc', borderRadius: 50 }}>
                                    <Text style={{ fontSize: 25, color: '#fff', fontWeight: 'bold'}}>+</Text>
                                </View>
                            
                            </TouchableOpacity>    
                                   
                        </View>
                    );
                }
    
}