/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput,NetInfo, Alert, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

    const DATA_BASE_URL = '@base_url';
    const LOGIN_CREDENTIALS = '@login_credentials';
    export default class PhoneCheck extends Component{

        constructor() {
            super();

            this.state= {
                phoneno: '',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
             
                
                dataSource:[],
                data:'',
                baseUrl: '',
                loadingStatus: false,
            }
        }
        nextpage(){
            Actions.ResetPswd();
          }
phoneCheck(){
    if(this.state.phoneno == "" )
    {
        alert("Enter your valid Phone number");
    }else{
    this.getBaseUrl();
    }

}
     

        async getBaseUrl() {
            try {
                let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
                this.setState({ baseUrl: baseUrl });
                var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
                var login_credentials = JSON.parse(loginCredentials);
                //this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
                var fam_id = login_credentials.family_id;
                this.getPhoneData(baseUrl, fam_id);
                
            } catch (error) {
                console.error('Something went Wrong in Get Section' + error);
            }
        }

        getPhoneData(baseUrl,fam_id){
            var endpoint = 'phone_check.php';
            var url = baseUrl + endpoint;
            var data = new FormData();
            data.append('hashcode', '##church00@');
            data.append('Fam_Id', fam_id);
            data.append('phone', this.state.phoneno);
           
            fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data,
            })
            .then((response) => response.json())
            .then((responseJson) => {
          
               
                alert(responseJson.Message)
                if(responseJson.status==200)  {
                  this.nextpage()
                 
                }
              
               
            })
            .catch((error) => {
                console.error(error);
           });
      
          }
    

      

        render(){
            if (this.state.loadingStatus) {
                return (
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size='large' />
                    </View>
                )
            } 
            return(
                
                <View style={{ backgroundColor: '#fff', height: '100%', width: '100%', alignItems: 'center' }}>
                    <View style={{ marginTop: 100 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>                                                                                                                                                                                                  
                        <Text style= {{ fontSize: 23, fontWeight: 'bold', marginTop:0 }}> Enter Valid phone Number</Text>     
                        <Text style= {{ fontSize: 15, marginTop: 5 }}> </Text>
                    </View>

                    <View style= {styles.login}>
                        <TextInput
                         autoCapitalize="none"
                         style={{ color: '#000', fontSize: 16, fontWeight: '400', paddingLeft: 20 }}
                         value={this.state.phoneno}
                         placeholder='phoneno'
                         onChangeText={phoneno => this.setState({ phoneno})}
                        />
                        
                    </View>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                    <TouchableOpacity 
                        onPress={() => this. phoneCheck()}
                        style={{alignItems: 'center', justifyContent: 'center', backgroundColor: '#45788f', width: 160, height: 40, marginTop:25, borderRadius: 15, marginLeft:130}}>
                        <Text style={{color: '#ffff'}}>Ok</Text>

                    </TouchableOpacity>
                   
                    
                     
                    </View>    
                </View>
            )
        }
    }

    styles = StyleSheet.create({
        login: {
            marginTop: 20,
            borderRadius: 20, 
            backgroundColor: '#7BCFE8', 
            width: 320, 
            height: 64, 
            alignItems: 'center' ,
            flexDirection: 'row',
        },
        password: {
            marginTop: 20,
            borderRadius: 20, 
            backgroundColor: '#69aabf', 
            width: 320, 
            height: 64, 
            alignItems: 'center' ,
            flexDirection: 'row',
            
        },
        text: {
            paddingLeft: 25,
            fontSize: 18,
            fontWeight: '300',
            color: '#000',
            width: '100%'
        }
    })