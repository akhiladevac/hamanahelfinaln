/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity, ActivityIndicator,NetInfo,Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
import Home from './Home';

const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
const DATA_NOTIFICATION = '@notification_list';


export default class Notifications extends Component {

    constructor() {
        super();
         this.viewUpdateNotifications = this.viewUpdateNotifications.bind(this)

        this.state = {
            response: [],
            response1: [],
            loading:false,
            responseUpdateUser:[],
            birthdayResponse: [],
            loadingStatus: false,
            loadingStatus1: false,

            connection_Status : '',
            settimeout:false,
            responseNotification:[],
            baseUrl: '',
            fam_id: '',
            userRole:'',
            deletenotifresponse:[],
            notification_id:'',
            responsenotifi:'',
            response2:[],
            data:[],
            temp:[]
        };
        this.arrayholder = [];
    }
    
    componentDidMount() {
        console.log("notification");
        this.checkNetwork();

        this.getBaseUrl();

    }

    apiCallBirthday(){
        
                this.apiCallBirthdayPriest(this.state.baseUrl,this.state.fam_id);

                this.apiCallBirthdayUser(this.state.baseUrl,this.state.fam_id);

            

        
        

    }
    async getNotificationList() {
        
        try {
            const notificationList = await AsyncStorage.getItem(DATA_NOTIFICATION);
            if (notificationList !== null) {
                          const abc = JSON.parse(notificationList);
                        this.setState({          
                            data: abc,  
                            temp: abc,        
                            error: null,  
                            loadingStatus:false        
                                
                          });  
                          this.arrayholder = abc; 
            }
        } catch (error) {
            console.error(error);
        }
    }
    async getNotificationListUser() {
        this.setState({ loadingStatus1: true })
        try {
            const notificationList = await AsyncStorage.getItem(DATA_NOTIFICATION);

            if (notificationList !== null) {
                          const abc = JSON.parse(notificationList);
                        this.setState({          
                            response1: abc,  
                            temp: abc,        
                            error: null,   
                             loadingStatus1:false       
                                
                          });  
                          this.arrayholder = abc; 
            }
        } catch (error) {
            console.error(error);
        }
    }
   
    
    getUpdatedNotificationList(){
        this.getBaseUrlUpdate();
      }
     
      async getBaseUrlUpdate(){
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
            if (loginCredentials) {
             var login_credentials = JSON.parse(loginCredentials);
             var fam_id = login_credentials.family_id;
            this.setState({ userRole: login_credentials.role, baseUrl: baseUrl, fam_id: fam_id });
            if(this.state.userRole=='priest'){
                 this.apiCallBirthdayPriest(baseUrl,fam_id);

             }else if(this.state.userRole=='user'){

                 this.apiCallBirthdayUser(baseUrl,fam_id);

            }


           
            }
            
            
        } 
        catch(error){
            console.error('Something went wrong in Get Section '+ error)
        }
    }
  
    async getBaseUrl(){

        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
            if (loginCredentials) {
             var login_credentials = JSON.parse(loginCredentials);
             var fam_id = login_credentials.family_id;
            this.setState({ userRole: login_credentials.role, baseUrl: baseUrl, fam_id: fam_id });
            if(this.state.userRole=='priest'){
                this.getNotificationList();
              
                    this.getUpdatedNotificationList();

            

              

            }else if(this.state.userRole=='user'){

                this.getNotificationListUser();
                if(this.state.connection_Status=='Online'){
                this.getUpdatedNotificationList();
                }else{

                }

            }

                // this.apiCallBirthdayPriest(baseUrl,fam_id);

           
                // this.apiCallBirthdayUser(baseUrl,fam_id);
            }
            
            
        } 
        catch(error){
            console.error('Something went wrong in Get Section '+ error)
        }
    }
    checkNetwork=() => {
        NetInfo.isConnected.addEventListener(
          'connectionChange',
          this._handleConnectivityChange
    
      );
     
      NetInfo.isConnected.fetch().done((isConnected) => {
    
        if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
        }
        else
        {
          this.setState({connection_Status : "Offline"})
    
    
    
        }
    
      });
    
      }
      componentWillUnmount() {
    
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
    
          }
          else
          {
            this.setState({connection_Status : "Offline"})
    
            
    
    
          }
      };
     
      async storeNotification(response, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(response));
    }
    
     showAlert(){
        Alert.alert(  
            'You are in offline',  
            [  
                {  
                    text: 'Cancel',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'Retry', onPress: () => this.checkNetwork()},  
            ]  
        );  
     }
    
     componentWillMount(){

        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this._handleConnectivityChange
     
        );
     
      }
     
      _handleConnectivityChange = (isConnected) => {
     
        if(isConnected == true)
          {
            this.setState({connection_Status : "Online"})
          }
          else
          {
            this.setState({connection_Status : "Offline"})
            return (
                // <TouchableOpacity  onPress={() => Actions.detailedNotification({ contents: response, title: 'Notifications' })}>
                    <View style={{ margin: 5, borderRadius: 5, height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#000' }}>
                       
                        <Text style={{ color: '#fff', fontSize: 11, fontWeight: 'bold', width: 100, paddingLeft: 10, paddingRight: 10 }}> You are offline </Text>
                        
                    </View>
                   
            );
          }
      };
  
    viewUpdateNotifications(){
              this.getBaseUrlUpdate();

       

        
      }
    async apiCall(baseUrl) {
        this.setState({  response: [] });

        var endpoint = 'notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
       

        .then((response) => response.json())
        
        .then((responseJson) => {
            if (responseJson.status === 200) {          

            // console.log(responseJson);
            this.setState({ 
                 response: this.state.response.concat(responseJson.response), 
            //  response: responseJson.response, 

                
            })
            this.chechVariations();

        }

            // alert(responseJson.response.msg)

            // console.log(responseJson);
        }) 
        
        .catch((error) => {
          console.error(error);
        });
        // setTimeout(() => abort, 1000)
    }
    async chechVariations() {
        try {
            const notificationList = await AsyncStorage.getItem(DATA_NOTIFICATION);
            if (notificationList !== null) {
              const abc = JSON.parse(notificationList);
            
              this.setState({          
                  data: abc,  
                  temp: abc,        
                  error: null,          
                  loading: false,
                  query: '',        
                });  
                this.arrayholder = abc; 
              } 

              if(JSON.stringify(this.state.response) == JSON.stringify(this.state.data))
              { }else{
                this.updateAsyncValues(this.state.response,DATA_NOTIFICATION);
              }
          
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    async updateAsyncValues(response, KEY) {
      await AsyncStorage.setItem(KEY, JSON.stringify(response));
      this.getNotificationList();
    }
    async chechVariationsUser() {
        try {
            const notificationList = await AsyncStorage.getItem(DATA_NOTIFICATION);
            if (notificationList !== null) {
              const abc = JSON.parse(notificationList);
            
              this.setState({          
                  response1: abc,  
                  temp: abc,        
                  error: null,          
                  loading: false,
                  query: '',        
                });  
                this.arrayholder = abc; 
              } 

              if(JSON.stringify(this.state.responseUpdateUser) == JSON.stringify(this.state.response1))
              { }else{
                this.updateAsyncValuesUser(this.state.responseUpdateUser,DATA_NOTIFICATION);
              }
          
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    async updateAsyncValuesUser(response, KEY) {
      await AsyncStorage.setItem(KEY, JSON.stringify(response));
      this.getNotificationListUser();
    }
    
    async apiCallBirthdayPriest(baseUrl, fam_id) {

                

        this.setState({  response: [] });
        var endpoint = 'wish_notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    response: responseJson.response,
                    birthdayResponse: responseJson.response,
                    // settimeout:'false'
                });
                this.apiCall(baseUrl);

            } else {
                this.apiCall(baseUrl);


            }
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
        // setTimeout(() => {
        //     this.setState({
        //         loadingStatus:false,
        //         settimeout:true
        //     })
        //     },45000)

    }
    async apiCallBirthdayUser(baseUrl, fam_id) {

        this.setState({  response: [] });
        var endpoint = 'wish_notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    responseUpdateUser: responseJson.response,
                    birthdayResponse: responseJson.response,
                    settimeout:'false'
                });
                this.notificationCategory(baseUrl,fam_id)

                this.allNotification(baseUrl);
            } else {
                this.notificationCategory(baseUrl,fam_id)

                this.allNotification(baseUrl);
            }
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
       

    }
    async allNotification(baseUrl) {

        var endpoint = 'all_notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    responseUpdateUser: this.state.responseUpdateUser.concat(responseJson.response), 
                    
                   
                });
                this.chechVariationsUser();

                //   this.notificationCategory(baseUrl,this.state.fam_id)
            } else {
                this.setState({ 
                    responseUpdateUser: this.state.responseUpdateUser, 
                    
                   
                });
                this.chechVariationsUser();

                //    this.notificationCategory(baseUrl,this.state.fam_id)

            }
             console.log("allnotif"+JSON.stringify(responseJson) );
        })
        .catch((error) => {
          console.error(error);
        });
        
       

    }
    async notificationCategory(baseUrl, fam_id) {

        var endpoint = 'notification_category.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    responseUpdateUser: this.state.responseUpdateUser.concat(responseJson.response), 
                   
                });
                
            } else {
               
            }
             console.log("gdhtdft"+JSON.stringify(responseJson) );
        })
        .catch((error) => {
          console.error(error);
        });
        
    }
    renderNotifications() {
       if(this.props.role=='priest'){

        if(this.state.data.length==''){
        }
        else{
        return (
            this.state.data.map((data) => {

            if (data.date) {
                var months = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                  ];
                
                  var Value = data.date.split("-");
                  const monthVar = Number.parseInt(Value[1]);
                  var date = Value[2];
                  var month = months[monthVar - 1]; 
            } else {
                var date = '';
                var month = '';
            }

            
                    var profPic = require('../../img/man.png');
                
                return (
                    <TouchableOpacity key={data.id} 
                     onPress={() => Actions.detailedNotification({ contents: data, title: '' })}>
                    <View key={data.id} style={{ height: 200, width: 160, backgroundColor: '#fff', borderRadius: 10, marginLeft: 12,}}>
                        <View style={{ margin: 5, borderRadius: 5, height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#000' }}>
                            
                            <Text style={{ color: '#fff', fontSize: 11, fontWeight: 'bold', width: 80, paddingLeft: 7, paddingRight: 10 }}> Priest</Text>
                           
                            </View>
                        <Text style={{ color: '#000', marginTop: 5, textAlign: 'right', paddingRight: 20, fontSize: 9, }}> {date} {month}</Text>

                        <Text style={{ fontSize: 13, fontWeight: '600', marginTop: 10, paddingLeft: 10, paddingRight: 10}}> {data.title}</Text>

                    </View>
                    </TouchableOpacity>
                );

                    
            })
        );
        

        }
    }
    else if(this.props.role=='user'){
        return (
            this.state.response1.map((response1) => {

            if (response1.date) {
                var months = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                  ];
                
                  var Value = response1.date.split("-");
                  const monthVar = Number.parseInt(Value[1]);
                  var date = Value[2];
                  var month = months[monthVar - 1]; 
            } else {
                var date = '';
                var month = '';
            }

            
                    var profPic = require('../../img/man.png');
                
                return (
                    <TouchableOpacity key={response1.id} 
                     onPress={() => Actions.detailedNotification({ contents: response1, title: 'Notifications' })}>
                    <View key={response1.id} style={{ height: 200, width: 160, backgroundColor: '#fff', borderRadius: 10, marginLeft: 12,}}>
                        <View style={{ margin: 5, borderRadius: 5, height: 50, flexDirection: 'row', alignItems: 'center', backgroundColor: '#000' }}>
                            
                            <Text style={{ color: '#fff', fontSize: 11, fontWeight: 'bold', width: 80, paddingLeft: 5, paddingRight: 10 }}> Priest </Text>
                           
                            </View>
                        <Text style={{ color: '#000', marginTop: 5, textAlign: 'right', paddingRight: 20, fontSize: 9, }}> {date} {month}</Text>

                        <Text style={{ fontSize: 13, fontWeight: '600', marginTop: 10, paddingLeft: 10, paddingRight: 10}}> {response1.title}</Text>

                    </View>
                    </TouchableOpacity>
                );

                    
            })
        );
        console.log("state"+this.state.response1)
    }
               
    
}

    renderContents() {
if(this.state.userRole=='priest'){
        if (this.state.loadingStatus) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size='large' />
                </View>
            );
        }
        else if(this.state.data.length==0){
            return (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                             <Text style={{fontSize:16}}>No New Notification</Text>
                             </View>
            )

        }
        // else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

        //     return (
        //         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        //             <Text style={{fontSize:18}}>You are offline</Text>
        //             <View style={{ flexDirection: 'row', alignItems: 'center'}}>
        //             <TouchableOpacity onPress={() => this.apiCallBirthday(this.state.baseUrl, this.state.fam_id)}>
        //                 <Icon style={{ marginTop:20}} name="redo" size={17} color="#fff" />
        //                 </TouchableOpacity>
        //             <Text style={{fontSize:18,marginTop:20,marginLeft:10}}>Retry</Text>
        //             </View>

        //         </View>
        //     );
        // }
        else if(this.state.connection_Status=='Online')  {
            return (
                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flex: 1, marginTop: 30, }}>
                    
                    {this.renderNotifications()}
                </ScrollView>
            )
        } 
        else{
            return (
                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flex: 1, marginTop: 30, }}>
                    
                    {this.renderNotifications()}
                </ScrollView>
            )

        }
    }else if(this.state.userRole=='user'){
        if (this.state.loadingStatus) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator size='large' />
                </View>
            );
        }
        else if(this.state.response1.length==0){
            return (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                             <Text style={{fontSize:16}}>No New Notification</Text>
                             </View>
            )

        }
        // else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

        //     return (
        //         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        //             <Text style={{fontSize:18}}>You are offline</Text>
        //             <View style={{ flexDirection: 'row', alignItems: 'center'}}>
        //             <TouchableOpacity onPress={() => this.apiCallBirthday(this.state.baseUrl, this.state.fam_id)}>
        //                 <Icon style={{ marginTop:20}} name="redo" size={17} color="#fff" />
        //                 </TouchableOpacity>
        //             <Text style={{fontSize:18,marginTop:20,marginLeft:10}}>Retry</Text>
        //             </View>

        //         </View>
        //     );
        // }
        else if(this.state.connection_Status=='Online')  {
            return (
                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flex: 1, marginTop: 30, }}>
                    
                    {this.renderNotifications()}
                </ScrollView>
            )
        } 
        else{
            return (
                <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{ flex: 1, marginTop: 30, }}>
                    
                    {this.renderNotifications()}
                </ScrollView>
            )

        }


    }
    }
    render() {
        return (
            
            <View style={{ flex: 1 }}>
                 {/* <View style={{ position: 'absolute', marginLeft: '72%', marginTop: -20, marginBottom: 20 }}> */}
                    <View style={{ position: 'absolute', marginLeft: '75%'}}>
                       <TouchableOpacity onPress={() => this.apiCallBirthday()}><Icon style={{ marginLeft: '40%' }} name="redo" size={17} color="#fff" /></TouchableOpacity>
                    </View>
            {this.renderContents()}
            </View>
        );
    }
}