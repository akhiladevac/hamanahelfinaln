/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, ScrollView, ActivityIndicator, TouchableOpacity, Dimensions, Alert, BackHandler, ToastAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Notifications from './Notifications';
import ListMembers from '../../Scenes/UsersList/ListMembers';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
const DATA_PROFILE = '@profile_list';
const DATA_WISH_LIST = '@wish_list';
const DATA_LIST = '@memlist_list';

const DATA_LIST_MEMBERS = '@members_list';
const DATA_NOTIFICATION = '@notification_list';
const DATA_VISIT = '@visit_list';

const DATA_EVENT = '@event_list';
const DATA_EVENT_LIST = '@eventlist_list';

const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
const MAX_HEIGHT = Dimensions.get('window').height;
const MAX_WIDTH = Dimensions.get('window').width;

export default class Home extends Component {

    constructor() {
        super();

        this.state = {
            loadingStatus: false,
            responseUser:[],
            userRole: '',
            fam_id: '',
            loadingStatusNotify: false,
            baseUrl: '',
            responsenotification:[],
            eventsData1:[],
            response1:[],
            response:[],
            person_id:''
        }
    }

    componentDidMount() {
        console.log("home");
        this.checkUserRole();   
        // this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     
    }
    
    //   componentWillUnmount() {
    //     this.backHandler.remove();
    //   }
    
    //   handleBackPress = () => {
    //     if (Actions.currentScene !== '_Home') {
    //     } else {
    //     //     ToastAndroid.show('Press again to exit', ToastAndroid.SHORT);
    //     //   setTimeout(() => { BackHandler.exitApp(); }, 1000);
    //       return true;
    //     }
    // }

    async checkUserRole() {
        try {
            
            var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
            if (loginCredentials) {
            var login_credentials = JSON.parse(loginCredentials);
            this.setState({ userRole: login_credentials.role, fam_id: login_credentials.family_id, 
            person_id:login_credentials.person_id}); 
            this.getBaseUrl();
            } else {
               
            }
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////
    async getBaseUrl() {
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            this.setState({ baseUrl: baseUrl });
            this.getMemberList(baseUrl);
            this.getMemList(baseUrl);

            
            this.getProfileDetail(baseUrl,this.state.fam_id)
               this.getNotificationList(baseUrl);
            this.getEventList(baseUrl);
            this.getVisitList(baseUrl);
             this.getEventPriestList(baseUrl);
              this.getWishList(baseUrl);
            
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
    }

    async getMemberList(baseUrl) {
        
        try {
            const memberList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
            if (memberList === null) {
                 this.callApiMembers(baseUrl);
            } else {
            this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
    async getMemList(baseUrl) {
        
        try {
            const memList = await AsyncStorage.getItem(DATA_LIST);
            if (memList === null) {
                 this.callApiMembersList(baseUrl);
            } else {
            this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
    callApiMembersList(baseUrl) {
            
        this.setState({ loadingStatus: true });
        var endpoint = 'member_list.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            
            if (responseJson.status === 200) {
            this.storeItemsMembers(responseJson.response, DATA_LIST);
            }
            else {
            const error = 'Async Storage Failed';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    async storeItemsMembers(response, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(response));
        this.setState({ loadingStatus: false });
    }
    async getWishList(baseUrl) {
        
        try {
            const wishList = await AsyncStorage.getItem(DATA_WISH_LIST);
            if (wishList === null) {
                 this.callApiWishList(baseUrl);
            } else {
            this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
    /////////////////////////////////////////////////////////////////////
    callApiWishList(baseUrl) {
    
        // this.setState({ loadingStatus: true });
        var endpoint = 'wish_list.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        //console.log(url);
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('seeet2 : ' + responseJson);
            console.log(responseJson);
            if (responseJson.status === 200) {
                this.storeWishList(responseJson.response, DATA_WISH_LIST);

            
            } else {
              
            }
        })
        .catch((error) => {
            console.error(error);
        });
       
    }
    async storeWishList(responsepr, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(responsepr));
        this.setState({ loadingStatus: false });
    }
    
    async getEventPriestList(baseUrl) {
        
        try {
            const eventPriestList = await AsyncStorage.getItem(DATA_EVENT_LIST);
            if (eventPriestList === null) {
                 this.callApiEventsListPriest(baseUrl);
            } else {
            this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
    callApiEventsListPriest(baseUrl){
        this.setState({  response: [] , eventsData2:[]});
        var endpoint = 'birthday_wish.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.storeEventPriest(responseJson.response, DATA_EVENT_LIST);

                    // this.storeItems(responseJson.response, DATA_LIST_MEMBERS);
                    
            } else {
              this.setState({ 
                
                  loadingStatus:false
              });
                
            }
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
      
  
    }
    async storeEventPriest(responsepr, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(responsepr));
        this.setState({ loadingStatus: false });
    }
      
    viewVisitsApiCall(baseUrl) {
        var endpoint = 'visit_unique.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('view visits api call');
            console.log(responseJson);
            if (responseJson.status === 200) {
                this.storeVisit(responseJson.response, DATA_VISIT);

                // this.setState({ 
                //     notificationData: responseJson.response, 
                //     loadingStatus: false
                // });

             
                
            } else {
              
            }
            
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
      
    }
    async storeVisit(responsepr, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(responsepr));
        this.setState({ loadingStatus: false });
    }
    async getVisitList(baseUrl) {
        
        try {
            const visitList = await AsyncStorage.getItem(DATA_VISIT);
            if (visitList === null) {
                 this.viewVisitsApiCall(baseUrl);
            } else {
            this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
    callApiProfilePriest(baseUrl, person_id) {
       

        //const { baseUrl, fam_id } = this.state;
        // this.setState({ loadingStatus: true });
        var endpoint = 'priest_profile.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Person_Id', person_id);
        
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {

            if (responseJson.status === 200) {
                this.storeProfile(responseJson.response[0], DATA_PROFILE);
            }

            else {
            const error = 'Newtork error';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    async getProfileDetail(baseUrl,fam_id) {
        
        try {
            const profileDetails = await AsyncStorage.getItem(DATA_PROFILE);
            if (profileDetails === null) {
                if(this.state.userRole=='priest'){
                    this.callApiProfilePriest(baseUrl,this.state.person_id);


                }
                this.callApiProfile(baseUrl,fam_id);

            } else {
            this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
    async callApiCall(baseUrl) {
        this.setState({ loadingStatus: true, response: [] });

        var endpoint = 'notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
       

        .then((response) => response.json())
        
        .then((responseJson) => {
            if (responseJson.status === 200) {          
                  this.storeNotification(responseJson.response, DATA_NOTIFICATION);

          
        }

            // alert(responseJson.response.msg)

            // console.log(responseJson);
        }) 
        
        .catch((error) => {
          console.error(error);
        });
        // setTimeout(() => abort, 1000)
    }
    async storeNotification(response, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(response));

    }
    async getNotificationList(baseUrl) {
        
        try {
            const notificationList = await AsyncStorage.getItem(DATA_NOTIFICATION);
            if (notificationList === null) {
                if(this.state.userRole=='priest'){
                    this.callApiCall(baseUrl);

                }else if(this.state.userRole=='user'){
                    this.apiCallBirthdayUser(baseUrl,this.state.fam_id);
                }
               

            } else {
            this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    async apiCallBirthdayUser(baseUrl, fam_id) {

        this.setState({  responseUser: [] });
        var endpoint = 'wish_notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    responseUser: responseJson.response,
                    birthdayResponse: responseJson.response,
                    
                });
                this.notificationCategory(baseUrl,fam_id)
    
                this.allNotification(baseUrl);
            } else {
                this.notificationCategory(baseUrl,fam_id)
    
                this.allNotification(baseUrl);
            }
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
       
    
    }
    async allNotification(baseUrl) {
    
        var endpoint = 'all_notification.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    responseUser: this.state.responseUser.concat(responseJson.response), 
                    
                   
                });
                this.storeNotification(this.state.responseUser, DATA_NOTIFICATION);
    
                //   this.notificationCategory(baseUrl,this.state.fam_id)
            } else {
                this.setState({ 
                    responseUser: this.state.responseUser, 
                    loadingStatus: false,
                    
                   
                });
                this.storeNotification(this.state.responseUser, DATA_NOTIFICATION);
    
                //    this.notificationCategory(baseUrl,this.state.fam_id)
    
            }
             console.log("allnotif"+JSON.stringify(responseJson) );
        })
        .catch((error) => {
          console.error(error);
        });
        
    
    }
    async notificationCategory(baseUrl, fam_id) {
    
        var endpoint = 'notification_category.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                    responseUser: this.state.responseUser.concat(responseJson.response), 
                   
                });
                
            } else {
               
            }
             console.log("gdhtdft"+JSON.stringify(responseJson) );
        })
        .catch((error) => {
          console.error(error);
        });
        
    }
    async getEventList(baseUrl) {
        
        try {
            const eventList = await AsyncStorage.getItem(DATA_EVENT);
            if (eventList === null) {
                 this.callApiEventsList(baseUrl);
            } else {
            // this.setState({ loadingStatus: false });
            }
        } catch (error) {
            console.error(error);
        }
    }
//     async getNotificationList(baseUrl) {
        
//         try {
//             const eventList = await AsyncStorage.getItem(DATA_EVENT);
//             if (eventList === null) {
//                  this.apiCall(baseUrl);
//             } else {
//             // this.setState({ loadingStatus: false });
//             }
//         } catch (error) {
//             console.error(error);
//         }
//     }
//     async apiCall(baseUrl) {
//     this.setState({ loadingStatus: true, responsenotification: [] });

//     var endpoint = 'notification.php';
//     var url = baseUrl + endpoint;
//     // console.log(url);
//     var data = new FormData();
//     data.append('hashcode', '##church00@');
//     fetch(url, {
//     method: 'POST',
//     headers: {
//         Accept: 'application/json',
//         'Content-Type': 'multipart/form-data',
//     },
//     body: data,
//     })
   

//     .then((response) => response.json())
    
//     .then((responseJson) => {
//         // console.log(responseJson);
//         this.setState({ 
//              response: responseJson.response, 
//         //  response: responseJson.response, 

//             // loadingStatus: false,
//             //   responseNotification:this.state.response.concat(responseJson.response),
//         })
//         this.storeNotification(this.state.response, DATA_NOTIFICATION);

//         // alert(responseJson.response.msg)

//         // console.log(responseJson);
//     }) 
    
//     .catch((error) => {
//       console.error(error);
//     });
//     // setTimeout(() => abort, 1000)
// }
// async storeNotification(response, KEY) {
//     await AsyncStorage.setItem(KEY, JSON.stringify(response));
//     alert(JSON.stringify(response))
// }

   
    callApiEventsList(baseUrl){
        var endpoint = 'birthday_list.php';
        var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('bithday');
            // console.log(responseJson);
            if (responseJson.status === 200) {
                this.setState({ 
                  eventsData1: responseJson.response,
                });
                console.log("jj"+responseJson.response)
                this.storeEvent(responseJson.response, DATA_EVENT);
    
            } else {
              
    
            }
            // console.log(responseJson);
        })
        .catch((error) => {
          console.error(error);
        });
    }          
    async storeEvent(response, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(response));

    }
    
   
    callApiMembers(baseUrl) {
    
        // this.setState({ loadingStatus: true });
        var endpoint = 'list_of_members.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', 1);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('seeet2 : ' + responseJson);
            console.log(responseJson);
            if (responseJson.status === 200) {
            this.storeItems(responseJson.response, DATA_LIST_MEMBERS);
            }
            else {
            const error = 'Async Storage Failed';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    async storeProfile(responsepr, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(responsepr));
        this.setState({ loadingStatus: false });
    }
   
    callApiProfile(baseUrl,fam_id) {
    
        // this.setState({ loadingStatus: true });
        var endpoint = 'profile.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', fam_id);
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('seeet2 : ' + responseJson);
            console.log(responseJson);
            if (responseJson.status === 200) {
                this.storeProfile(responseJson.response[0], DATA_PROFILE);
            }
            else {
            const error = 'Async Storage Failed';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    
    //this function for save the array response of members list to AsyncStorage
    async storeItems(response, KEY) {
        await AsyncStorage.setItem(KEY, JSON.stringify(response));
        // this.setState({ loadingStatus: false });
    }
    // async storeNotification(response, KEY) {
    //     await AsyncStorage.setItem(KEY, JSON.stringify(response));
    // }
    renderPlusButton() {
        if (this.state.userRole === 'priest') {

            var width = MAX_WIDTH - (MAX_WIDTH/5);
            var height = MAX_HEIGHT - (MAX_HEIGHT/5);
            //(70 + 90);
            return (
                <TouchableOpacity  onPress={()=> Actions.VisitNotification()} style={{ position: 'absolute', marginLeft: width, marginTop: height }}>
                
                    <View style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0ea0cc', borderRadius: 50 }}>
                        <Text style={{ fontSize: 25, color: '#fff', fontWeight: 'bold'}}>+</Text>
                    </View>
                
                </TouchableOpacity>
            )
        }
    }  

    render() {
        if (this.state.loadingStatus) {
            return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size='large' />
            </View>
            )  
        } else {
            return (
                <View style={{ flex: 1 }}>
                <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                <View style={{ backgroundColor: '#7BCFE8', borderBottomLeftRadius: 20, borderBottomRightRadius: 20 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#7BCFE8', height: 80, flexDirection: 'row' }}>
                <Text style={{ fontSize: 25, fontWeight: 'bold' ,marginLeft:10}}>HaManahel</Text>

                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={{ paddingTop: 1, paddingLeft: 22,}}> Notifications</Text>
                    
                </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8',height: 280, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                            <Notifications role={this.state.userRole}/> 
                        </View>
                    </View>
                    <ListMembers/>
                </ScrollView>
                {this.renderPlusButton()}
                </View>
            );
        }
    }
}