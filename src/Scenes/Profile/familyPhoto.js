
import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, ActivityIndicator,BackHandler } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import { Actions } from 'react-native-router-flux';
const DATA_BASE_URL = '@base_url';

export default class FamilyPhoto extends Component {

    constructor() {
        super();

        this.state = {
            fam_photo: '',
            baseUrl: '',
            image_loading: false,
            abc:'',
            image2:'',
            response3:''
        }
    }

    UpdateData = () => {
        this.props.parentCallback("updated");
      
      }
    componentDidMount() {
        this.getBaseUrl();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

      
    }
    componentWillUnmount() {
        this.backHandler.remove();
    }
    handleBackPress = () => {
        
        if (Actions.currentScene !== 'familyPhoto') {
        } 
        else 
        {
            this.UpdateData();

            Actions.pop();

      
          return true;
        }
      }
    familyPhotoApi(baseUrl) {
          this.setState({fam_photo:''})
          
        var endpoint = 'show_family_photo.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', this.props.datas.Fam_Id); 
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.status === 200) {
              this.setState({
                 fam_photo: baseUrl + responseJson.response[0].family_photo,
                 image_loading:false
              })
              console.log(this.state.fam_photo)

            }
            else {
            const error = 'Newtork error';
            this.setState({
                image_loading:false
             })
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }

    async getBaseUrl() {
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            this.setState({ baseUrl: baseUrl, fam_photo: baseUrl + this.props.datas.members[0].family_photo });
           
             this.familyPhotoApi(baseUrl);
            
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
      }

      addFamilyPhoto = () => {
        const options = {
            title: 'Select Image',
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
          };
          ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else {
              const source = { uri: response.uri };
          
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
              console.log("iamgepath"+response.uri);
              this.setState({
                // abc: response.uri,

                 
                 response3:response.uri
              });
            
              this.UpdateFamilyPhoto(response.uri);
              
            }
          })
    };

    UpdateFamilyPhoto(file) {
        const { baseUrl } = this.state;
         this.setState({ image_loading: true });
        var endpoint = 'family_photo.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', this.props.datas.Fam_Id);
        data.append('photo_visibility', 0);
        data.append('file', {
            uri: file,
             type: 'image/png' || 'image/jpg',
             name: 'fam_photo.png' || 'fam_photo.jpg'
          });
        
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.status === 200) {
                // this.familyPhotoApi(this.state.baseUrl)
this.showupalert();
                // Alert.alert('Updated Successfully.Allow few minutes for changes to take Effect. ');

              this.setState({ image_loading: false });

                this.familyPhotoApi(this.state.baseUrl)

              //Alert.alert('photo updated successfully');

            // this.UpdateData();
            }
            else {
            const error = 'Newtork error';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
    deleteFamilyPhoto() {
        Alert.alert(  
            'Delete ',  
            'Do you really want to delete this Family Photo?',  
            [  
                {  
                    text: 'No',  
                    onPress: () => console.log('Cancel Pressed'),  
                    style: 'cancel',  
                },  
                {text: 'Yes', onPress: () => this.deletePhoto()},  
            ]  
        ); 
    }
    Updatealert(){
        Alert.alert(  
            ' ',  
            'Deleted Successfully.Allow few minutes for changes to take effect',  
            [  
                {  
                   
                },  
                {text: 'ok', },  
            ]  
        ); 

    }
    showupalert (){
        Alert.alert(  
            ' ',  
            'Updated Successfully.Allow few minutes for changes to take effect',  
            [  
                {  
                   
                },  
                {text: 'ok', },  
            ]  
        ); 

    }
    deletePhoto(){
        const { baseUrl } = this.state;
        //  this.setState({ image_loading: true });
        var endpoint = 'delete_family_photo.php';
        var url = baseUrl + endpoint;
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id', this.props.datas.Fam_Id);
       
        
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log(responseJson);
            if (responseJson.status === 200) {

              //his.setState({ response: responseJson.response[0], loadingStatus: false });
              this.Updatealert();
            //   Alert.alert(' Deleted Successfully.Allow few minutes for changes to take effect ');
              this.setState({
                  fam_photo:this.state.baseUrl + this.props.datas.members[0].family_photo,
                  image_loading:false
              })
            //   this.familyPhotoApi(this.state.baseUrl);

              console.log(this.state.fam_photo)

            //    this.familyPhotoApi(this.state.baseUrl)
            this.UpdateData();
            }
            else {
            const error = 'Newtork error';
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }


    renderIndicator() {
        
        if (this.state.image_loading) {
            return (
                <View style={{ marginTop: 200 }}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    render() {
        // if (this.state.abc) {
        //     var profPic =  { uri: this.state.abc };
        // } else if (this.state.fam_photo) {
        //         var profPic =  { uri: this.state.fam_photo};
        //     } else {
        //         var profPic = require('../../img/avatar.png');
            
        // }
        
         const { fam_photo ,response3} = this.state;
        
         if (fam_photo) {
            var famPhoto =  {
                uri: fam_photo
            };
        
        } else {
            var famPhoto = require('../../img/avatar.png');
        }
        return (
            <View style={{ flex: 1, alignItems: 'center' }}>
                <View  
                style={{ height: 400, width: '100%', backgroundColor: '#7BCFE8', justifyContent: 'center', alignItems: 'center' }}>
                    {this.renderIndicator()}
                    <Image 
                         onLoadEnd={() => this.setState({ image_loading: false })}
                        style={{ height: 380, width: '95%', borderRadius: 10 }}
                        source={famPhoto }
                    />
                    
                </View>

                <TouchableOpacity onPress={() => this.addFamilyPhoto()} style={{ justifyContent: 'center', alignItems: 'center', height: 50, width: '90%', borderWidth: 1, borderColor: '#7BCFE8', borderRadius: 10, marginTop: 10 }}>
                    <Text>Change Photo</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.deleteFamilyPhoto()} style={{ justifyContent: 'center', alignItems: 'center', height: 50, width: '90%', borderWidth: 1, borderColor: '#7BCFE8', borderRadius: 10, marginTop: 10 }}>
                    <Text>Delete Photo</Text>
                </TouchableOpacity>

            </View>
            
        )
    }
}
// import React, { Component } from 'react';
// import { View, Image, Text, TouchableOpacity, Alert, ActivityIndicator,BackHandler } from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';
// import ImagePicker from 'react-native-image-picker';
// import { Actions } from 'react-native-router-flux';

// const DATA_BASE_URL = '@base_url';

// export default class FamilyPhoto extends Component {

//     constructor() {
//         super();

//         this.state = {
//             fam_photo: '',
//             baseUrl: '',
//             image_loading: true,
//         }
//     }

//     componentDidMount() {
//         this.getBaseUrl();
//         this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

//         // console.log('family photo');
//         // console.log(this.props.datas);
//         // console.log(this.props.datas.members[0].family_photo);

//         // this.setState({
//         //     fam_photo: this.props.datas.members[0].family_photo,
//         // })
//         // console.log(this.state.fam_photo);
//     }
//     componentWillUnmount() {
//         this.backHandler.remove();
//     }
//     handleBackPress = () => {
        
//         if (Actions.currentScene !== 'familyPhoto') {
//         } 
//         else 
//         {
//             Actions.pop();

      
//           return true;
//         }
//       }
//     familyPhotoApi(baseUrl) {

//         var endpoint = 'show_family_photo.php';
//         var url = baseUrl + endpoint;
//         var data = new FormData();
//         data.append('hashcode', '##church00@');
//         data.append('Fam_Id', this.props.datas.Fam_Id); 
//         fetch(url, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'multipart/form-data',
//         },
//         body: data,
//         })
//         .then((response) => response.json())
//         .then((responseJson) => {
//             console.log(responseJson);
//             if (responseJson.status === 200) {
//               this.setState({
//                 fam_photo: baseUrl + responseJson.response[0].family_photo,
//               })
//             }
//             else {
//             const error = 'Newtork error';
//             }
//         })
//         .catch((error) => {
//             console.error(error);
//         });
//     }

//     async getBaseUrl() {
//         try {
//             let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
//             this.setState({ baseUrl: baseUrl, fam_photo: baseUrl + this.props.datas.members[0].family_photo });
//             this.familyPhotoApi(baseUrl);
            
//         } catch (error) {
//             console.error('Something went Wrong in Get Section' + error);
//         }
//       }

//       addFamilyPhoto = () => {
//         const options = {
//             title: 'Select Image',
//             storageOptions: {
//               skipBackup: true,
//               path: 'images',
//             },
//           };
//           ImagePicker.showImagePicker(options, (response) => {
//             console.log('Response = ', response);
          
//             if (response.didCancel) {
//               console.log('User cancelled image picker');
//             } else if (response.error) {
//               console.log('ImagePicker Error: ', response.error);
//             } else {
//               const source = { uri: response.uri };
          
//               // You can also display the image using data:
//               // const source = { uri: 'data:image/jpeg;base64,' + response.data };
//               console.log(response);
//               this.setState({
//                 fam_photo: response.uri,
//               });
            
//               this.UpdateFamilyPhoto(response.uri);
              
//             }
//           })
//     };

//     UpdateFamilyPhoto(file) {
//         const { baseUrl } = this.state;
//         this.setState({ loadingStatus: true });
//         var endpoint = 'family_photo.php';
//         var url = baseUrl + endpoint;
//         var data = new FormData();
//         data.append('hashcode', '##church00@');
//         data.append('Fam_Id', this.props.datas.Fam_Id);
//         data.append('photo_visibility', 0);
//         data.append('file', {
//             uri: file,
//             type: 'image/jpg',
//             name: 'fam_photo.jpg'
//           });
        
//         fetch(url, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'multipart/form-data',
//         },
//         body: data,
//         })
//         .then((response) => response.json())
//         .then((responseJson) => {
//             console.log(responseJson);
//             if (responseJson.status === 200) {
//               //his.setState({ response: responseJson.response[0], loadingStatus: false });
//               //Alert.alert('photo updated successfully');
//             }
//             else {
//             const error = 'Newtork error';
//             }
//         })
//         .catch((error) => {
//             console.error(error);
//         });
//     }

//     renderIndicator() {
        
//         if (this.state.image_loading) {
//             return (
//                 <View style={{ marginTop: 200 }}>
//                     <ActivityIndicator size='large' />
//                 </View>
//             )
//         }
//     }

//     render() {
//         const { fam_photo } = this.state;
        
//         if (fam_photo) {
//             var famPhoto =  {
//                 uri: fam_photo
//             };
        
//         } else {
//             var famPhoto = require('../../img/avatar.png');
//         }
//         return (
//             <View style={{ flex: 1, alignItems: 'center' }}>
//                 <View style={{ height: 400, width: '100%', backgroundColor: '#7BCFE8', justifyContent: 'center', alignItems: 'center' }}>
//                     {this.renderIndicator()}
//                     <Image 
//                         onLoadEnd={() => this.setState({ image_loading: false })}
//                         style={{ height: 380, width: '95%', borderRadius: 10 }}
//                         source={famPhoto}
//                     />
                    
//                 </View>

//                 <TouchableOpacity onPress={() => this.addFamilyPhoto()} style={{ justifyContent: 'center', alignItems: 'center', height: 50, width: '90%', borderWidth: 1, borderColor: '#7BCFE8', borderRadius: 10, marginTop: 10 }}>
//                     <Text>Change Photo</Text>
//                 </TouchableOpacity>

//             </View>
            
//         )
//     }
// }



// // import React, { Component } from 'react';
// // import { View, Image, Text, TouchableOpacity, Alert, ActivityIndicator,BackHandler } from 'react-native';
// // import AsyncStorage from '@react-native-community/async-storage';
// // import ImagePicker from 'react-native-image-picker';
// // import { Actions } from 'react-native-router-flux';

// // const DATA_BASE_URL = '@base_url';

// // export default class FamilyPhoto extends Component {

// //     constructor() {
// //         super();
// //         this.updateFamily = this.updateFamily.bind(this);

// //         this.state = {
// //             fam_photo: '',
// //             baseUrl: '',
// //             image_loading: '',
// //             visible:false,
// //             image:'',
// //             abc:'',
// //         }
// //     }

// //     componentDidMount() {
// //         this.getBaseUrl();
// //         this.setState({
// //             image: this.props.datas.members[0].family_photo,
// //             //imageVisible: this.props.datas.,
// //             });
        
// //         this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

       
// //     }
   
// //     componentWillUnmount() {
// //         this.backHandler.remove();
// //     }
// //     handleBackPress = () => {
        
// //         if (Actions.currentScene !== 'familyPhoto') {
// //         } 
// //         else 
// //         {
// //             // this.UpdateData();
// //      Actions.pop()
      
// //           return true;
// //         }
// //       }
// //       updateFamily(){
// //           this.getBaseUrl();
// //       }
// //     familyPhotoApi(baseUrl) {

// //         var endpoint = 'show_family_photo.php';
// //         var url = baseUrl + endpoint;
// //         var data = new FormData();
// //         data.append('hashcode', '##church00@');
// //         data.append('Fam_Id', this.props.datas.Fam_Id); 
// //         fetch(url, {
// //         method: 'POST',
// //         headers: {
// //             Accept: 'application/json',
// //             'Content-Type': 'multipart/form-data',
// //         },
// //         body: data,
// //         })
// //         .then((response) => response.json())
// //         .then((responseJson) => {
// //             console.log(responseJson);
// //             if (responseJson.status === 200) {
// //               this.setState({
// //                 fam_photo: baseUrl + responseJson.response[0].family_photo,
                
// //               })
// //             //   alert(this.state.fam_photo)
// //             //   this.UpdateData();
// //             }
// //             else {
// //             const error = 'Newtork error';
// //             }
// //         })
// //         .catch((error) => {
// //             console.error(error);
// //         });
// //     }
// //     UpdateData = () => {
// //         this.props.parentCallback("updated");
      
// //       }
// //     async getBaseUrl() {
// //         try {
// //             let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
// //             this.setState({ baseUrl: baseUrl});
       
// //             this.familyPhotoApi(baseUrl);
            
// //         } catch (error) {
// //             console.error('Something went Wrong in Get Section' + error);
// //         }
// //       }

// //       addFamilyPhoto = () => {
// //         const options = {
// //             title: 'Select Image',
// //             storageOptions: {
// //               skipBackup: true,
// //               path: 'images',
// //             },
// //           };
// //           ImagePicker.showImagePicker(options, (response) => {
// //             console.log('Response = ', response);
          
// //             if (response.didCancel) {
// //               console.log('User cancelled image picker');
// //             } else if (response.error) {
// //               console.log('ImagePicker Error: ', response.error);
// //             } else {
// //               const source = { uri: response.uri };
          
// //               // You can also display the image using data:
// //               // const source = { uri: 'data:image/jpeg;base64,' + response.data };
// //               console.log(response);
// //               this.setState({
// //                 abc:response.uri,
// //                 fam_Photo: response.uri,
// //                 visible:true
// //               });
            
// //               this.UpdateFamilyPhoto(this.state.fam_photo);
              
// //             }
// //           })
// //     };

// //     UpdateFamilyPhoto(file) {
// //          const { baseUrl } = this.state;
// //         this.setState({ loadingStatus: true });
// //         var endpoint = 'family_photo.php';
// //         var url = baseUrl + endpoint;
// //         var data = new FormData();
// //         data.append('hashcode', '##church00@');
// //         data.append('Fam_Id', this.props.datas.Fam_Id);
// //         data.append('photo_visibility', 0);
// //         data.append('file', {
// //             uri: this.state.abc,
// //             // uri: file,
// //             type: 'image/jpg',
// //             name: 'fam_photo.jpg'
// //           });
        
// //         fetch(url, {
// //         method: 'POST',
// //         headers: {
// //             Accept: 'application/json',
// //             'Content-Type': 'multipart/form-data',
// //         },
// //         body: data,
// //         })
// //         .then((response) => response.json())
// //         .then((responseJson) => {
// //             console.log(responseJson);
// //             if (responseJson.status === 200) {
// //               //his.setState({ response: responseJson.response[0], loadingStatus: false });
// //               //Alert.alert('photo updated successfully');
// //               this.familyPhotoApi(baseUrl);


             
// //             }
// //             else {
// //             const error = 'Newtork error';
// //             }
// //         })
// //         .catch((error) => {
// //             console.error(error);
// //         });
// //     }

// //     renderIndicator() {
        
// //         if (this.state.image_loading) {
// //             return (
// //                 <View style={{ marginTop: 200 }}>
// //                     <ActivityIndicator size='large' />
// //                 </View>
// //             )
// //         }
// //     }

// //     render() {
// //         const { fam_photo } = this.state;
        
// //         if (fam_photo) {
// //             var famPhoto =  {
// //                 uri: fam_photo
// //             };
        
// //         } else {
// //             var famPhoto = require('../../img/avatar.png');
// //         }
// //     //      const { fam_photo ,visible,baseUrl,image} = this.state;
// //     //     // if(visible==false){
// //     //     //     var famPhoto =  {
// //     //     //         uri: fam_photo
// //     //     //     };
// //     //     // }else if(visible==true){
// //     //     //     var famPhoto =  {
// //     //     //         uri: fam_photo
// //     //     //     };
// //     //     // // }
// //     //     // if (this.state.image) {
// //     //     //     var profPic =  { uri: this.state.image };
// //     //     // } else {
// //     //     //     if (this.props.datas.members[0].family_photo) {
// //     //     //         var profPic =  { uri: this.state.baseUrl + this.props.datas.members[0].family_photo};
// //     //     //     } else {
// //     //     //         var profPic = require('../../img/avatar.png');
// //     //     //     }
// //     //     // }
// //     //     if (this.props.datas.members[0].family_photo) {
// //     //         var profPic =  {
// //     //             uri:this.state.abc
// //     //         }
// //     //  alert(this.props.datas.family_photo)
// //     //     } else {
// //     //         var profPic = require('../../img/avatar.png');
// //     //     }
       
 
// //     // //     if (fam_photo) {
// //     // //         var famPhoto =  {
// //     // //             uri: fam_photo
// //     // //         };
            
// //     // //     }else if (this.props.datas.members[0].family_photo){
// //     // //     var famPhoto=
// //     // //     {
// //     // //         uri: fam_photo
// //     // //     };
// //     // // }
    
// //     // //      else {
// //     // //         var famPhoto = require('../../img/avatar.png');
// //     // //     }
// //     // //     if (this.state.fam_photo=='') {
// //     // //         var fam_Photo=
// //     // //     {
// //     // //         uri:this.state.baseUrl + this.props.datas.members[0].family_photo
// //     // //     };
// //     // //     // alert(this.state.baseUrl)
// //     // //     this.setState({
// //     // //         fam_photo:fam_Photo
// //     // //     })
// //     // // }
// //     // //     else {
// //     // //         // var mobileNumber = this.props.data.Phone;
// //     // //     }
   
// //          return (
// //             <View style={{ flex: 1, alignItems: 'center' }}>
// //                 <View style={{ height: 400, width: '100%', backgroundColor: '#7BCFE8', justifyContent: 'center', alignItems: 'center' }}>
// //                     {this.renderIndicator()}
// //                     <Image 
// //                         // onLoadEnd={() => this.setState({ image_loading: false })}
// //                         style={{ height: 380, width: '95%', borderRadius: 10 }}
// //                         source={famPhoto}
// //                     />
                    
// //                 </View>

// //                 <TouchableOpacity onPress={() => this.addFamilyPhoto()} style={{ justifyContent: 'center', alignItems: 'center', height: 50, width: '90%', borderWidth: 1, borderColor: '#7BCFE8', borderRadius: 10, marginTop: 10 }}>
// //                     <Text>Change Photo</Text>
// //                 </TouchableOpacity>

// //             </View>
            
// //         )
// //     }
// // }