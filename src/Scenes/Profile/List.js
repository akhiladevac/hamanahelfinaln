import React, { Component } from 'react';
import { View, Text , Image, FlatList, TouchableOpacity,BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

import Dialog, {
    DialogTitle,
    DialogContent,
    DialogFooter,
    DialogButton,
    SlideAnimation,
    ScaleAnimation,
  } from 'react-native-popup-dialog';
  import Modal, { ModalContent } from 'react-native-modals';


export default class List extends Component {
    constructor() {
        super();

        this.state = {
            baseUrl: 'https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/',
            propicture:'',
            slideAnimationDialog: false,
    
        }
    }
    showPicture(profPic){
        this.setState({ slideAnimationDialog: true,propicture:profPic})
    
      }
      UpdateData = () => {
        this.props.visibleotp1("hgvhjjjjjjjjj");
      
      }
      
componentDidMount(){
    
     this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

}
componentWillUnmount() {
    this.backHandler.remove();
   }
   handleBackPress = () => {
   
    if (Actions.currentScene !== '_Profile') {
        
    } else {
      if(this.state.slideAnimationDialog==true ){
        this.setState({ slideAnimationDialog: false });


      }
else{
  this.UpdateData()

  Actions.pop();

}

  
      return true;
    }
  }

renderList=({item}) => {

    if (item.Photo) {
        var profPic =  { uri: this.props.baseUrl + item.Photo };
    } else {
            var profPic = require('../../img/avatar.png');
        }
   
    return(
        <View style={{ justifyContent: 'center', backgroundColor: '#fff', marginTop: 10, marginLeft: 10,marginRight: 10, borderRadius: 10, height: 70}}>
        <View style={{ alignItems: 'center', flexDirection: 'row'}}>
        <TouchableOpacity  onPress={()=> this.showPicture(profPic)}>

            <Image 
              style={{ height: 45, width: 45, marginLeft: 10, borderRadius: 20 }}
              source={profPic}
            /> 
            </TouchableOpacity>
           <View style={{ marginLeft: 10, width: '65%' }} >
           
                <Text style={{ fontSize: 17, fontWeight: '500' }}>{item.Person_Name}</Text>              
                <Text style={{ fontSize: 10, paddingLeft: 2,  paddingTop: 5 }}>{item.job}</Text>
                                 
           </View>   
           <TouchableOpacity onPress={() => Actions.EditProfile({ datas: item, baseUrl: this.props.baseUrl, parentCallback: this.props.parentCallback})}>
                <View style={{ width: 50, marginLeft: 2, height:50, justifyContent: 'center'}}>
                    <Icon style={{}} name="edit" size={16} color="#000" />

                </View>  
            </TouchableOpacity>
        </View>    
                 
        </View>                          
    );
}

    render() {
        return(
                <View style= {{ backgroundColor: '#EBEBEB', marginTop: 10, borderRadius:20}}>
                     {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
          onDismiss={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          onTouchOutside={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          visible={this.state.slideAnimationDialog}
          // dialogTitle={<DialogTitle title="" />}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
          <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                 
                 source={this.state.propicture}
                 />
          </DialogContent>
        </Dialog> */}
         {/* <Modal
    visible={this.state.slideAnimationDialog}
    onTouchOutside={() => {
      this.setState({ slideAnimationDialog: false });
    }}
  >
    <ModalContent>
    <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:7}}
                 
                 source={this.state.propicture}
                 />
    </ModalContent>
  </Modal> */}
   <Modal 
    visible={this.state.slideAnimationDialog}
    onTouchOutside={() => {
      this.setState({ slideAnimationDialog: false });
    }}
  >
    <ModalContent>
     
    <Image 
    style={{ width: 200, height: 200,marginTop:3}}
                 
                 source={this.state.propicture}
                 />
    </ModalContent>

  </Modal>
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{ paddingBottom: 25}}     

                        style={{marginTop: 10, marginBottom: 30}}                
                        data={this.props.abc}
                        renderItem={this.renderList}
                        keyExtractor={(item,index)=>index.toString()}
                    />
                </View>
        );
    }
}
    