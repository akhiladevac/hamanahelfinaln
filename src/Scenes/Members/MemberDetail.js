/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Platform, View, Text, Alert, Linking, Image, ScrollView, BackHandler, ActivityIndicator, TouchableOpacity, Modal, Dimensions } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import AsyncStorage from '@react-native-community/async-storage';
 import List from './List';
// import List from '../Members/List';
import ImageZoom from 'react-native-image-pan-zoom';


import { Actions } from 'react-native-router-flux';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';

const DATA_BASE_URL = '@base_url';
export default class MemberDetail extends Component {

    constructor() {
        super();

        this.state = {
            response: 'Joseph Augustine',
            abc: [],
            loadingStatus: false,
            slideAnimationDialog: false,

            baseUrl: '',
            propicture:'',
            slideAnimationDialog: false,
            openImageVIsibile: false,
            currentImageIndex: '',
            showimage:false
    
        }
    }

    componentDidMount() {

      this.getBaseUrl()
      // this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     

    }
    // componentWillUnmount() {
    //   this.backHandler.remove();
    // }
    
    // handleBackPress = () => {
     
    //   if (Actions.currentScene !== 'DetailedUser') {
    //       Actions.pop();
    //   } else {
    //     this.setState({ slideAnimationDialog: false });

    //     Actions.pop();
    //     return true;
    //   }
    // }
      
    
    

    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl }); 
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
    }

      callFuntion(number) {
          if (number !== null && number.length >= 8) {
            Linking.openURL(`tel:${number}`);
          } else {
            Alert.alert('Number is Empty');
          }
      }

      openImage() {
        this.setState({
           openImageVIsibile: true,
          
           });
        // console.log(index);
        
    }

      locationFunction() {
        // const latitude = '11.196927';
        // const longitude = '75.864387';

        var latitude = this.props.fam_details.members[0].latitude;
        var longitude = this.props.fam_details.members[0].longitude;

        if (latitude && longitude) {
            if (Platform.OS === 'ios') {
              Linking.openURL(`maps://app?saddr=10.115926+76.478652&daddr=${latitude}+${longitude}`)
            } else {
              Linking.openURL(`google.navigation:q=${latitude}+${longitude}`);
            }
        } else {
          Alert.alert('Location not available')
        }
   
      }

      familyPhoto() {

          if (this.props.fam_details.members[0].family_photo) {
            this.openImage()
          } else {
            Alert.alert('Family photo not available');
          }
      }

      renderZoomImage() {
        // var imgz = [{ url: this.state.baseUrl + this.props.fam_details.members[0].family_photo, width: Dimensions.get('window').width, height: 250 }]
        console.log('famPhoto test');
        // console.log(imgz);
        return (
            <Modal visible={this.state.openImageVIsibile} transparent={true}  onRequestClose={() => { this.setState({openImageVIsibile:false}) } }>
                 <ImageZoom cropWidth={Dimensions.get('window').width}
                       cropHeight={Dimensions.get('window').height}
                       imageWidth={350}
                       onSwipeDown={() => this.setState({ openImageVIsibile: false })}
                       enableSwipeDown
                       imageHeight={350}>
                <Image style={{width:350, height:350}}
                
                       source={{uri:this.state.baseUrl + this.props.fam_details.members[0].family_photo}}/>
            </ImageZoom>
            </Modal>
        )
      
    }

      

        render() {
          console.log(this.props.fam_details);
            const { loadingStatus } = this.state;
            if(loadingStatus) {
              return (
                <View style={{ flex: 1, justifyContent: 'center', marginTop: 300}}>
                   <ActivityIndicator size='large'/>
                </View>
               
              );
            } else {
              if (this.props.fam_details.members[0].photo) {
                var profPic =  { uri: this.props.baseUrl + this.props.fam_details.members[0].photo };
            } else {
                    var profPic = require('../../img/avatar.png');
                }
            
            return(
             <ScrollView showsVerticalScrollIndicator={false}>
              {this.renderZoomImage()}
               <View style= {{ alignItems: 'center', backgroundColor:'#7BCFE8',height: 350, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
                    <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8',height: 350, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                        <Image
                            style={{ height: 101, width: 101, borderRadius: 50 }}
                            source={profPic}
                        /> 
                        <Text style= {{ fontSize: 22, fontWeight: 'bold', marginTop: 10 }}>{this.props.fam_details.Fam_Owner}</Text>     
                        <Text style= {{ fontSize: 10, marginTop: 5 }}> {this.props.fam_details.members[0].Person_Occ}</Text> 
                        <Text style= {{ fontSize: 13, marginTop: 10 }}> {this.props.fam_details.Fam_Name} House</Text> 
                    </View>
                        <View style= {{marginLeft: '45%', marginTop: -50, flexDirection: 'row'}}>
  
                                  <TouchableOpacity onPress={() => this.familyPhoto(this.setState({showimage:true}))} style= {{ marginRight: 20, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: '#fff', height: 25, width: 65}}>
                                      <Text style= {{fontSize: 9}}>Family Photo</Text>
                                  </TouchableOpacity>

                                  <TouchableOpacity onPress={() => this.callFuntion(this.props.fam_details.members[0].phone)} style= {{ marginRight: 20, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: '#fff', height: 25, width: 46}}>
                                      <Text style= {{fontSize: 9}}>Call</Text>
                                  </TouchableOpacity>
                            
                                  <TouchableOpacity onPress={() => this.locationFunction()} style= {{ marginRight: 15, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: '#fff', height: 25, width: 46}}>
                                      <Text style= {{fontSize: 9}}>Location</Text>
                                  </TouchableOpacity>
                            
                        </View>
                    
                </View>
                    <List abc={this.props.fam_details.members} baseUrl={this.props.baseUrl} />
              </ScrollView>
            );

          }
        }
    }