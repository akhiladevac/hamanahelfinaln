import React, { Component } from 'react';
import { View, Text, Image, FlatList, TouchableOpacity, Alert, Linking,BackHandler } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Actions } from 'react-native-router-flux';

import Dialog, {
    DialogTitle,
    DialogContent,
    DialogFooter,
    DialogButton,
    SlideAnimation,
    ScaleAnimation,
  } from 'react-native-popup-dialog';
  import Modal, { ModalContent } from 'react-native-modals';

export default class List extends Component {
    constructor() {
        super();
        this.state = {
            propicture:'',
            slideAnimationDialog: false,
         
    
        }
    }
    showPicture(profPic){
        this.setState({ slideAnimationDialog: true, propicture:profPic})
    
      }
      componentDidMount(){
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     
    
    }
   
    componentWillUnmount() {
         this.backHandler.remove();
       }
       handleBackPress = () => {
       
        if (Actions.currentScene !== 'DetailedUser') {
            
        } else {
            if(this.state.slideAnimationDialog==true){
                this.setState({ slideAnimationDialog: false });

            }else{
                
                Actions.pop();

            }
    
    
      
          return true;
        }
      }
    
renderCall(phone) {
    if (phone) {
        return (
            <TouchableOpacity onPress={() => this.callFuntion(phone)}>
                <Icon style={{marginLeft:20}}   name="phone" size={20} color="#000" />
            </TouchableOpacity>
        )
    }
}
renderEmail(email) {
       if (email) {
        return (
            <TouchableOpacity onPress={() => this.emailFuntion(email)}>
                <Icon style={{marginLeft:0}} name="email" size={20} color="#000" />
            </TouchableOpacity>
        )
          }
}
callFuntion(number) {
console.log("list"+Actions.currentScene)
    if (number !== null && number.length >= 8) {
      Linking.openURL(`tel:${number}`);
    } else {
      Alert.alert('Number is Empty');
    }
}
emailFuntion(email) {
    if (email !== null ) {
        Linking.openURL('mailto:'+ email +'?subject=&body=');
                            }
     else {
      Alert.alert('Email is Empty');
    }
}
renderList=({item}) => {

    console.log('testttt');
    console.log(item);
    console.log("jygtu"+item.phone);
        if (item.photo) {
            var profPic =  { uri: this.props.baseUrl + item.photo };
        }  else {
            var profPic = require('../../img/avatar.png');
        }
    
    
    if (item.email && item.email !== '0') {
        var email = item.email;
    } else {
        var email = '';
    }
    if (item.phone && item.phone !== '0') {
        var phone = item.phone;
    } else {
        var phone = '';
    }
    if (item.Status === 'Y') {
        var color = '#fff'
    } else {
        var color = 'rgba(255,50,30,0.5)'
    }
    return(
        <View>
            <View style={{ backgroundColor: '#fff', marginTop: 10, marginLeft: 10,marginRight: 10, borderRadius: 10, height: 70 }}>
                <View style={{ alignItems: 'center', flexDirection: 'row', backgroundColor: color, height: 70, borderRadius: 10,}}>
                <TouchableOpacity  onPress={()=> this.showPicture(profPic)}>
                   <Image 

                    style={{ height: 45, width: 45, marginLeft: 10, borderRadius: 20 }}
                    source={profPic}
                    /> 
</TouchableOpacity>
                <View style={{ marginLeft: 10, width: '50%'}} >
                
                        <Text style={{ fontSize: 17, fontWeight: '500' }}>{item.Person_Name}</Text>
                        <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Person_Occ}</Text>
                
                </View>
           <View style={{flexDirection:'row',marginLeft:40}}>
           {this.renderEmail(email)}

                {this.renderCall(phone)}
                </View>
                
                </View>
            </View>
        </View>
    );
}

    render() {
        return(
                <View style= {{ backgroundColor: '#EBEBEB', borderRadius:20}}>
                      {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
          onDismiss={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          onTouchOutside={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          visible={this.state.slideAnimationDialog}
          // dialogTitle={<DialogTitle title="" />}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
          <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                 
                 source={this.state.propicture}
                 />
          </DialogContent>
        </Dialog> */}
         <Modal 
    visible={this.state.slideAnimationDialog}
    onTouchOutside={() => {
      this.setState({ slideAnimationDialog: false });
    }}
  >
    <ModalContent>
     
    <Image 
    style={{ width: 200, height: 200,marginTop:3}}
                 
                 source={this.state.propicture}
                 />
    </ModalContent>

  </Modal>
        
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        style={{ marginBottom: 30}}                
                        data={this.props.abc}
                        renderItem={this.renderList}
                        keyExtractor={(item,index)=>index.toString()}
                    />

                </View>

        );
    }
}
    