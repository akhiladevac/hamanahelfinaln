import React, { Component } from 'react';
import { View, Text , Image,ScrollView, ActivityIndicator, TouchableOpacity,NetInfo,Alert} from 'react-native';
import History from '../finance/History';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/FontAwesome';


const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
export default class Amount extends Component{

    constructor() {
        super();

        this.state = {
           
            loadingStatus: true,
            baseUrl: '',
            userRole:'',
            personid:'',
            response:[],
            connection_Status : '',
            settimeout:false,


    
        }
    }
    async componentDidMount() {
      this.checkNetwork();
      this.getBaseUrl();

     
  }
  checkNetwork=() => {
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this._handleConnectivityChange

  );
 
  NetInfo.isConnected.fetch().done((isConnected) => {

    if(isConnected == true)
    {
      this.setState({connection_Status : "Online"})
    }
    else
    {
      this.setState({connection_Status : "Offline"})



    }

  });

  }
  componentWillUnmount() {

    NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this._handleConnectivityChange
 
    );
 
  }
 
  _handleConnectivityChange = (isConnected) => {
 
    if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})

      }
      else
      {
        this.setState({connection_Status : "Offline"})

        


      }
  };
 

  //check user is already logged in
  async getBaseUrl() {
    
        try {
            let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
            this.setState({ baseUrl: baseUrl });
            var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
            var login_credentials = JSON.parse(loginCredentials);
            //this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
            var person_Id = login_credentials.person_id;

            //console.log(login_credentials.person_id);
            this.payment(baseUrl,person_Id);
            
        } catch (error) {
            console.error('Something went Wrong in Get Section' + error);
        }
    }
      
 
async getData() {
  try {
      var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
    
          //console.log(loginCredentials);
          var login_credentials = JSON.parse(loginCredentials);
          //console.log(login_credentials.role);
          this.setState({ personid: login_credentials.person_id});
      
  } catch (error) {
      console.error('Something went Wrong in Get Section' + error);
  }
}

payment(baseUrl,personid) {

  this.setState({ loadingStatus: true });

  var endpoint = 'payment.php';
  var url = baseUrl + endpoint;
  var data = new FormData();
  data.append('hashcode', '##church00@');
  data.append('Person_Id', personid);
  console.log(data);
  fetch(url, {
  method: 'POST',
  headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      'Accept-Encoding':  'gzip, deflate',
  },
  body: data,
  })
  .then((response) => response.json())
  .then((responseJson) => {

    console.log('amount');
      console.log(responseJson);
      if (responseJson.status === 200) {
        this.setState({ response: responseJson.response[0], loadingStatus: false });
      }
     
    
      else {
      const error = 'Newtork error';
   
      }
  })
  .catch((error) => {
      console.error(error);
  });
  setTimeout(() => {
    this.setState({
        loadingStatus:false,
        settimeout:true
    })
    },45000)
}


    

        render() {
            const { loadingStatus, response } = this.state;
            if(loadingStatus) {
              return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems:'center'}}>
                   <ActivityIndicator size='large'/>
                </View>
               
              );
             }else if(loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline')
             {

              return (
                  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                      <Text style={{fontSize:18}}>You are offline</Text>
                      <View style={{alignItems:'center', flexDirection: 'row', alignItems: 'center',justifyContent:'center'}}>
                      <TouchableOpacity onPress={() => this.payment(this.state.baseUrl,this.state.personid)}>
                          <Icon style={{ marginRight:10,marginTop:10}} name="undo" size={17} color="#7BCFE8" />
                          </TouchableOpacity>
                      <Text  style={{fontSize:18,marginTop:10}}>Retry</Text>
                      </View>
      
                  </View>
              );
          } else if(this.state.settimeout == true && this.state.connection_Status=='Online')   
          {

            return(
             <ScrollView showsVerticalScrollIndicator={false}>
             <View style= {{ backgroundColor:'#7BCFE8',height: 330, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
             <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#7BCFE8', height: 80, flexDirection: 'row' }}>
                <Text style={{ fontSize: 25, fontWeight: 'bold',marginLeft:10 }}>Payment</Text>

                </View>
           
              <View>
                    <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8',height: 380, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                         <Text style= {{ fontSize: 40, fontWeight: 'bold'}}>{this.state.response.due} ₹</Text>     
                    </View>                
              </View>
             
              <View style={{marginTop:-390,marginLeft:20}}>
              
                            <Text style= {{ fontSize: 17, fontWeight: '300'}}>Due amount</Text>
                        </View>
              </View>
                    
                    <History abc={response.Payment_History} />
              </ScrollView>

            );

          }else{
            return(
              <ScrollView showsVerticalScrollIndicator={false}>
              <View style= {{ backgroundColor:'#7BCFE8',height: 380, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}} >
              <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#7BCFE8', height: 80, flexDirection: 'row' }}>
                <Text style={{ fontSize: 22, fontWeight: 'bold',marginLeft:10 }}>Payment</Text>

                </View>
               <View>
                     <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor:'#7BCFE8',height: 350, borderRadius: 20, borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
                          <Text style= {{ fontSize: 40, fontWeight: 'bold'}}>{this.state.response.due} ₹</Text>     
                     </View>                
               </View>
               <View style={{marginTop:-350,marginLeft:20}}>
                             <Text style= {{ fontSize: 17, fontWeight: '300'}}>Due amount</Text>
                         </View>
               </View>
                     
                     <History abc={response.Payment_History} />
               </ScrollView>
 
             );
          }
        }
    }
  