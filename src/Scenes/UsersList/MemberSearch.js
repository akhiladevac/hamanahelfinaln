
/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity,Alert, Image,Linking,ActivityIndicator,NetInfo, TextInput,BackHandler,Picker,location } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
const DATA_LIST_MEMBERS = '@members_list';
const DATA_LIST = '@memlist_list';


import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import Modal, { ModalContent } from 'react-native-modals';

export default class MemberSearch extends Component {

    constructor() {
        super();

        this.state = {
          data1:[],
            loading: false,      
             data: [],
             data2:[],
             response:[],
            temp: [],  
            baseUrl: '',    
            error: null,  
            abd: []  ,
            familycount:'',
            someVal:'',
            visiblelist:false,
             PickerSelectedVal:'',
             connection_Status : '',
             propicture:'',
             slideAnimationDialog: false,

             throttlemode:'',
             choosenLabel: '', choosenindex: '',
             category:'',
             currentLabel: '',
             response3: [
              { 'bloodgroup': 'A+'},
              { 'bloodgroup': 'B+'},
              { 'bloodgroup': 'O+'},
             
             ],
             currency:'',
             response3:[
               {bloodgroup:'A+'},
               {bloodgroup:'A-'},
               {bloodgroup:'B+'},
               {bloodgroup:'B-'},
               {bloodgroup:'O+'},
               {bloodgroup:'O-'},
               {bloodgroup:'AB+'},
               {bloodgroup:'AB-'},

            
            ],
            visibleEmail: false,
            visibleOtp: false,
            visibleNewPassword: false,
  
            passwordRe: '',
            passwordReCon: '',
  
           
          };
        
        this.arrayholder = [];
    }

    static onEnterSomeView = () => {
      Actions.refresh();
    }
   
    componentDidUpdate(prevProps, prevState) {
      if(prevProps.visibleEmail== false){
       
        this.setState({visibleEmail: true});
      }
    }
    
    componentWillReceiveProps(nextProps){
      if(nextProps.visibleEmail!==true){
       
        this.setState({visibleEmail: true });
      }
    }
    componentWillMount(){
      this.getMemList();
      this.getUpdatedUsers();
    }
    componentDidMount() {
     

      // location.reload();      // this.setState({visibleEmail:true});
this.checkNetwork();
        // this.getBaseUrl();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     


    }
    async getMemList() {
      // this.setState({ loading: true });
      try {
          const memList = await AsyncStorage.getItem(DATA_LIST);
          if (memList !== null) {
            const abc = JSON.parse(memList);
          this.setState({          
              data1: abc,  
              temp: abc,        
              error: null,          
              // loading: false,
              query: '',  
              familycount:''     
            });  
            this.arrayholder = abc; 
            
          } 
          // alert(JSON.stringify(this.state.data))

      } catch (error) {
          console.log('Something went Wrong Saving list');
      }
    }

    getUpdatedUsers(){
      this.getBaseUrl();
    }

    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
          var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
          var login_credentials = JSON.parse(loginCredentials);
          //this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
          var fam_id = login_credentials.family_id;
        
        
          this.UdtUsersList(baseUrl);
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
  }

    UdtUsersList(baseUrl){
      var endpoint = 'member_list.php';
       var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
     
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
      .then(response => response.json())
      .then((responseJson)=> {

        if (responseJson.status === 200) {
          this.setState({
            loading: false,
            abd: responseJson.response,
             membercount:responseJson.Total_members
           })
           
           this.chechVariations();
        }
        
      })
      .catch(error=>console.log(error)) //to catch the errors if any
      }

      async chechVariations() {
        try {
            const memList = await AsyncStorage.getItem(DATA_LIST);
            if (memList !== null) {
              const abc = JSON.parse(memList);
            
              this.setState({          
                  data1: abc,  
                  temp: abc,        
                  error: null,          
                  // loading: false,
                  query: '',        
                });  
                this.arrayholder = abc; 
              } 

              if(JSON.stringify(this.state.abd) == JSON.stringify(this.state.data1))
              { }else{
                this.updateAsyncValues(this.state.abd,DATA_LIST);
              }
          
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    async updateAsyncValues(response, KEY) {
      await AsyncStorage.setItem(KEY, JSON.stringify(response));
      this.getMemList();
    }
    
    
    
    

    componentWillUnmount() {
      this.backHandler.remove();
    }
    checkNetwork=() => {
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this._handleConnectivityChange
  
    );
   
    NetInfo.isConnected.fetch().done((isConnected) => {
  
      if(isConnected == true)
      {
        this.setState({connection_Status : "Online"})
      }
      else
      {
        this.setState({connection_Status : "Offline"})
  
  
  
      }
  
    });
  
    }
    componentWillUnmount() {
  
      NetInfo.isConnected.removeEventListener(
          'connectionChange',
          this._handleConnectivityChange
   
      );
   
    }
   
    _handleConnectivityChange = (isConnected) => {
   
      if(isConnected == true)
        {
          this.setState({connection_Status : "Online"})
  
        }
        else
        {
          this.setState({connection_Status : "Offline"})
  
          
  
  
        }
    };
   
  
  
    handleBackPress = () => {
     
      if (Actions.currentScene !== '_MemberSearch') {
          
      } else {
        if(this.state.slideAnimationDialog==true){
          this.setState({ slideAnimationDialog: false });

        }else{
         
          this.setState({ visibleEmail: false })

        Actions.pop();
        }
        return true;
      }
    }
   async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
           this.setState({visibleEmail:true});

            // this.bloodListMembers(baseUrl,this.state.throttlemode);
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
  }

    bloodListMembers(baseUrl,r){
      this.setState({
        abd:[],
        throttlemode:r
      })
      // alert(r)
      var endpoint = 'blood_group_list.php';
       var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('blood_group',r);
     
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
      .then(response => response.json())
      .then((responseJson)=> {

        if (responseJson.status === 200) {
          this.setState({
            loading: false,
            abd: responseJson.response,
        
             })
          console.log("jhj"+JSON.stringify(responseJson) )
       
        }else{
          Alert.alert('No Data Found')

        }
        
      })
      .catch(error=>console.log(error)) //to catch the errors if any
      }

    

      searchFunction(query) {
       
       
        
        if (query.length === 0) {
            this.setState({ data1: this.state.temp, query: query });
        } else {
            this.setState({ query: query, });

             this.searchFilterFunction(this.state.query);
        }
        
    }

    searchFilterFunction = text => {   
      this.setState({
        visibleEmail:false
      }) 
        const newData = this.arrayholder.filter(item => {      
          const itemData = `${item.Person_Blood.toUpperCase()}  
                          `; 
          
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
        
         this.setState({ data2: newData });
        //  alert(JSON.stringify)
        //  this.setState({data1:newData}) 
      };

      bloodListMembers1(baseUrl,r){
        this.setState({loading:true})
        this.setState({
          abd:[],
          PickerSelectedVal:r,
          visibleEmail:false,
          
        })
        // alert(r)
        var endpoint = 'blood_group_list.php';
         var url = baseUrl + endpoint;
          // console.log(url);
          var data = new FormData();
          data.append('hashcode', '##church00@');
          data.append('blood_group',r);
       
          fetch(url, {
          method: 'POST',
          headers: {
              Accept: 'application/json',
              'Content-Type': 'multipart/form-data',
          },
          body: data,
          })
        .then(response => response.json())
        .then((responseJson)=> {
  
          if (responseJson.status === 200) {
            this.setState({
              loading: false,
              abd: responseJson.response,
          
               })
            console.log("jhj"+JSON.stringify(responseJson) )
         
          }else{
            this.setState({
              loading: false,
             
          
               })
  
          }
          
        })
        .catch((error) => {
          console.error(error);
        });
        setTimeout(() => {
            this.setState({
                loading:false,
                settimeout:true
            })
            },45000)
          }
  
        renderProperties1 = ({ item }) => {
          
          if (item.photo) {
            var profPic =  { uri: this.state.baseUrl + item.photo };
        } else {
                var profPic = require('../../img/avatar.png');
            }
            if (item.phone && item.phone !== '0') {
              var phone = item.phone;
          } else {
              var phone = '';
          }
          return (
            <TouchableOpacity onPress={() => this.searchFilterFunction(item.bloodgroup)}>
                
              <View style={{ backgroundColor: '#fff', height: 40, marginTop: 5, marginLeft:5,marginRight:5 }}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  
                    <View style={{  width: '100%',alignItems: 'center',justifyContent:'center'}} >
                  
                  <Text style={{ fontSize: 17, fontWeight: '500'}}>{item.bloodgroup}</Text>
                  {/* <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Name}</Text>
                  <View style={{flexDirection:'row'}}>
                  <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>Family Owner :</Text>
  
                  <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Owner}</Text> */}
                  {/* </View> */}
          
          </View>
     
                </View>
              </View>
            </TouchableOpacity>
          );
        };
        showPicture(profPic){
          this.setState({ slideAnimationDialog: true,propicture:profPic})
      
        }
      renderProperties = ({ item }) => {
          
        if (item.photo) {
          var profPic =  { uri: this.state.baseUrl + item.photo };
      } else {
              var profPic = require('../../img/avatar.png');
          }
          if (item.phone && item.phone !== '0') {
            var phone = item.phone;
        } else {
            var phone = '';
        }
        return (
          <TouchableOpacity >
            <View style={{ backgroundColor: '#fff', height: 85, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                  
                 <TouchableOpacity  onPress={()=> this.showPicture(profPic)}
                 style={{ width: 50, height: 50, marginLeft: 10, borderRadius: 20 }}>
                  <Image style={{ width: 50, height: 50, borderRadius: 20 }}
                 
                  source={profPic}
                  />
                  </TouchableOpacity>
                  <View style={{ marginLeft: 10, width: '50%'}} >
                
                <Text style={{ fontSize: 17, fontWeight: '500' }}>{item.Person_Name}</Text>
                <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Name}</Text>
                <View style={{flexDirection:'row'}}>
                <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>Family Owner :</Text>

                <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Owner}</Text>
                </View>
        
        </View>
   <View style={{flexDirection:'row',marginLeft:60}}>

        {this.renderCall(phone)}
        </View>
                
              </View>
            </View>
          </TouchableOpacity>
        );
      };
      renderCall(phone) {
        if (phone) {
            return (
                <TouchableOpacity onPress={() => this.callFuntion(phone)}>
                    <Icon style={{marginLeft:8}}   name="phone" size={20} color="#000" />
                </TouchableOpacity>
            )
        }
    }
    callFuntion(number) {
  

      if (number !== null && number.length >= 8) {
        Linking.openURL(`tel:${number}`);
      } else {
        Alert.alert('Number is Empty');
      }
  }
  renderPhoneCheck() {
    return (
        <View>
        <Dialog
            visible={this.state.visibleEmail}
            dialogTitle={<DialogTitle title=" Select a Blood Group" />}
            footer={
                <DialogFooter>
                  
                    <DialogButton 
                     text="Close"
                    onPress={() => { this.phoneSubmitBtn() }}
                    />
                </DialogFooter>
                }
            >
            <DialogContent>

                <View style={{ width: 250, alignItems: 'center' }}>

                    <View style={{ marginTop: 20, width: 250, height: 300, backgroundColor: '#cfcdcc', borderRadius: 5,flexDirection:'row' }}>
                    <FlatList   
        style={{ marginTop: 2 }}       
        contentContainerStyle={{ paddingBottom: 5}}     

        data={this.state.response3}          
        renderItem={this.renderProperties1}          
        keyExtractor={(item, index) => index.toString()}  
        ItemSeparatorComponent={this.renderSeparator}                             
    />    
     
                    {/* <Text style={{width:100,height:40,marginTop:10,marginLeft:20,fontWeight:'700'}}>Blood Group :</Text>

                        <Text style={{width:60,height:40,marginTop:10,marginLeft:10}}>{this.state.PickerSelectedVal}</Text>
                       
                        <Picker style={{width: '8%',height:40,marginBottom:20}}
    selectedValue={this.state.PickerSelectedVal}
    onValueChange={(itemValue, itemIndex) => 
      this.bloodListMembers1(this.state.baseUrl,itemValue)}>
    {/* onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} > */}
    {/* <Picker.Item label="Select a blood group" value="Select a blood group" />

    <Picker.Item label="A+" value="A+" />
    <Picker.Item label="A-" value="A-" />
    <Picker.Item label="B+" value="B+" />
    <Picker.Item label="B-" value="B-" />
     <Picker.Item label="O+" value="O+" />
     <Picker.Item label="O-" value="O-" />
     <Picker.Item label="AB+" value="AB+" />
     <Picker.Item label="AB-" value="AB-" />



   </Picker> */}
 
                    </View>
                </View>

            </DialogContent>
        </Dialog>
    </View>
    )
}
renderContents() {
  if (this.state.loading) {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',marginTop:200}}>
            <ActivityIndicator size='large' />
        </View>
    );
}else if(this.state.data2.length==0 ){
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
       <Text style={{marginTop:400}}>No Data Found</Text>
    </View>
  );



}   else if(this.state.loadingStatus==false && this.state.settimeout == true && this.state.connection_Status=='Offline' ){

  return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize:18}}>You are offline</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity onPress={() => this.setState({visibleEmail:true})}>
              <Icon style={{ marginTop:20}} name="redo" size={17} color="#fff" />
              </TouchableOpacity>
          <Text style={{fontSize:18,marginTop:20,marginLeft:10}}>Retry</Text>
          </View>

      </View>
  );
}

  
  

      return (
        <View>
           <Modal
    visible={this.state.slideAnimationDialog}
    onTouchOutside={() => {
      this.setState({ slideAnimationDialog: false });
    }}
  >
    <ModalContent>
    <Image style={{ width: 200, height: 200, marginTop:3}}
                 
                 source={this.state.propicture}
                 />
    </ModalContent>
  </Modal>
        {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
        onDismiss={() => {
          this.setState({ slideAnimationDialog: false });
        }}
        onTouchOutside={() => {
          this.setState({ slideAnimationDialog: false });
        }}
        visible={this.state.slideAnimationDialog}
        // dialogTitle={<DialogTitle title="" />}
        dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
        <DialogContent>
        <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
               
               source={this.state.propicture}
               />
        </DialogContent>
      </Dialog> */}
        <FlatList   
        style={{ marginTop: 2 }}   
        contentContainerStyle={{ paddingBottom: 25}}     
    
        data={this.state.data2}          
        renderItem={this.renderProperties}          
        keyExtractor={(item, index) => index.toString()}  
        ItemSeparatorComponent={this.renderSeparator}                             
    /> 
    </View>   
      );
  
}
PhoneCheckForPassword(baseUrl){
  this.setState({
    abd:[],
    // visibleEmail: false

  })
    // alert(this.state.PickerSelectedVal)
  var endpoint = 'blood_group_list.php';
   var url = baseUrl + endpoint;
    // console.log(url);
    var data = new FormData();
    data.append('hashcode', '##church00@');
    data.append('blood_group',this.state.PickerSelectedVal);
 
    fetch(url, {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
    },
    body: data,
    })
  .then(response => response.json())
  .then((responseJson)=> {

    if (responseJson.status === 200) {
      this.setState({
        loading: false,
        abd: responseJson.response,
    
         })
      console.log("JsonParsing"+JSON.stringify(responseJson) )
   
    }else{
      Alert.alert('No Data Found')

    }
    
  })
  .catch(error=>console.log(error)) //to catch the errors if any
  


}

phoneSubmitBtn() {
  this.setState({ visibleEmail: false })

  Actions.Home();
//   if(this.state.PickerSelectedVal){
//     this.PhoneCheckForPassword(this.state.baseUrl)

//   }else{
// Alert.alert('Please select a blood group')
//   }
  
 
}
    render() {
        return (
          <View style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            
          <View style={{ backgroundColor: '#7BCFE8'}}>

          {this.renderPhoneCheck()}

          <View style={{marginTop:25,marginLeft:20}}>
                <Text style={{fontSize:22,fontWeight:'700',marginLeft:10}}>Blood Group Search</Text>

                </View> 
           

<View style={{justifyContent:'center',flexDirection:'row', alignItems:'center'}}>


           
            </View>
            
          <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#fff', height: 40, margin: 20, marginTop: 20, borderRadius: 50, flexDirection: 'row' }}>
          {/* <Picker style={{width: '90%',height:50,marginBottom:20,marginTop:20}}
            selectedValue={this.state.category}
 
            onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue})} >
 
            { this.state.response3.map((item, key)=>(
            <Picker.Item label={item.bloodgroup} value={item.bloodgroup} key={key} />)
            )}
    
          </Picker> */}
           {/* <TextInput  
                       
                       placeholder='Search'
                       style={{ color: '#000', paddingLeft: 20 }}
                       onChangeText={text => this.searchFunction(text)}
                       selectTextOnFocus={true}
                     //  onFocus={this.readFromClipboard()}
                     //  autoFocus={true}
                       // autoCapitalize={false}
                       // autoCorrect={false}
                   /> */}
                   
            <Text style={{width:'80%',marginLeft:10}} onPress={() => this.setState({visibleEmail:true})}>Select a Blood Group</Text>  
          {/* <Picker style={{width: '80%',height:50}}
    selectedValue={this.state.throttlemode}
    onValueChange={(someVal, throttlemodeIndex) => 
    this.bloodListMembers(this.state.baseUrl,someVal)}>
    <Picker.Item label="Select a blood group" value="Select a blood group" />

    <Picker.Item label="A+" value="A+" />
    <Picker.Item label="A-" value="A-" />
    <Picker.Item label="B+" value="B+" />
    <Picker.Item label="B-" value="B-" />
     <Picker.Item label="O+" value="O+" />
     <Picker.Item label="O-" value="O-" />
     <Picker.Item label="AB+" value="AB+" />
     <Picker.Item label="AB-" value="AB-" />



   </Picker> */}

            
            <Icon  style={{ color: '#000'}} name={'search'} size={18} />

          </View>

          </View>
          
                      {this.renderContents()}
        
      </View>
  );
}
}
// /* eslint-disable prettier/prettier */
// /* eslint-disable no-trailing-spaces */
// import React, { Component } from 'react';
// import Dialog, {
//   DialogTitle,
//   DialogContent,
//   DialogFooter,
//   DialogButton,
//   SlideAnimation,
//   ScaleAnimation,
// } from 'react-native-popup-dialog';
// import Modal, { ModalContent } from 'react-native-modals';

// // import Dialog, { DialogTitle, DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
// // import { Dialog } from 'react-native-simple-dialogs';
// import { View, Text, FlatList, TouchableOpacity, Image, TextInput,BackHandler ,Picker, Linking ,Clipboard,ScrollView} from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import AsyncStorage from '@react-native-community/async-storage';
// import { Actions } from 'react-native-router-flux';
// const DATA_BASE_URL = '@base_url';
// const LOGIN_CREDENTIALS = '@login_credentials';
// const DATA_LIST_MEMBERS = '@members_list';
// const extractKey = ({members}) => members
// let renderData1 ='';
// let s1=[];
// export default class MemberSearch extends Component {

//     constructor() {
//         super();

//         this.state = {
//           PickerValueHolder : '',
//           visibleEmail:false,

//           response7:[
//             {bloodgroup:'A+'},
//             {bloodgroup:'A-'},
//             {bloodgroup:'B+'},
//             {bloodgroup:'B-'},
//             {bloodgroup:'O+'},
//             {bloodgroup:'O-'},
//             {bloodgroup:'AB+'},
//             {bloodgroup:'AB-'},

         
//          ],
//           propicture:'',
//           alertvisibility:false,
//           slideAnimationDialog: false,
//             loading: true,      
//             data: [],
//             dataMembers:[],
//             temp: [],  
//             baseUrl: '',    
//             error: null,  
//             abd:[]  ,
//             membercount:'0',
//             familycount:'',
//             visible:false,
//             index:'ANU MARY MATHEW',
//             username:'',
//             email_id:'',
//             clipboardContent1: '',
//             visibletext:false,
//             visibleOtp:false

//           };

//         this.arrayholder = [];
//     }

//     componentDidMount() {
//       this.getMembersList();
     
     
//       this.getUpdatedUsers();
//       this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     


//     }
//     renderProperties7 = ({ item }) => {
//           // alert(item)
//     //   if (item.photo) {
//     //     var profPic =  { uri: this.state.baseUrl + item.photo };
//     // } else {
//     //         var profPic = require('../../img/avatar.png');
//     //     }
//     //     if (item.phone && item.phone !== '0') {
//     //       var phone = item.phone;
//     //   } else {
//     //       var phone = '';
//     //   }
//       return (
//         <TouchableOpacity onPress={()=>{
//           this.searchFunction(item.bloodgroup)
//         }}>
            
//           <View style={{ backgroundColor: '#fff', height: 40, marginTop: 5, marginLeft:5,marginRight:5 }}>
//             <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
              
//                 <View style={{  width: '100%',alignItems: 'center',justifyContent:'center'}} >
              
//               <Text style={{ fontSize: 17, fontWeight: '500'}}>{item.bloodgroup}</Text>
//               {/* <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Name}</Text>
//               <View style={{flexDirection:'row'}}>
//               <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>Family Owner :</Text>

//               <Text style={{ fontSize: 10, paddingLeft: 2, paddingTop: 5 }}>{item.Fam_Owner}</Text> */}
//               {/* </View> */}
      
//       </View>
 
//             </View>
//           </View>
//         </TouchableOpacity>
//       );
//     };
//     componentWillUnmount() {
//       this.backHandler.remove();
//      }
//      handleBackPress = () => {
     
//       if (Actions.currentScene !== '_UserList') {
          
//       } else {
//         if(this.state.slideAnimationDialog==true){
//         this.setState({ slideAnimationDialog: false });
//         }else{

//         Actions.pop();
//         }
//         return true;
//       }
//     }
 

//     async getMembersList() {
     
//       this.setState({ loading: true });
//       try {
//           const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
//           if (membersList !== null) {
//             const abc = JSON.parse(membersList);
//           this.setState({          
//               data: abc,  
//               temp: abc,   
     
//               error: null,          
//               loading: false,
//               query: '',  
//               familycount:''     
//             });  
//             this.arrayholder = abc; 
            
//           } 
         
//           // alert(JSON.stringify(this.state.dataMembers) )
//       } catch (error) {
//           console.log('Something went Wrong Saving list');
//       }
//     }

//     getUpdatedUsers(){
//       this.getBaseUrl();
//     }

//     async getBaseUrl() {
    
//       try {
//           let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
//           this.setState({ baseUrl: baseUrl });
//           var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
//           var login_credentials = JSON.parse(loginCredentials);
//           //this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
//           var fam_id = login_credentials.family_id;
        
        
//           this.UdtUsersList(baseUrl,fam_id);
          
//       } catch (error) {
//           console.error('Something went Wrong in Get Section' + error);
//       }
//   }

//     UdtUsersList(baseUrl,fam_id){
//       var endpoint = 'list_of_members.php';
//        var url = baseUrl + endpoint;
//         // console.log(url);
//         var data = new FormData();
//         data.append('hashcode', '##church00@');
//         data.append('Fam_Id',fam_id)
     
//         fetch(url, {
//         method: 'POST',
//         headers: {
//             Accept: 'application/json',
//             'Content-Type': 'multipart/form-data',
//         },
//         body: data,
//         })
//       .then(response => response.json())
//       .then((responseJson)=> {

//         if (responseJson.status === 200) {
//           this.setState({
//             loading: false,
//             abd: responseJson.response,
//              membercount:responseJson.Total_members
//            })
           
//            this.chechVariations();
//         }
        
//       })
//       .catch(error=>console.log(error)) //to catch the errors if any
//       }

//       async chechVariations() {
//         try {
//             const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
//             if (membersList !== null) {
//               const abc = JSON.parse(membersList);
            
//               this.setState({          
//                   data: abc,  
//                   temp: abc,        
//                   error: null,          
//                   loading: false,
//                   query: '',        
//                 });  
//                 this.arrayholder = abc; 
//               } 

//               if(JSON.stringify(this.state.abd) == JSON.stringify(this.state.data))
//               { }else{
//                 this.updateAsyncValues(this.state.abd,DATA_LIST_MEMBERS);
//               }
          
//         } catch (error) {
//             console.log('Something went Wrong Saving list');
//         }
//     }

//     async updateAsyncValues(response, KEY) {
//       await AsyncStorage.setItem(KEY, JSON.stringify(response));
//       this.getMembersList();
//     }
    
    
    
    

//     searchFunction(query) {
//       this.setState({
        
//         visibleEmail:false,
        
//       })
//       alert(query)
//         if (query.length === 0) {
//             this.setState({ data: this.state.temp, query: query });
//         } else {
//             this.setState({ query: query, });
//             this.searchFilterFunction(this.state.query);
//         }
        
//     }

//     searchFilterFunction = text => {   
        
//     //   const newData = this.arrayholder.filter(s1 => {      
//     //     const itemData = `${s1.Person_Name.toUpperCase()}   
//     //                      `; 
        
//     //      const textData = text.toUpperCase();
          
//     //      return itemData.indexOf(textData) > -1;    
//     //   });
      
//     //   this.setState({ parentData: newData });  
//     // };
//     const newData = this.arrayholder
//     .filter(item => {  
//       renderData1 = item.members.map(members =>{
       
//         return ` ${members.Person_Blood}`
     
// }).join("\n")
//       const itemData = `
//                         ${renderData1.toUpperCase()}

                       
                        
//                       `; 
//        const textData = text.toUpperCase();
        
//        return itemData.indexOf(textData) > -1;    
//     });
  
 
//     this.setState({ data: newData });  
//   };
    
    
//   showdata(){
//     if(this.state.visible==false){
      
//     }else{
//       return(
      
//         <Text style={{}}>{ renderData1 } </Text>
  
//       )
//     }
    
//   }
//   // show(){
//   //   this.setState({
//   //     visibletext:true
//   //   })
//   //   if(this.state.visibletext ==true){
//   //     this.readFromClipboard();
//   //   }
//   // }
//   showPicture(profPic){
//     this.setState({ slideAnimationDialog: true,propicture:profPic})

//   }
  
//     setvalue(text){
      
//       this.setState({clipboardContent : text } )     
//         this.searchFunction(text)
//     }
//       renderProperties = ({ item }) => {
//         let parentData = item;
//         if (item.photo) {
//           var profPic =  { uri: this.state.baseUrl + item.photo };
//           propi=profPic;
         
//       } else {
//               var profPic = require('../../img/avatar.png');
//               // propi=profPic;

//           }
//           if (item.phone && item.phone !== '0') {
//             var phone = item.phone;
//         } else {
//             var phone = '';
//         }
           

//   // console.log("renderdataphoto"+renderData2);
 
//         return (
//           <TouchableOpacity >
            
//             <View style={{ backgroundColor: '#fff', height: 90, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>

//               <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center',width:300 }}>
            
//                  <TouchableOpacity  onPress={()=> this.showPicture(profPic)}
//                  style={{ width: 50, height: 50, marginLeft: 10, borderRadius: 20 }}>
//                   <Image style={{ width: 50, height: 50, borderRadius: 20 }}
                 
//                   source={profPic}
//                   />
//                   </TouchableOpacity>
//                   <View>
//                       <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.Person_Name }</Text>
//                       <Text  numberOfLines={1} style={{paddingLeft: 15, paddingTop: 8 ,width:250 }}>{ item.Fam_Name } House</Text>
//                       <View style={{flexDirection:'row'}}>
//                       <Text numberOfLines={1} style={{ paddingLeft: 15, paddingTop: 8 }}>Blood Group: </Text>

//                       <Text numberOfLines={1} style={{ paddingLeft: 5, paddingTop: 8 }}>{ item.Person_Blood }</Text>
//                       </View>
//                         {this.showdata()}
                       
//                         </View>
                      
//                         <View style={{justifyContent:'flex-end'}}>

// {this.renderCall(phone)}
// </View>
                
//               </View>
//             </View>
//           </TouchableOpacity>
//         );
//       };
//       renderCall(phone) {
//         if (phone) {
//             return (
//                 <TouchableOpacity onPress={() => this.callFuntion(phone)}>
//                     <Icon style={{}}   name="phone" size={20} color="#000" />
//                 </TouchableOpacity>
//             )
//         }
//     }
//     callFuntion(number) {
  

//       if (number !== null && number.length >= 8) {
//         Linking.openURL(`tel:${number}`);
//       } else {
//         Alert.alert('Number is Empty');
//       }
//   }
//       renderProperties1 = ({ item }) => {
// // let s2=[];
// // for(let i=0;i<item.Fam_Id;i++){

// //   console.log("renderdataphoto"+JSON.stringify(item.members[i]));
// //    s1=s1.concat(item.members[i]);
   

// // }
// // console.log("array"+JSON.stringify(s1));

 
//         return (
//              <View>
//                 <ScrollView>
//                 <FlatList
//                     data={item.members}
//                     contentContainerStyle={{ paddingBottom: 160}}     

//                     renderItem={this.renderProperties}          

//                     // renderItem={({item}) => {
//                     //     console.log('data', parentData, item);
//                     //     return(
//                     //         <View>
//                     //             <Text>{item.Person_Name}</Text>
//                     //         </View>
//                     //     );
//                     // }
//                   // }
                  
//                 />     
//             </ScrollView>
//           </View>
//         );
//       };
//       phoneSubmitBtn() {
//         this.setState({ visibleEmail: false })
      
//         //  Actions.Home();
//       //   if(this.state.PickerSelectedVal){
//       //     this.PhoneCheckForPassword(this.state.baseUrl)
      
//       //   }else{
//       // Alert.alert('Please select a blood group')
//       //   }
        
       
//       }
//       renderPhoneCheck() {
//         return (
//             <View>
//             <Dialog
//                 visible={this.state.visibleEmail}
//                 dialogTitle={<DialogTitle title=" Select a Blood Group" />}
//                 footer={
//                     <DialogFooter>
                      
//                         <DialogButton 
//                          text="Close"
//                         onPress={() => { this.phoneSubmitBtn() }}
//                         />
//                     </DialogFooter>
//                     }
//                 >
//                 <DialogContent>
    
//                     <View style={{ width: 250, alignItems: 'center' }}>
    
//                         <View style={{ marginTop: 20, width: 250, height: 300, backgroundColor: '#cfcdcc', borderRadius: 5,flexDirection:'row' }}>
//                         <FlatList   
//             style={{ marginTop: 2 }}       
//             contentContainerStyle={{ paddingBottom: 5}}     
    
//             data={this.state.response7}          
//             renderItem={this.renderProperties7}          
//             keyExtractor={(item, index) => index.toString()}  
//             ItemSeparatorComponent={this.renderSeparator}                             
//         />    
         
//                         {/* <Text style={{width:100,height:40,marginTop:10,marginLeft:20,fontWeight:'700'}}>Blood Group :</Text>
    
//                             <Text style={{width:60,height:40,marginTop:10,marginLeft:10}}>{this.state.PickerSelectedVal}</Text>
                           
//                             <Picker style={{width: '8%',height:40,marginBottom:20}}
//         selectedValue={this.state.PickerSelectedVal}
//         onValueChange={(itemValue, itemIndex) => 
//           this.bloodListMembers1(this.state.baseUrl,itemValue)}>
//         {/* onValueChange={(itemValue, itemIndex) => this.setState({PickerSelectedVal: itemValue})} > */}
//         {/* <Picker.Item label="Select a blood group" value="Select a blood group" />
    
//         <Picker.Item label="A+" value="A+" />
//         <Picker.Item label="A-" value="A-" />
//         <Picker.Item label="B+" value="B+" />
//         <Picker.Item label="B-" value="B-" />
//          <Picker.Item label="O+" value="O+" />
//          <Picker.Item label="O-" value="O-" />
//          <Picker.Item label="AB+" value="AB+" />
//          <Picker.Item label="AB-" value="AB-" />
    
    
    
//        </Picker> */}
     
//                         </View>
//                     </View>
    
//                 </DialogContent>
//             </Dialog>
//         </View>
//         )
//     }
//     render() {
    
//       const keys = Object.keys(this.state.abd);
//         return (
//             <View   style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
//             {/* {this.renderResetPassword()}  */}
//             {this.renderPhoneCheck()}
//                 <View style={{ backgroundColor: '#7BCFE8'}}>
//                 <View style={{marginTop:25,marginLeft:30}}>
//                 <Text style={{fontSize:22,fontWeight:'700'}}>Blood Group Based Search</Text>

//                 </View> 
 
                 
                
//                 <TouchableOpacity onPress={() => this.setState({visibleEmail:true})}
//                  style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 10, backgroundColor: '#fff', height: 40, margin: 20, marginTop: 20, borderRadius: 50, flexDirection: 'row' }}>
//                         {/* <TextInput  
                       
//                               placeholder='Search'
//                               style={{ color: '#000', paddingLeft: 20 }}
//                               onChangeText={text => this.searchFunction(text)}
//                               selectTextOnFocus={true}
//                             //  onFocus={this.readFromClipboard()}
//                             //  autoFocus={true}
//                               // autoCapitalize={false}
//                               // autoCorrect={false}
//                           /> */}
//                            <Text style={{width:'80%',marginLeft:10}} >Select a Blood Group</Text> 
//                           {/* <Picker style={{}}
//             selectedValue={this.state.PickerValueHolder}
 
//             onValueChange={(itemValue, itemIndex) => this.setState({PickerValueHolder: itemValue})} >
 
//             { this.state.response3.map((item, key)=>(
//             <Picker.Item label={item.bloodgroup} value={item.} key={key} />)
//             )}
    
//           </Picker> */}
//                         {/* <TextInput 
//                               placeholder='Search'
//                               style={{ color: '#000', paddingLeft: 20 }}
//                               onChangeText={text => this.searchFunction(text)}
//                               selectTextOnFocus={true}
//                               // autoCapitalize={false}
//                               // autoCorrect={false}
//                           />
//                         </View> */}
//                     </TouchableOpacity>
//                   {/* <View style={{  borderRadius: 70, paddingLeft:10,margin: 20, marginTop: 20,
//             backgroundColor: '#ffff', 
//             width: '90%', 
//             height: 40, 
//             alignItems: 'center' ,
//             flexDirection: 'row',  }}>
//                           <TextInput 
//                               placeholder='Search'
//                               style={{ color: '#000', paddingLeft: 20 }}
//                               onChangeText={text => this.searchFunction(text)}
//                               autoCapitalize={false}
//                               autoCorrect={false}
//                           />

//                       </View> */}
                 
//                 </View>
//                 <Modal
//     visible={this.state.slideAnimationDialog}
//     onTouchOutside={() => {
//       this.setState({ slideAnimationDialog: false });
//     }}
//   >
//     <ModalContent>
//     <Image style={{ width: 200, height: 200,marginTop:3}}
                 
//                  source={this.state.propicture}
//                  />
//     </ModalContent>
//   </Modal>
//                 {/* <Dialog
//           onDismiss={() => {
//             this.setState({ slideAnimationDialog: false });
//           }}
//           onTouchOutside={() => {
//             this.setState({ slideAnimationDialog: false });
//           }}
//           visible={this.state.slideAnimationDialog}
          
//           dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
//           <DialogContent>
//           <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                 
//                  source={this.state.propicture}
//                  />
//           </DialogContent>
//         </Dialog> */}
//                 {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
//           onDismiss={() => {
//             this.setState({ slideAnimationDialog: false });
//           }}
//           onTouchOutside={() => {
//             this.setState({ slideAnimationDialog: false });
//           }}
//           visible={this.state.slideAnimationDialog}
//           // dialogTitle={<DialogTitle title="" />}
//           dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
//           <DialogContent>
//           <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                 
//                  source={this.state.propicture}
//                  />
//           </DialogContent>
//         </Dialog> */}
               
//                 {/* <FlatList   
//                     style={{ marginTop: 2 }}  
//                      contentContainerStyle={{ paddingBottom: 25}}     
//                     data={this.state.data} 
//                      renderItem={this.renderProperties}          

//                     //  renderItem={({item, index}) => this.renderProperties(item, keys[index])}                           
//                     // keyExtractor={(item, index) => item.title}
//                     keyExtractor={(item, index) =>item.Fam_Id}  
//                     ItemSeparatorComponent={this.renderSeparator}  
//                 />  */}
//                 <ScrollView>
//                <FlatList
//                                     contentContainerStyle={{ paddingBottom: 160}}     

//     data={this.state.data}
//     renderItem={this.renderProperties1}          

   
                  
//                 /> 
//                 </ScrollView>     
//             </View>
//         );
    
  

           
//     }
// }