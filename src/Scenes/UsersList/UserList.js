/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
import React, { Component } from 'react';
import Dialog, {
  DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
} from 'react-native-popup-dialog';
import Modal, { ModalContent } from 'react-native-modals';

// import Dialog, { DialogTitle, DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
// import { Dialog } from 'react-native-simple-dialogs';
import { View, Text, FlatList, TouchableOpacity, Image, TextInput,BackHandler ,Clipboard} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
const DATA_BASE_URL = '@base_url';
const LOGIN_CREDENTIALS = '@login_credentials';
const DATA_LIST_MEMBERS = '@members_list';
const extractKey = ({members}) => members
let renderData1 ='';
let propi='';
export default class UserList extends Component {

    constructor() {
        super();

        this.state = {
          propicture:'',
          alertvisibility:false,
          slideAnimationDialog: false,
            loading: false,      
            data: [],
            temp: [],  
            baseUrl: '',    
            error: null,  
            abd:[]  ,
            membercount:'0',
            familycount:'',
            visible:false,
            index:'ANU MARY MATHEW',
            username:'',
            email_id:'',
            clipboardContent1: '',
            visibletext:false,
            visibleOtp:false

          };

        this.arrayholder = [];
    }

    componentDidMount() {
      this.getMembersList();
      this.getUpdatedUsers();
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);     


    }
    componentWillUnmount() {
      this.backHandler.remove();
     }
     handleBackPress = () => {
     
      if (Actions.currentScene !== '_UserList') {
          
      } else {
        if(this.state.slideAnimationDialog==true){
        this.setState({ slideAnimationDialog: false });
        }else{

        Actions.pop();
        }
        return true;
      }
    }
 

    async getMembersList() {
      this.setState({ loading: true });
      try {
          const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
          if (membersList !== null) {
            const abc = JSON.parse(membersList);
          this.setState({          
              data: abc,  
              temp: abc,        
              error: null,          
              loading: false,
              query: '',  
              familycount:''     
            });  
            this.arrayholder = abc; 
            
          } 
      } catch (error) {
          console.log('Something went Wrong Saving list');
      }
    }

    getUpdatedUsers(){
      this.getBaseUrl();
    }

    async getBaseUrl() {
      try {
          let baseUrl = await AsyncStorage.getItem(DATA_BASE_URL);
          this.setState({ baseUrl: baseUrl });
          var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
          var login_credentials = JSON.parse(loginCredentials);
          //this.setState({ baseUrl: baseUrl, fam_id: login_credentials.family_id})
          var fam_id = login_credentials.family_id;
        
        
          this.UdtUsersList(baseUrl,fam_id);
          
      } catch (error) {
          console.error('Something went Wrong in Get Section' + error);
      }
  }

    UdtUsersList(baseUrl,fam_id){
      var endpoint = 'list_of_members.php';
       var url = baseUrl + endpoint;
        // console.log(url);
        var data = new FormData();
        data.append('hashcode', '##church00@');
        data.append('Fam_Id',fam_id)
     
        fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
        body: data,
        })
      .then(response => response.json())
      .then((responseJson)=> {

        if (responseJson.status === 200) {
          this.setState({
            loading: false,
            abd: responseJson.response,
             membercount:responseJson.Total_members
           })
           
           this.chechVariations();
        }
        
      })
      .catch(error=>console.log(error)) //to catch the errors if any
      }

      async chechVariations() {
        try {
            const membersList = await AsyncStorage.getItem(DATA_LIST_MEMBERS);
            if (membersList !== null) {
              const abc = JSON.parse(membersList);
            
              this.setState({          
                  data: abc,  
                  temp: abc,        
                  error: null,          
                  loading: false,
                  query: '',        
                });  
                this.arrayholder = abc; 
              } 

              if(JSON.stringify(this.state.abd) == JSON.stringify(this.state.data))
              { }else{
                this.updateAsyncValues(this.state.abd,DATA_LIST_MEMBERS);
              }
          
        } catch (error) {
            console.log('Something went Wrong Saving list');
        }
    }

    async updateAsyncValues(response, KEY) {
      await AsyncStorage.setItem(KEY, JSON.stringify(response));
      this.getMembersList();
    }
    
    
    
    

    searchFunction(query) {
        
        if (query.length === 0) {
            this.setState({ data: this.state.temp, query: query });
        } else {
            this.setState({ query: query, });
            this.searchFilterFunction(this.state.query);
        }
        
    }

    searchFilterFunction = text => {   
        
     
        const newData = this.arrayholder
        .filter(item => {  
          renderData1 = item.members.map(members =>{
           
            return ` ${members.Person_Name}`
       
  }).join("\n")
          const itemData = `${item.Fam_Owner.toUpperCase()}   
                            ${item.Fam_Name.toUpperCase()}
                            ${renderData1.toUpperCase()}

                           
                            
                          `; 
           const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });
      
     
        this.setState({ data: newData });  
        console.log("uihihjiojonnkik"+JSON.stringify(newData))
      };
    
    
    
  showdata(){
    if(this.state.visible==false){
      
    }else{
      return(
      
        <Text style={{}}>{ renderData1 } </Text>
  
      )
    }
    
  }
  // show(){
  //   this.setState({
  //     visibletext:true
  //   })
  //   if(this.state.visibletext ==true){
  //     this.readFromClipboard();
  //   }
  // }
  showPicture(profPic){
    this.setState({ slideAnimationDialog: true,propicture:profPic})

  }
  
    setvalue(text){
      
      this.setState({clipboardContent : text } )     
        this.searchFunction(text)
    }
      renderProperties = ({ item }) => {
        if (item.members[0].photo) {
          var profPic =  { uri: this.state.baseUrl + item.members[0].photo };
          propi=profPic;
         
      } else {
              var profPic = require('../../img/avatar.png');
              // propi=profPic;

          }
           renderData1 = item.members.map(members =>{
           
            return ` ${members.Person_Name}`
         
  }).join("\n")
//   const renderData2 = item.members.map(members =>{
//     // console.log("name"+members.Person_Name)
//     // console.log("photo"+members.photo)

           
//     return ` ${members.Person_Name}`
 
// }).join("\n")
  
  // console.log("renderdataphoto"+renderData1);
 
        return (
          <TouchableOpacity onPress={() => Actions.DetailedUser({ fam_details: item, baseUrl: this.state.baseUrl })}>
            
            <View style={{ backgroundColor: '#fff', height: 70, marginTop: 10, marginLeft: 10, marginRight: 10, borderRadius: 10 }}>

              <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            
                 <TouchableOpacity  onPress={()=> this.showPicture(profPic)}
                 style={{ width: 50, height: 50, marginLeft: 10, borderRadius: 20 }}>
                  <Image style={{ width: 50, height: 50, borderRadius: 20 }}
                 
                  source={profPic}
                  />
                  </TouchableOpacity>
                  <View>
                      <Text style={{ paddingLeft: 15, fontSize: 18, fontWeight: '500' }}>{ item.Fam_Owner }</Text>
                      <Text style={{ paddingLeft: 15, paddingTop: 8 }}>{ item.Fam_Name } House</Text>
                    
                        {this.showdata()}
                       
               
                      

                  </View>
                
              </View>
            </View>
          </TouchableOpacity>
        );
      };
      
    render() {
    
      const keys = Object.keys(this.state.abd);
        return (
            <View   style={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
            {/* {this.renderResetPassword()}  */}

                <View style={{ backgroundColor: '#7BCFE8'}}>
                <View style={{marginTop:25,marginLeft:30}}>
                <Text style={{fontSize:22,fontWeight:'700'}}>Families</Text>

                </View> 
 <View style={{justifyContent:'center',flexDirection:'row', alignItems:'center'}}>
 <View style={{justifyContent:'center',flexDirection:'row', alignItems:'center',width:160,height:30,marginTop:10,borderRadius:10}}>
 <Text  style={{fontWeight:'700'}}>Total Members : </Text>
<Text style={{fontWeight:'700'}}>{this.state.membercount}</Text>

</View>
<View style={{justifyContent:'center',flexDirection:'row', alignItems:'center',width:180,height:30,marginLeft:10,marginTop:10,borderRadius:10}}>
<Text style={{fontWeight:'700'}}>Total Family: </Text>
<Text style={{fontWeight:'700'}}>{this.state.abd.length}</Text>

</View>
                 
                  </View>
                
                  <View                             
 style={{  borderRadius: 70, paddingLeft:10,margin: 20, marginTop: 20,
                    backgroundColor: '#fff', height: 40, marginTop: 20, width: '90%',marginLeft:20 }}>
                        <View  style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput  
                       
                              placeholder='Search'
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={text => this.searchFunction(text)}
                              selectTextOnFocus={true}
                            //  onFocus={this.readFromClipboard()}
                            //  autoFocus={true}
                              // autoCapitalize={false}
                              // autoCorrect={false}
                          />
                          
                        </View>
                        {/* <TextInput 
                              placeholder='Search'
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={text => this.searchFunction(text)}
                              selectTextOnFocus={true}
                              // autoCapitalize={false}
                              // autoCorrect={false}
                          />
                        </View> */}
                    </View>
                  {/* <View style={{  borderRadius: 70, paddingLeft:10,margin: 20, marginTop: 20,
            backgroundColor: '#ffff', 
            width: '90%', 
            height: 40, 
            alignItems: 'center' ,
            flexDirection: 'row',  }}>
                          <TextInput 
                              placeholder='Search'
                              style={{ color: '#000', paddingLeft: 20 }}
                              onChangeText={text => this.searchFunction(text)}
                              autoCapitalize={false}
                              autoCorrect={false}
                          />

                      </View> */}
                 
                </View>
                <Modal
    visible={this.state.slideAnimationDialog}
    onTouchOutside={() => {
      this.setState({ slideAnimationDialog: false });
    }}
  >
    <ModalContent>
    <Image style={{ width: 200, height: 200,marginTop:3}}
                 
                 source={this.state.propicture}
                 />
    </ModalContent>
  </Modal>
                {/* <Dialog
          onDismiss={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          onTouchOutside={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          visible={this.state.slideAnimationDialog}
          
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
          <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                 
                 source={this.state.propicture}
                 />
          </DialogContent>
        </Dialog> */}
                {/* <Dialog style={{justifyContent:'center',alignItems:'center'}}
          onDismiss={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          onTouchOutside={() => {
            this.setState({ slideAnimationDialog: false });
          }}
          visible={this.state.slideAnimationDialog}
          // dialogTitle={<DialogTitle title="" />}
          dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}>
          <DialogContent>
          <Image style={{ width: 200, height: 200, borderRadius: 100,marginTop:23}}
                 
                 source={this.state.propicture}
                 />
          </DialogContent>
        </Dialog> */}
               
                <FlatList   
                    style={{ marginTop: 2 }}  
                     contentContainerStyle={{ paddingBottom: 25}}     
                    data={this.state.data} 
                     renderItem={this.renderProperties}          

                    //  renderItem={({item, index}) => this.renderProperties(item, keys[index])}                           
                    // keyExtractor={(item, index) => item.title}
                    keyExtractor={(item, index) =>item.Fam_Id}  
                    ItemSeparatorComponent={this.renderSeparator}  
                />            
            </View>
        );
    }
}