/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';

  export default class DetailedNotification extends Component{
    

    render(){
      const { title, description, role, month, date } = this.props.contents;
      return (

          <View style={{height: '100%'}}>
              <View style= {{backgroundColor: '#7BCFE8',width: '100%', height: '100%', borderRadius: 20, borderTopStartRadius: 0, borderTopEndRadius: 0}}>              
              <View style={{marginTop:25,marginLeft:20}}>
                <Text style={{fontSize:21,fontWeight:'700'}}>Notification Details</Text>

                </View> 
                  <View style={{ borderRadius: 5, backgroundColor:'#fff', margin: 16, marginBottom: 58, marginTop: 35}}>
                    <View style={{ flexDirection: 'row'}}>
                      <Text style={{ fontSize:17, fontWeight: '600', marginTop: 35, marginLeft: 15, width: 250, lineHeight: 25}}>{title}</Text>
                      <Text style={{ fontSize: 10, marginTop: 38, fontWeight: 'bold'}}>{date} </Text>
                    </View>
                    <View style={{ borderRadius: 5, backgroundColor: '#ebebeb', marginTop: 40,height: '60%', marginLeft: 8, marginRight: 8}}>
                    <ScrollView style={{ flex: 1 }}>
                        <Text style= {{ marginLeft: 15, marginTop: 15}}>{description}</Text>
                    </ScrollView>
                    </View>
                  </View>
                  <View style={{marginTop: -80}}>
                      {/* <Text style={{ fontSize: 11, marginLeft: 265, fontWeight: 'bold'}}>{role}</Text>                   */}
                  </View>
                  
              </View>
              
              
          </View>
      )
    }
  }