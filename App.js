/* eslint-disable no-dupe-class-members */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, StatusBar ,Alert} from 'react-native';
import Routers from './src/Router/Routers';
import AsyncStorage from '@react-native-community/async-storage';
const LOGIN_CREDENTIALS = '@login_credentials';

export default class App extends Component {
  constructor() {
    super();

    this.state = {
        userRole: '',
        // status_church:'',
        // status_home:'',

    }
}
  componentDidMount() {
    this.checkUserIsLoggedIn();
  }
  async checkUserIsLoggedIn() {
    try {
        var loginCredentials = await AsyncStorage.getItem(LOGIN_CREDENTIALS);
        if(loginCredentials) {
        
            // console.log(loginCredentials);
            var login_credentials = JSON.parse(loginCredentials);
            // console.log(login_credentials.role);
            this.setState({ userRole: login_credentials.role });
            //  this.setState({ status_church: false, status_home: true });
             
        }  else {
          // this.setState({ status_church: true, status_home: false });

           
        }
    } 
    catch (error) {
    }
}
  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar hidden/>
        <Routers abc={this.state.status_church} abc1={this.state.status_home} />
      </View>
    );
  }
}